from setuptools import setup, find_packages

setup(name='pysopale',
      version='0.1.1',
      description='Utilities for Sopale Plots',
      url='',
      author='Romain Beucher',
      author_email='romain.beucher@geo.uib.no',
      license='MIT',
      install_requires = ['h5py',
                          'scipy',
                          'numpy',
                          'matplotlib',
                          'pyfastscape @ git+ssh://git@bitbucket.org:rbeucher/pyfastscape@master'],
      packages=find_packages(),
      zip_safe=False
      )
