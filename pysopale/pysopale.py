#usr/bin/python

#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


import os
import glob
import sys
#import tarfile
import numpy as np
from numpy import ma
from matplotlib.patches import Polygon
from matplotlib.patches import Rectangle
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pylab as plt
from matplotlib import colors
from matplotlib.colors import LogNorm, Normalize
from matplotlib import animation
from matplotlib.path import Path
from matplotlib.collections import PathCollection
from matplotlib.collections import LineCollection
#from .viewer2d import viewer_2d
import scipy.constants as constant
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import griddata
from scipy.interpolate import interp2d
from matplotlib import cm
import h5py
import json
import operator
from scipy import spatial
#from profilehooks import profile
from scipy.signal import savgol_filter

if sys.version_info <= (3, 0):
    from pyevtk.hl import gridToVTK, VtkGroup

try:
    from pyAFT import KetchamModel
    from pyAFT.thermal_histories import Thermal_history
except:
    pass


slm = "#fcfa01"
lml = "#03fbfa"
uml = "#01c890"
lc = "#fdfb9c"
uc = "#fa7c0c"
seed = "pink"
sed1 = "green"
sed2 = "cyan"
sed3 = "red"

lml = uml
seed = lml
ritskemat = colors.ListedColormap([slm, lml, uml, lc, uc, seed, sed1, sed2])
ritskemat2 = colors.ListedColormap([slm, lml, lc, uc, sed1, sed2])
ritskemat3 = colors.ListedColormap([slm, lml, lc, uc])

def SetHalfPageHorizontal():
    # RC params
    fig_width_mm = 190.0
    fig_height_mm = 115.0
    fig_size = [fig_width_mm*0.039, fig_height_mm*0.039]
    plt.rcParams.update({"figure.figsize":  fig_size})


def SetHalfPageVertical():
    # RC params
    fig_width_mm = 95.0
    fig_height_mm = 230.0
    fig_size = [fig_width_mm*0.039, fig_height_mm*0.039]
    plt.rcParams.update({"figure.figsize": fig_size})


def SetQuarterPage():
    # RC params
    fig_width_mm = 95.0
    fig_height_mm = 115.0
    fig_size = [fig_width_mm*0.039, fig_height_mm*0.039]
    plt.rcParams.update({"figure.figsize": fig_size})


def SetFullPage():
    # RC params
    fig_width_mm = 190.0
    fig_height_mm = 230.0
    fig_size = [fig_width_mm*0.039, fig_height_mm*0.039]
    plt.rcParams.update({"figure.figsize": fig_size})

class Power_law(object):
    def __init__(self, A=None, n=None,
                 Ea=None, Va=None, name="flawlaw"):
        self.A = None
        self.n = None
        self.Ea = None
        self.Va = None
        self.name = None


class Thermal_material(object):
    def __init__(self, name="Material"):
        self.name = name


class Mechanical_material(object):
    def __init__(self, name="Material"):
        self.name = name

class ThermoMechanicalModel(object):

    def __init__(self):
        return

    def GetSeaLevel(self):
        """ Get initial sea level from input file """
        sea_level = 0.
        with open(self.path+"/RUN00/FastScape.in", "r") as f:
            for line in f:
                if "sealevel" in line:
                    sea_level = line.split()[2]
        return float(sea_level)

    def matlab_to_hdf(self, **kwargs):
        eulerian_fields = {"X": self.get_eulerian_xcoordinates,
                           "Y": self.get_eulerian_ycoordinates,
                           "Horizontal Velocity": self.get_eulerian_horizontal_velocity,
                           "Vertical Velocity": self.get_eulerian_vertical_velocity,
                           "Temperature": self.get_eulerian_temperature_field,
                           "Pressure": self.get_eulerian_pressure_field,
                           "Strain": self.get_eulerian_strain_field,
                           "Strain Rate": self.get_eulerian_strainRate_field,
                           "Viscosity": self.get_eulerian_viscosity_field,
                           "Material": self.get_eulerian_material_colors}
        lagrangian_fields = {"X": self.get_lagrangian_xcoordinates,
                             "Y": self.get_lagrangian_ycoordinates,
                             "Strain": self.get_lagrangian_strain_field,
                             "Material": self.get_lagrangian_material_colors}

        # Find number of material strings
        strings = glob.glob(self.path+"/matlab/x2s*")
        nstrings = np.unique(np.array([(os.path.split(string)[-1])[:4] for string in strings]))
        nstrings = nstrings.size

        f = h5py.File(self.HDF5_filename, "w")
        for i, step in enumerate(self.steps):
            metadata = self.read_loopFile(step)
            grp = f.create_group("Step"+str(step).zfill(5))
            grp.attrs["Time"] = metadata["time"]
            grp.attrs["dt"] = metadata["dt"]
            subgroup = grp.create_group("Eulerian")
            subgroup.attrs["nx"] = metadata["eul_nx"]
            subgroup.attrs["ny"] = metadata["eul_ny"]
            for key, func in eulerian_fields.items():
                data = func(step)
                if len(data) <= 3:
                    data = data[0]
                dataset = subgroup.create_dataset(str(key), data=data, **kwargs)
                dataset.attrs["Time"] = self.get_time(step)
            subgroup = grp.create_group("Lagrangian")
            subgroup.attrs["nx"] = metadata["lag_nx"]
            subgroup.attrs["ny"] = metadata["lag_ny"]
            for key, func in lagrangian_fields.items():
                data = func(step)
                if len(data) <= 3:
                    data = data[0]
                dataset = subgroup.create_dataset(str(key), data=data, **kwargs)
                dataset.attrs["Time"] = self.get_time(step)
            if step > 0:
                for string in range(1, nstrings+1):
                    subgroup2 = subgroup.create_group("String"+str(string))
                    x, y = self._read_string(step, string)
                    dataset = subgroup2.create_dataset("X", data=x)
                    dataset.attrs["Time"] = self.get_time(step)
                    dataset = subgroup2.create_dataset("Y", data=y)
                    dataset.attrs["Time"] = self.get_time(step)
        f.close()

    def setColormap(self, colors, name="Personal colormap"):
        self.cmap = colors.ListedColormap(colors, name=name)

    def _list_available_steps(self):
        try:
            if not os.path.isfile(self.HDF5_filename):
                raise Exception()
            A = self.HDF5
            steps = list(A)
            if steps == []:
                raise Exception()
            steps = sorted([int(step[4:]) for step in steps])
        except:
            steps = glob.glob(self.path+"/matlab/loop*")
            steps = sorted([int(step.split(".")[-1]) for step in steps])
        return steps

    def _find_available_steps(self,field):

        eulerian = ["x1", "y1", "vx1", "vy1", "temp1", "rhoel", "p", "e2d",
                    "color1", "noise", "strain1", "density_el"]
        lagrangian = ["x2", "y2", "vx2", "vy2", "color2", "strain2"]

        fields = {"x1": "X",
                  "y1": "Y",
                  "vx1": "Horizontal Velocity",
                  "vy1": "Vertical Velocity",
                  "temp1": "Temperature",
                  "p": "Pressure",
                  "strain1": "Strain",
                  "e2d": "Strain Rate",
                  "rhoel": "Viscosity",
                  "color1": "Material",
                  "x2": "X",
                  "y2": "Y",
                  "strain2": "Strain",
                  "color2": "Material",
                  "density_el":"Density",
                  "dt":"dt",
                  "Time":"Time"}

        kind=""

        if field in lagrangian:
            kind = "Lagrangian"

        if field in eulerian:
            kind = "Eulerian"

        steps = []
        for step in self.steps:
            if "Step"+str(step).zfill(5)+"/"+kind+"/"+fields[field] in self.HDF5:
                steps.append(step)
        for step in self.steps:
             exist = os.path.isfile(self.path + "/matlab/" + field +
                     str(step).zfill(5) + ".ml")
             if exist:
                 steps.append(step)

        return sorted(list(set(steps)))

    def _find_last_available_step(self, field):

        eulerian = ["x1", "y1", "vx1", "vy1", "temp1", "rhoel", "p", "e2d",
                    "color1", "noise", "strain1", "density_el"]
        lagrangian = ["x2", "y2", "vx2", "vy2", "color2", "strain2"]

        fields = {"x1": "X",
                  "y1": "Y",
                  "vx1": "Horizontal Velocity",
                  "vy1": "Vertical Velocity",
                  "temp1": "Temperature",
                  "p": "Pressure",
                  "strain1": "Strain",
                  "e2d": "Strain Rate",
                  "rhoel": "Viscosity",
                  "color1": "Material",
                  "x2": "X",
                  "y2": "Y",
                  "strain2": "Strain",
                  "color2": "Material",
                  "density_el":"Density",
                  "dt":"dt",
                  "Time":"Time"}

        kind=""

        if field in lagrangian:
            kind = "Lagrangian"

        if field in eulerian:
            kind = "Eulerian"

        for step in reversed(self.steps):
            if "Step"+str(step).zfill(5)+"/"+kind+"/"+fields[field] in self.HDF5:
                last = step
                break
        for step in reversed(self.steps):
             exist = os.path.isfile(self.path + "/matlab/" + field +
                     str(step).zfill(5) + ".ml")
             if exist and step > last:
                 last = step
                 break

        return last

    def GetElementFromInputFile(self, lineno, position):
        import linecache
        filename = self.path+"/SOPALE1_i"
        A = linecache.getline(filename, lineno).split()[position-1]
        A = A.replace("d", "E")
        A = A.replace("D", "E")
        return float(A)

    def ReplaceElementInputFile(self, lineno, pos, value, filename=None):
        if filename is None:
            filename = self.path+"/SOPALE1_i"
        f = open(filename, "r")
        lines = f.readlines()
        f.close()
        line = lines[lineno].split()
        line[pos] = str(value)
        line.append("\n")
        line = " ".join(line)
        lines[lineno] = line
        f = open(filename, "w")
        f.writelines(lines)

    def Replace_number_of_steps(self, value, filename=None):
        self.ReplaceElementInputFile(self, 13, 1, value, filename)

    def GetHeightOfModel(self):
        return self.GetElementFromInputFile(15, 2)*1e-3

    def GetWidthOfModel(self):
        return self.GetElementFromInputFile(15, 1)*1e-3

    def GetEulerianDims(self):
        """Get the dimension of the Eulerian grid

        Return:
           nx, ny
        """
        nx = self.GetElementFromInputFile(15, 3)
        ny = self.GetElementFromInputFile(15, 4)
        return int(nx), int(ny)

    def GetLagrangianDims(self):
        """Get the dimension of the Lagrangian grid

        Return:
           nx, ny
        """
        nx = self.GetElementFromInputFile(15, 9)
        ny = self.GetElementFromInputFile(15, 10)
        return int(nx), int(ny)

    def setModeldim(self, height=600, width=1200):
        """Set dimensions of the model.

        Keyword arguments:
        ------------------
        *height* -- height of the model in kilometers, default (600)
        *width*  -- width of the model in kilometers, default (1200)

        Returns:
        -------
        The dimensions are set as global variable.
        The dimension of the model (width, height).
        """

        self.model_width = width
        self.model_height = height
        return self.model_width, self.model_height

    def GetConvergenceCriteria(self):
        nmat = int(self.GetNumberOfMaterials())
        lineno = 21 + 1 + nmat*8 + 3
        verror = self.GetElementFromInputFile(lineno + 1, 2)
        verror1 = self.GetElementFromInputFile(lineno + 1, 3)
        return {"verror": verror, "verror1": verror1}

    def GetNumberOfStepsBetweenRestartWrite(self):
        return self.GetElementFromInputFile(6, 1)

    def GetNumberOfTimeSteps(self):
        return self.GetElementFromInputFile(13, 1)

    def GetdtModel(self):
        return self.GetElementFromInputFile(13, 2)

    def GetAngleOfGravity(self):
        return abs(self.GetElementFromInputFile(20, 1))

    def GetGravity(self):
        return abs(self.GetElementFromInputFile(20, 2))

    def GetLowerBoundPressure(self):
        """ Get Lower bound for pressure
             Pressure below this value are set to value
             Pressure is returned in Pascal.
        """
        return abs(self.GetElementFromInputFile(20, 3))

    def GetNumberOfMaterials(self):
        return int(self.GetElementFromInputFile(21, 1))

    def GetMaterialParams(self, mat):

        N = self.GetNumberOfMaterials()
        if mat > N:
            raise ValueError("Only "+str(N)+" materials available")

        lineno = 21 + (mat - 1)*8
        # Density of Material
        density = float(self.GetElementFromInputFile(lineno + 2, 1))
        # "Bulk Viscosity", actually a numerical parameter used to
        # maintain incompressibility.
        bulkv = float(self.GetElementFromInputFile(lineno + 3, 1))

        # Friction
        phi1 = float(self.GetElementFromInputFile(lineno + 4, 1))
        phi2 = float(self.GetElementFromInputFile(lineno + 4, 2))
        phie1 = float(self.GetElementFromInputFile(lineno + 4, 3))
        phie2 = float(self.GetElementFromInputFile(lineno + 4, 4))
        lambda_pore_prs1 = float(self.GetElementFromInputFile(lineno + 4, 5))
        lambda_pore_prs2 = float(self.GetElementFromInputFile(lineno + 4, 6))
        lambda_pore_prse1 = float(self.GetElementFromInputFile(lineno + 4, 7))
        lambda_pore_prse2 = float(self.GetElementFromInputFile(lineno + 4, 8))

        coh1 = float(self.GetElementFromInputFile(lineno + 5, 1))
        coh2 = float(self.GetElementFromInputFile(lineno + 5, 2))
        cohe1 = float(self.GetElementFromInputFile(lineno + 5, 3))
        cohe2 = float(self.GetElementFromInputFile(lineno + 5, 4))

        # Grain size sensitive creep
        gscreep_mu = float(self.GetElementFromInputFile(lineno + 6, 1))
        gscreep_m = float(self.GetElementFromInputFile(lineno + 6, 2))
        gscreep_T = float(self.GetElementFromInputFile(lineno + 6, 3))
        gscreep_strain1 = float(self.GetElementFromInputFile(lineno + 6, 4))
        gscreep_strain2 = float(self.GetElementFromInputFile(lineno + 6, 5))
        gscreep_a0 = float(self.GetElementFromInputFile(lineno + 6, 6))
        gscreep_a1 = float(self.GetElementFromInputFile(lineno + 6, 7))
        gscreep_a2 = float(self.GetElementFromInputFile(lineno + 6, 8))
        gscreep_updown_t = float(self.GetElementFromInputFile(lineno + 6, 9))

        # Non-linear Creep
        nlcreep_eps = float(self.GetElementFromInputFile(lineno + 7, 1))
        nlcreep_sc1 = float(self.GetElementFromInputFile(lineno + 7, 2))
        nlcreep_sc2 = float(self.GetElementFromInputFile(lineno + 7, 3))
        nlcreep_n = float(self.GetElementFromInputFile(lineno + 7, 4))
        nlcreep_T = float(self.GetElementFromInputFile(lineno + 7, 5))
        nlcreep_strain1 = float(self.GetElementFromInputFile(lineno + 7, 6))
        nlcreep_strain2 = float(self.GetElementFromInputFile(lineno + 7, 7))
        nlcreep_updown_T = float(self.GetElementFromInputFile(lineno + 7, 8))

        # Peirs', law creep
        pei_eps = float(self.GetElementFromInputFile(lineno + 8, 1))
        pei_Tn = float(self.GetElementFromInputFile(lineno + 8, 2))
        pei_sc = float(self.GetElementFromInputFile(lineno + 8, 3))
        pei_T2 = float(self.GetElementFromInputFile(lineno + 8, 4))
        pei_strain1 = float(self.GetElementFromInputFile(lineno + 8, 5))
        pei_strain2 = float(self.GetElementFromInputFile(lineno + 8, 6))

        texpand_i = float(self.GetElementFromInputFile(lineno + 9, 1))
        tdensity_i = float(self.GetElementFromInputFile(lineno + 9, 2))

        general = {"Density": density, "Bulkv": bulkv,
                   "Expand": (texpand_i, tdensity_i)}

        friction = {"phi1": phi1, "phi2": phi2,
                    "phie1": phie1, "phie2": phie2,
                    "lambda_pore_prs1": lambda_pore_prs1,
                    "lambda_pore_prs2": lambda_pore_prs2,
                    "lambda_pore_prse1": lambda_pore_prse1,
                    "lambda_pore_prse2": lambda_pore_prse2}

        cohesion = {"coh1": coh1, "coh2": coh2, "cohe1": cohe1, "cohe2": cohe2}
        gscreep = {"mu": gscreep_mu, "m": gscreep_m, "T": gscreep_T,
                   "strain1": gscreep_strain1, "strain2": gscreep_strain2,
                   "a0": gscreep_a0, "a1": gscreep_a1, "a2": gscreep_a2,
                   "updown_t": gscreep_updown_t}
        nl_creep = {"eps": nlcreep_eps, "sc1": nlcreep_sc1, "sc2": nlcreep_sc2,
                    "n": nlcreep_n, "T": nlcreep_T, "strain1": nlcreep_strain1,
                    "strain2": nlcreep_strain2, "updown_T": nlcreep_updown_T}

        peirs = {"eps": pei_eps, "Tn": pei_Tn, "sc": pei_sc, "T2": pei_T2,
                 "strain1": pei_strain1, "strain2": pei_strain2}

        return {"general": general, "friction": friction, "cohesion": cohesion,
                "non-linear creep": nl_creep, "Peir's law": peirs,
                "grain-size creep": gscreep}

    def GetBoundaryConditionsNumber(self):
        nmat = int(self.GetNumberOfMaterials())
        lineno = 21 + 1 + nmat*8
        return int(self.GetElementFromInputFile(lineno + 1, 1))

    def GetBoundaryConditionsParams(self):
        nmat = self.GetNumberOfMaterials()
        lineno = 21 + 1 + nmat*8
        nb = self.GetBoundaryConditionsNumber()
        if(nb == 300):
            vxleft_up = float(self.GetElementFromInputFile(lineno + 2, 1))
            vxleft_down = float(self.GetElementFromInputFile(lineno + 2, 2))
            yleft1 = float(self.GetElementFromInputFile(lineno + 2, 3))
            yleft2 = float(self.GetElementFromInputFile(lineno + 2, 4))
            rollleft = int(self.GetElementFromInputFile(lineno + 2, 5))
            vxright_up = float(self.GetElementFromInputFile(lineno + 2, 6))
            vxright_down = float(self.GetElementFromInputFile(lineno + 2, 7))
            yright1 = float(self.GetElementFromInputFile(lineno + 2, 8))
            yright2 = float(self.GetElementFromInputFile(lineno + 2, 9))
            rollright = int(self.GetElementFromInputFile(lineno + 2, 10))
            vytop0 = float(self.GetElementFromInputFile(lineno + 2, 11))
            vxbottom0 = float(self.GetElementFromInputFile(lineno + 2, 12))
            reference_pressure_column = int(self.GetElementFromInputFile(lineno + 2, 13))
            reference_material = int(self.GetElementFromInputFile(lineno + 2, 14))
        else:
            print("Model is Not Implemented yet")

        return {'vxleft_up': vxleft_up, 'vxleft_down': vxleft_down,
                'yleft1': yleft1, 'yleft2': yleft2, 'rollleft': rollleft,
                'vxright_up': vxright_up, 'vxright_down': vxright_down,
                'yright1': yright1, 'yright2': yright2, 'rollright': rollright,
                'vytop0': vytop0, 'vxbottom0': vxbottom0,
                'reference_pressure_column': reference_pressure_column,
                'reference_material': reference_material}

    def GetNumberOfThermalMaterials(self):
        nmat = int(self.GetNumberOfMaterials())
        lineno = 21 + 1 + nmat*8 + 12
        return int(self.GetElementFromInputFile(lineno, 1))

    def GetThermalMaterialParams(self, mat):
        nmat = int(self.GetNumberOfMaterials())
        lineno = 21 + 1 + nmat*8 + 12
        lineno += (mat - 1)*5
        conductivity = float(self.GetElementFromInputFile(lineno + 1, 1))
        rau0 = float(self.GetElementFromInputFile(lineno + 2, 1))
        cp = float(self.GetElementFromInputFile(lineno + 3, 1))
        fheating = float(self.GetElementFromInputFile(lineno + 4, 1))
        power_permass = float(self.GetElementFromInputFile(lineno + 5, 1))
        return {"Conductivity": conductivity, "rau0": rau0, "Capacity": cp,
                "fheating": fheating, "Power per mass": power_permass}

    def GetNumberOfMaterialsUsed(self):
        nmat1 = int(self.GetNumberOfMaterials())
        nmat2 = int(self.GetNumberOfThermalMaterials())
        lineno = 21 + 1 + nmat1*8 + 12
        lineno += nmat2*5
        lineno += 16
        return int(self.GetElementFromInputFile(lineno, 1))

    def GetMaterialDistribution(self):
        nmat1 = int(self.GetNumberOfMaterials())
        nmat2 = int(self.GetNumberOfThermalMaterials())
        lineno = 21 + 1 + nmat1*8 + 12
        lineno += nmat2*5
        lineno += 16
        listmat = []
        nmat = int(self.GetNumberOfMaterialsUsed())
        for i in range(1, nmat+1):
            line = lineno + (i-1)*3
            x1 = self.GetElementFromInputFile(line + 1, 1) * 1e-3
            y1 = self.GetElementFromInputFile(line + 1, 2) * 1e-3
            x2 = self.GetElementFromInputFile(line + 1, 3) * 1e-3
            y2 = self.GetElementFromInputFile(line + 1, 4) * 1e-3
            x3 = self.GetElementFromInputFile(line + 2, 1) * 1e-3
            y3 = self.GetElementFromInputFile(line + 2, 2) * 1e-3
            x4 = self.GetElementFromInputFile(line + 2, 3) * 1e-3
            y4 = self.GetElementFromInputFile(line + 2, 4) * 1e-3
            mat_number = int(self.GetElementFromInputFile(line + 3, 1))
            listmat.append((mat_number, [(x1, y1), (x2, y2), (x3, y3), (x4, y4)]))
        return listmat


    def get_rotation(self, step):
        """ The function calculates the total rotation between 2 successives points
        """

        # Get Lagrangian coordinates
        xl1 = self.get_lagrangian_xcoordinates(step)
        yl1 = self.get_lagrangian_ycoordinates(step)

        dy = (yl1[1:] - yl1[:-1])
        dx = (xl1[1:] - xl1[:-1])

        tan = dy / dx

        return np.arctan(tan)

    def get_stretch(self, step):
        # That function assumes that the particles are initially distributed
        # on a regular grid (That may not always be the case)

        xl = self.get_lagrangian_xcoordinates(step)
        yl = self.get_lagrangian_ycoordinates(step)

        dx = (xl[1:] - xl[:-1])
        dy = (yl[1:] - yl[:-1])

        dist = np.sqrt(dx**2 + dy**2)

        ## Get the distance between the particles at the initial step
        xl0 = self.get_lagrangian_xcoordinates(0)
        yl0 = self.get_lagrangian_ycoordinates(0)

        dx0 = (xl0[1:] - xl0[:-1])
        dy0 = (yl0[1:] - yl0[:-1])

        dist0 = np.sqrt(dx0**2 + dy0**2)

        abs_stretch = dist - dist0
        rel_stretch = dist / dist0

        return abs_stretch, rel_stretch

    def PlotMaterialDistribution(self):
        materials = self.GetMaterialDistribution()
        fig, ax = plt.subplots()
        colors = self.cmap.colors
        polygons = [Polygon(mat[1], True, color=colors[mat[0]-1]) for mat in materials]
        for polygon in polygons:
            ax.add_patch(polygon)
        ax.set_xlim(0, self.model_width)
        ax.set_ylim(0, self.model_height)

        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("left", size="5%", pad=0.)
        cax2 = divider.append_axes("right", size="5%", pad=0.)
        return ax

    def GetNumberOfThermalMaterialsUsed(self):
        nmat1 = int(self.GetNumberOfMaterials())
        nmat2 = int(self.GetNumberOfThermalMaterials())
        nmat3 = int(self.GetNumberOfMaterialsUsed())
        lineno = 21 + 1 + nmat1*8 + 12
        lineno += nmat2*5
        lineno += 16
        lineno += nmat3*3 + 1
        return int(self.GetElementFromInputFile(lineno, 1))

    def GetThermalMaterialDistribution(self):
        nmat1 = int(self.GetNumberOfMaterials())
        nmat2 = int(self.GetNumberOfThermalMaterials())
        nmat3 = int(self.GetNumberOfMaterialsUsed())
        lineno = 21 + 1 + nmat1*8 + 12
        lineno += nmat2*5
        lineno += 16
        lineno += nmat3*3 + 1
        listmat = []
        nmat = int(self.GetNumberOfThermalMaterialsUsed())
        for i in range(1, nmat+1):
            line = lineno + (i-1)*3
            x1 = self.GetElementFromInputFile(line + 1, 1) * 1e-3
            y1 = self.GetElementFromInputFile(line + 1, 2) * 1e-3
            x2 = self.GetElementFromInputFile(line + 1, 3) * 1e-3
            y2 = self.GetElementFromInputFile(line + 1, 4) * 1e-3
            x3 = self.GetElementFromInputFile(line + 2, 1) * 1e-3
            y3 = self.GetElementFromInputFile(line + 2, 2) * 1e-3
            x4 = self.GetElementFromInputFile(line + 2, 3) * 1e-3
            y4 = self.GetElementFromInputFile(line + 2, 4) * 1e-3
            mat_number = int(self.GetElementFromInputFile(line + 3, 1))
            listmat.append((mat_number, [(x1, y1), (x2, y2), (x3, y3), (x4, y4)]))
        return listmat

    def PlotThermalMaterialDistribution(self):
        materials = self.GetThermalMaterialDistribution()
        fig, ax = plt.subplots()
        colors = self.cmap.colors
        polygons = [Polygon(mat[1], True, color=colors[mat[0]-1]) for mat in materials]
        for polygon in polygons:
            ax.add_patch(polygon)
        plt.xlim(0, self.model_width)
        plt.ylim(0, self.model_height)
        return ax

    def get_range_fieldVal(self, f):
        """Get the range extent of a field value.

        Inputs_:
        --------
        A function that return a field value as an array.

        Returns:
        -------
        A tuple range (min, max)
        """

        minvalue = float("inf")
        maxvalue = float("-inf")
        for step in self.steps:
            A = f(step)
            minvalue = min(minvalue, A[0].min())
            maxvalue = max(maxvalue, A[0].max())

        return (minvalue, maxvalue)

    def read_loopFile(self, step):
        """ Get information from loop. file """
        step = str(step).zfill(5)
        fd = open(self.path+"/matlab/loop."+step, "r")
        data = fd.read().split()
        nx_e = int(data[0])
        ny_e = int(data[1])
        nx_l = int(data[2])
        ny_l = int(data[3])
        time_step = float(data[4])
        time_total = float(data[5])
        dt = float(data[6])
        fd.close()
        return {'eul_nx': nx_e, 'eul_ny': ny_e, 'lag_nx': nx_l, 'lag_ny': ny_l,
                'time_step': time_step, 'time': time_total, 'dt': dt}

    def _read_string(self, step, num):
        """ Read material string file """

        try:
            if not os.path.isfile(self.HDF5_filename):
                raise Exception()
            A = self.HDF5
            x = A["Step"+str(step).zfill(5)]["Lagrangian"]["String"+str(num)]["X"][()]
            y = A["Step"+str(step).zfill(5)]["Lagrangian"]["String"+str(num)]["Y"][()]
            x = x.flatten()
            y = y.flatten()
        except:
            f = open(self.path+"/matlab/x2s"+str(num)+str(step).zfill(5)+".ml", "r")
            data = f.read().split()
            ndata = int(data[0])
            x = np.array([float(i) for i in data[1: ndata+1]])
            f.close()
            f = open(self.path+"/matlab/y2s"+str(num)+str(step).zfill(5)+".ml", "r")
            data = f.read().split()
            ndata = int(data[0])
            y = np.array([float(i) for i in data[1: ndata+1]])
            f.close()
        return x, y

    def get_field(self, step, field):
        eulerian = ["x1", "y1", "vx1", "vy1", "temp1", "rhoel", "p", "e2d",
                    "color1", "noise", "strain1", "density_el", "conduct",
                    "color1t", "sxx", "sxy", "syy"]
        lagrangian = ["x2", "y2", "vx2", "vy2", "color2", "strain2"]
        kind = "Eulerian"

        fields = {"x1": "X",
                  "y1": "Y",
                  "vx1": "Horizontal Velocity",
                  "vy1": "Vertical Velocity",
                  "temp1": "Temperature",
                  "p": "Pressure",
                  "strain1": "Strain",
                  "e2d": "Strain Rate",
                  "rhoel": "Viscosity",
                  "color1": "Material",
                  "x2": "X",
                  "y2": "Y",
                  "strain2": "Strain",
                  "color2": "Material",
                  "density_el":"Density",
                  "conduct":"Conductivity",
                  "color1t":"TMaterial",
                  "sxx":"sxx",
                  "syy":"syy",
                  "sxy":"sxy"}

        if field in lagrangian:
            kind = "Lagrangian"

        try:
            A = self.HDF5
            val = A["Step"+str(step).zfill(5)][kind][fields[field]][()]
            if field in ["color1", "strain1", "density_el", "conduct", "color1t"]:
                shape = val.shape
                val = val.flatten().reshape(shape[1],shape[0]).T
            return val
        except KeyError:
            try:
                val = self._read_matlab_file(step, field)
                return val
            except:
                raise

    def _read_matlab_file(self, step, field):
        """ Read Matlab file """
        eulerian = ["x1", "y1", "vx1", "vy1", "temp1", "rhoel", "p", "e2d",
                    "color1", "noise", "strain1", "density_el"]
        lagrangian = ["x2", "y2", "vx2", "vy2", "color2", "strain2"]
        cell = ["noise", "color1", "density_el", "strain1"]

        if field in eulerian:
            nx, ny = self.eulerian_dims
        if field in lagrangian:
            nx, ny = self.lagrangian_dims

        if field in cell:
            dtypes = {"noise": np.float32,
                      "color1": np.uint32,
                      "density_el": np.float32,
                      "strain1": np.float32}
            nx -= 1
            ny -= 1
            path = self.path + "/matlab/" + field + str(step).zfill(5) + ".ml"
            val = np.fromfile(file=open(path, "rb"), dtype=dtypes[field]).byteswap()
            val = val[: nx*ny]
            return val.reshape(ny, nx).T

        dtype = np.float32
        path = self.path + "/matlab/" + field + str(step).zfill(5) + ".ml"
        val = np.fromfile(file=open(path, "rb"), dtype=dtype).byteswap()
        val = val[: nx*ny]
        return val.reshape(nx, ny)

    def _read_matlab_file2(self, step, field):
        """ Read Matlab file """
        eulerian = ["x1", "y1", "vx1", "vy1", "temp1", "rhoel", "p", "e2d",
                    "color1", "noise", "strain1", "density_el"]
        lagrangian = ["x2", "y2", "vx2", "vy2", "color2", "strain2"]
        cell = ["noise", "color1", "density_el", "strain1"]

        if field in eulerian:
            nx, ny = self.eulerian_dims
        if field in lagrangian:
            nx, ny = self.lagrangian_dims

        if field in cell:
            dtypes = {"noise": np.float32,
                      "color1": np.uint32,
                      "density_el": np.float32,
                      "strain1": np.float32}
            nx -= 1
            ny -= 1
            path = self.path + "/matlab/" + field + str(step).zfill(5) + ".ml"
            val = np.fromfile(file=open(path, "rb"), dtype=dtypes[field])
            val = val[: nx*ny]
            return val.reshape(nx, ny)

        dtype = np.float32
        path = self.path + "/matlab/" + field + str(step).zfill(5) + ".ml"
        val = np.fromfile(file=open(path, "rb"), dtype=dtype).byteswap()
        val = val[: nx*ny]
        return val.reshape(nx, ny)

    def get_dt(self, step):
        """Get dt at step in Myr"""
        secinyr = 3.15*1e7
        try:
            if not os.path.isfile(self.HDF5_filename):
                raise Exception()
            A = self.HDF5
            dt = A["Step"+str(step).zfill(5)]["dt"][()]
        except:
            dt = self.read_loopFile(step)["dt"]
            secinyr = 3.15*1e7
        return dt / secinyr * 1e-6

    def get_time(self, step):
        """ Get Time at Step in Myr """
        secinyr = 3.15*1e7
        try:
            if not os.path.isfile(self.HDF5_filename):
                raise Exception()
            A = self.HDF5
            time = A["Step"+str(step).zfill(5)]["Time"][()]
        except:
            time = self.read_loopFile(step)["time"]
        return time / secinyr * 1e-6

    def get_step_from_time(self, time):
        steps = self._find_available_steps("x1")
        times = []
        for step in steps:
            times.append(self.get_time(step))

        diff = np.array(times) - time
        diff = np.abs(diff)

        index = np.argmin(diff)
        return steps[index]

    def get_total_time(self):
        return self.get_time(self.steps[-1])

    def get_searef(self, step):
        """ Get Sea level reference elevation """
        try:
            A = self.HDF5
            searef = A["Step"+str(step).zfill(5)]["searef"][()]
        except:
            Model = fas.FastScapeModel(self.path+"RUN00/RUN00.h5")
            A = Model.GetInitial(step)
            searef = A[0,0]

        return searef

    def get_eulerian_grid_size(self, step):
        """ Get Dimension of the Eulerian grid """
        return self.eulerian_dims

    def get_lagrangian_grid_size(self, step):
        """ Get Dimension of the Lagrangian grid """
        return self.lagrangian_dims

    def get_eulerian_xcoordinates(self, step):
        """ Get X coordinates of Eulerian grid """
        return self.get_field(step, "x1")

    def get_eulerian_ycoordinates(self, step):
        """ Get Y coordinates of Eulerian grid """
        return self.get_field(step, "y1")

    def get_eulerian_horizontal_velocity(self, step):
        """ Get VX values of Eulerian grid """
        return self.get_field(step, "vx1")

    def get_eulerian_vertical_velocity(self, step):
        """ Get VY values of Eulerian grid """
        return self.get_field(step, "vy1")

    def add_xycoordinates(f):
        """ Decorator to add x and y coordinates outputs """
        def new_function(self, step):
            A = f(self, step)
            x = self.get_eulerian_xcoordinates(step)
            y = self.get_eulerian_ycoordinates(step)
            return A, x, y
        return new_function

    def save_to_file():
        def wrap(f):
            def new_function(self, step, **kwargs):
                a = f(self, step, **kwargs)
                try:
                    output = kwargs["output"]
                except:
                    return a

                plt.savefig(output, dpi=600, bbox_inches="tight")
                plt.close()
                return a
            return new_function
        return wrap

    def add_colorbar(label=None):
        """ Decorator to add colorbar to the plots """
        def wrap(f):
            def new_function(self, step, **kwargs):
                ax = f(self, step, **kwargs)
                try:
                    colorbar = kwargs["colorbar"]
                except KeyError:
                    colorbar = "right"
                cax = None
                if colorbar:
                    # create an axes on the right side of ax. The width of cax will be 5%
                    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
                    orientation = {"left": "vertical", "right": "vertical",
                                   "bottom": "horizontal", "top": "horizontal"}
                    divider = make_axes_locatable(ax)
                    cax = divider.append_axes(colorbar, size="5%", pad=0.05)
                    cb = plt.colorbar(orientation=orientation[colorbar],
                                      cax=cax, label=label)
                    cb.solids.set_rasterized(True)
                return ax, cax
            return new_function
        return wrap

    def get_freesurface_before_surface_processes(self, step):

        try:
            A = self.HDF5
            x = A["Step"+str(step).zfill(5)]["Eulerian"]["FSBeforeErosionX"][()]
            y = A["Step"+str(step).zfill(5)]["Eulerian"]["FSBeforeErosionY"][()]
            return x.ravel(), y.ravel()
        except:
            pass

    def get_freesurface_after_surface_processes(self, step):

        try:
            A = self.HDF5
            x = A["Step"+str(step).zfill(5)]["Eulerian"]["FSAfterErosionX"][()]
            y = A["Step"+str(step).zfill(5)]["Eulerian"]["FSAfterErosionY"][()]

            return x.ravel() ,y.ravel()
        except:
            pass

    @add_xycoordinates
    def get_eulerian_material_colors(self, step):
        """ Get Material on Eulerian grid """
        return self.get_field(step, "color1")

    def get_eulerian_material_colors_on_nodes(self, step):
        """ Get Material on Eulerian grid """
        mat, x, y = self.get_eulerian_material_colors(step)

        xb, yb = self.get_centroid_coordinates(step)
        tree = spatial.KDTree(list(zip(xb.ravel(), yb.ravel())))
        dist, indices = tree.query(list(zip(x.ravel(), y.ravel())))

        matnew = mat.ravel()[indices]
        matnew = matnew.reshape(x.shape)
        return matnew, x, y

    @add_xycoordinates
    def get_eulerian_Tmaterial_colors(self, step):
        """ Get Thermal Material on Eulerian grid """
        return self.get_field(step, "color1t")

    @add_xycoordinates
    def get_eulerian_temperature_field(self, step):
        """ Get Temperature on Eulerian grid """
        return self.get_field(step, "temp1")

    @add_xycoordinates
    def get_eulerian_viscosity_field(self, step):
        """ Get Viscosity on Eulerian grid """
        return self.get_field(step, "rhoel")

    @add_xycoordinates
    def get_eulerian_pressure_field(self, step):
        """ Get Pressure on Eulerian grid """
        return self.get_field(step, "p")

    @add_xycoordinates
    def get_eulerian_density_field(self, step):
        """ Get Density on Eulerian grid """
        return self.get_field(step, "density_el")

    @add_xycoordinates
    def get_eulerian_conductivity_field(self, step):
        """ Get conductivity on Eulerian grid """
        return self.get_field(step, "conduct")

    @add_xycoordinates
    def get_eulerian_noise_field(self, step):
        """ Get Noise on Eulerian grid """
        return self.get_field(step, "noise")

    @add_xycoordinates
    def get_eulerian_stressTensor(self, step):
        """ Get Strain on Eulerian grid """
        sxx = self.get_field(step, "sxx")
        syy = self.get_field(step, "syy")
        sxy = self.get_field(step, "sxy")

        sxx = sxx.flatten()
        syy = syy.flatten()
        sxy = sxy.flatten()

        stress = []
        for i in range(sxx.size):
            stress.append(np.mat([[sxx[i],sxy[i]],[sxy[i], syy[i]]]))

        return np.array(stress)

    @add_xycoordinates
    def get_eulerian_stress2ndInvariant(self, step):
        """ Get Strain on Eulerian grid """
        sxx = self.get_field(step, "sxx")
        syy = self.get_field(step, "syy")
        sxy = self.get_field(step, "sxy")

        sxx = sxx.flatten()
        syy = syy.flatten()
        sxy = sxy.flatten()

        # Calculate Stress Invariant in Megapascal
        stress2ndInvariant = np.sqrt(0.5 * (sxx**2 + syy**2 + sxy**2)) * 1e-6
        nx, ny = self.get_eulerian_grid_size(step)

        return stress2ndInvariant.reshape((nx, ny))

    @add_xycoordinates
    def get_eulerian_stress(self, step):
        """ Get Strain on Eulerian grid """
        eta, x, y = self.get_eulerian_viscosity_field(step)
        e2d, x, y = self.get_eulerian_strainRate_field(step)
        P, x, y = self.get_lithostatic_pressure(step)

        stress = (e2d * eta)
        print("Stress", stress.min(), stress.max())
        print("eta", eta.min(), eta.max())
        print("e2d", e2d.min(), e2d.max())

        return stress

    def get_eulerian_principal_stresses(self, step):

        T = self.get_eulerian_stressTensor(step)

        maxStress = []
        minStress = []
        maxStressVector = []
        minStressVector = []

        for i in range(T.size):
            Ti = np.matrix(T[i])
            eigenvalues, eigenvectors = np.linalg.eig(Ti)
            maxStress.append(eigenvalues[np.argmax(eigenvalues)])
            minStress.append(eigenvalues[np.argmin(eigenvalues)])
            maxStressVector.append(eigenvectors[:,np.argmax(eigenvalues)])
            minStressVector.append(eigenvectors[:,np.argmin(eigenvalues)])

        return (np.array(maxStress), np.array(minStress),
               np.array(maxStressVector), np.array(minStressVector))

    @add_xycoordinates
    def get_eulerian_strain_field(self, step):
        """ Get Strain on Eulerian grid """
        return self.get_field(step, "strain1")

    @add_xycoordinates
    def get_eulerian_strainRate_field(self, step):
        """ Get Strain Rate on Eulerian grid """
        return self.get_field(step, "e2d")

    def get_eulerian_maxVy_evolution(self):
        Vy = []
        for step in self.steps[1:]:
            Vy.append(abs(self.get_eulerian_vertical_velocity(step)).max())
        return self.steps[1:], Vy

    def get_eulerian_minStrainRate_evolution(self):
        time = []
        strainRate = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                strainRate.append(self.get_eulerian_strainRate_field(step)[0].min())
        return time, strainRate

    def get_lagrangian_xcoordinates(self, step):
        """ Get X coordinates of Lagrangian grid """
        return self.get_field(step, "x2")

    def get_lagrangian_ycoordinates(self, step):
        """ Get Y coordinates of Lagrangian grid """
        return self.get_field(step, "y2")

    def get_lagrangian_material_colors(self, step):
        return self.get_field(step, "color2")

    def get_lagrangian_strain_field(self, step):
        return self.get_field(step, "strain2")

    def get_heat_flux(self, step, mantle=3):
        """ need to check the units!!!

        arguments:

        step: the step at which we want to do the calculation
        mantle: index of asthenospheric material

        """

        # Get Temperature field at step
        T, x, y = self.get_eulerian_temperature_field(step)

        # Get Conductivities:
        Conductivities,_,_ = self.get_eulerian_conductivity_field(step)

        # The heat flux is calculated through each material element at the top
        # edge of each element.

        # Calculate the coordinates x and y of the middle of the edges.
        x = (x[1:,:] + x[:-1,:]) / 2.0
        y = (y[1:,:] + y[:-1,:]) / 2.0

        # Calculate the temperature at those points
        T = (T[1:,:] + T[:-1,:]) / 2.0

        # Calculate dT and dy
        dT = np.diff(T)
        dy = np.diff(y)

        # Calculate dT/dy
        dTdy = dT / dy

        Q = -Conductivities * dTdy

        return Q, x, y

    def get_surface_heat_flux(self, step):
        return self.get_heat_flux(step)[0][0]

    def plot_heat_flux(self, step, material=3):
        Q, x, y = self.get_heat_flux(step, material)
        ax = plt.pcolormesh(x, y, Q, cmap="rainbow")
        return ax

    def plot_surface_heat_flux(self, step):
        Q, x, y = self.get_heat_flux(step, material)
        ax = plt.plot(x[:,0], Q[:,0])
        return ax

    def plot_eulerian_material_colors(self, step, cmap=None,
                                      xlim=None, ylim=None, output=None,
                                      colorbar="right", title=None,
                                      visugrid=True, temperature=True,
                                      ctemp=[550, 1300],
                                      zoom=True, clean=False, velocities=False,
                                      sealevel=False):
        """Plot Eulerian Material

        Parameters:
            step: The step at which we want to plot.

        Keywords arguments:
            cmap: Colormap, default is Ristke colormap.
            xlim: x-range
            ylim: y-range
            output: name of the output file
            colorbar: position of the colorbar, default is (bottom),
                      options are: *left*, *right*, *top*, *bottom*

        The function a class axis for the plot and
        a class axis for the colorbar.
        """

        if cmap is None:
            cmap = self.cmap

        color, x, y = self.get_eulerian_material_colors(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        bounds = np.linspace(1, cmap.N+1, cmap.N+1)
        norm = colors.BoundaryNorm(bounds, cmap.N)

        ax = plt.gca()
        xtop, ytop = self.get_freesurface_elevation(step)
        ys = np.where(ytop < self.sealevel*1e-3, self.sealevel*1e-3, ytop)
        ax.plot(xtop, ys, color="k", linewidth=0.3)
        #ax.plot(xtop, (ytop*3.0) + 10.0, color="k", linewidth=0.3)

        if clean:
            plt.clf()

        plt.pcolormesh(x, y, color, cmap=cmap, norm=norm, rasterized=True)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-50, 10)

        if xlim:
            plt.xlim(xlim)

        if ylim:
            plt.ylim(ylim)

        if visugrid:
            ax = self.plot_visugrid(step)

        if temperature:
            T, dum, dum = self.get_eulerian_temperature_field(step)
            T = T - 273.0
            CS=ax.contour(x, y, T, ctemp, colors="r", linewidths=2)
            ax.clabel(CS, fontsize=10, inline=1)


        time = self.get_time(step)
        plt.title("%5.1f Myr" % time)
        if title:
            plt.title("")
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        #ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        #ax.xaxis.label.set_color('grey')
        #ax.yaxis.label.set_color('grey')
        #ax.tick_params(color='grey', labelcolor='grey')

        cax = None

        if(colorbar):
            # create an axes on the right side of ax. The width of cax will be 5%
            # of ax and the padding between cax and ax will be fixed at 0.05 inch.
            orientation = {"left": "vertical", "right": "vertical",
                           "bottom": "horizontal", "top": "horizontal"}
            divider = make_axes_locatable(ax)
            cax = divider.append_axes(colorbar, size="5%", pad=0.05)
            cb = plt.colorbar(orientation=orientation[colorbar], cax=cax)
            if colorbar == "right":
                ticks = []
                for j, lab in enumerate(['SLM', 'LML', 'UML', 'LC', 'UC',
                                         'SEED', 'SED1', 'SED2']):
                    ypos = .5/8.0 + j/8.0
                    ticks.append(ypos)
                    cb.ax.text(1.2, .5/8.0+j/8.0, lab, ha='left', va='center')
                cb.ax.get_yaxis().set_ticks(ticks)
                cb.ax.set_yticklabels([])
                cb.solids.set_rasterized(True)

        if velocities:
            plt.sca(ax)
            self.plot_eulerian_velocity_field_quiver(step)


        if(output):
            if(zoom):
                fig = plt.gcf()
                fig.set_figheight(3)
            plt.savefig(output, dpi=300, bbox_inches="tight")
            plt.close()
        return ax, cax

    @add_colorbar("TMaterials")
    def plot_eulerian_Tmaterial_colors(self, step,
                                      xlim=None, ylim=None, output=None,
                                      colorbar="right", title=None,
                                      visugrid=True, temperature=True,
                                      ctemp=[150, 350, 450, 550, 900,
                                             1100, 1300, 1500],
                                      zoom=True, clean=False):
        """Plot Eulerian Material

        Parameters:
            step: The step at which we want to plot.

        Keywords arguments:
            cmap: Colormap, default is Ristke colormap.
            xlim: x-range
            ylim: y-range
            output: name of the output file
            colorbar: position of the colorbar, default is (bottom),
                      options are: *left*, *right*, *top*, *bottom*

        The function a class axis for the plot and
        a class axis for the colorbar.
        """
        print("Plot step %s" % step)

        color, x, y = self.get_eulerian_Tmaterial_colors(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        ax = plt.gca()
        if clean:
            plt.clf()
        plt.pcolormesh(x, y, color, rasterized=True)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if xlim:
            plt.xlim(xlim)

        if ylim:
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        if temperature:
            T, dum, dum = self.get_eulerian_temperature_field(step)
            T = T - 273.0
            CS=ax.contour(x, y, T, ctemp, colors="r", linewidths=1)
            ax.clabel(CS, fontsize=9, inline=1)


        time = self.get_time(step)
        plt.title("Eulerian material at %5.1f Myr" % time)
        if title:
            plt.title("")
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    def animate_eulerian_field(self, field="Materials", output="video.mp4",
                               dpi=300, fps=5, bitrate=320):
        fields = {"Materials": self.plot_eulerian_material_colors,
                  "Strain Rate": self.plot_eulerian_strain_rate_field}
        f = fields[field]
        fig = plt.figure()
        f(self.steps[0])
        ani = animation.FuncAnimation(fig, f, self.steps[1:], blit=False)

        # Set up formatting for the movie files
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=fps, bitrate=bitrate)
        ani.save(output, writer=writer, dpi=dpi)
        plt.close()

    @save_to_file()
    @add_colorbar("Density  $kg.m^{-3}$")
    def plot_eulerian_density_field(self, step, xlim=None, ylim=None,
                                    output=None, colorbar="right", zoom=False,
                                    visugrid=True, vmin=0., vmax=3500.):
        """ Plot Density Field """
        density, x, y = self.get_eulerian_density_field(step)
        #vmin, vmax = self.get_range_fieldVal(self.get_eulerian_density_field)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        ax = plt.gca()
        A = plt.pcolormesh(x, y, density, rasterized=True, vmin=vmin, vmax=vmax)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Density Field at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    @add_colorbar("Strain")
    def plot_eulerian_strain_field(self, step, xlim=None, ylim=None,
                                   output=None, colorbar="right", zoom=False,
                                   visugrid=True, vmin=0., vmax=50.):
        """ Plot Strain on Eulerian Grid """
        strain, x, y = self.get_eulerian_strain_field(step)
        #vmin, vmax = self.get_range_fieldVal(self.get_eulerian_strain_field)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        ax = plt.gca()
        A = plt.pcolormesh(x, y, strain, rasterized=True, vmin=vmin, vmax=vmax)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Strain Field at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    def plot_visugrid(self, step, maxdist=30.):
        """ This is an attempt to solve the issue of long line
        crossing the model

        args:

        step : step at wich we want to do the plotting
        maxdist :  maximum distance (km) allowed between 2 points in the visugrid

        return : axis matplotlib object

        The code below makes use of the path collection available in
        matplotlib. A Path is simply a list of vertices together with
        some information on how the vertices are connected.
        Maplotlib artist draws the path one vertex at a time.
        Using a Path.MOVETO tells the "pen" to move to the point without
        actually drawing. Using a Path.LINETO means "draw a line from where
        you are to the next point".
        So the idea is to allow drawing only when vertices are not too far
        apart.

        Romain Beucher, March 2016
        """

        nx_l, ny_l = self.get_lagrangian_grid_size(step)

        # Lagrangian grid gives coordinate xl and yl of points
        xl = self.get_lagrangian_xcoordinates(step) * 1e-3
        yl = self.get_lagrangian_ycoordinates(step) * 1e-3 - self.model_height

        # Now we want to create a path collection, in which each path
        # define a line in the visugrid... but where a segment between 2
        # nodes can be hidden if the nodes are too far away from each other.

        # Create a list to which we will append the paths.
        collection = []

        for i in np.arange(0, ny_l, 30):
            x, y = xl[:,i], yl[:,i]
            dx = np.diff(x)
            dy = np.diff(y)
            distances = np.sqrt(dx**2 + dy**2)
            vertices = list(zip(x, y))
            codes = [Path.MOVETO if distance >= maxdist else Path.LINETO for distance in distances]
            # Take care of the first point on the left side...
            codes = [Path.MOVETO] + codes
            # Create the path and add it to the collection
            collection.append(Path(vertices, codes))

        # Create the actual collection object
        collection = PathCollection(collection, edgecolors="black",
                facecolors="None", linestyles="solid", linewidth=0.3)

        # Get current axis for the plotting
        ax = plt.gca()

        # Add the collection to the axis
        ax.add_collection(collection)

        # Now do the vertical lines, for which we do not really have to worry
        for i in np.arange(0, nx_l, 30):
            plt.plot(xl[i], yl[i], "-", linewidth=0.1, color="black",
                alpha=0.5)

        return ax

    def get_stretching_lines(self, step, maxdist=30., cmap="Greens", mat=5,
            alpha=1.0, linewidth=1.0):

        cmap = plt.get_cmap(cmap)

        nx_l, ny_l = self.get_lagrangian_grid_size(step)

        # Lagrangian grid gives coordinate xl and yl of points
        xl = self.get_lagrangian_xcoordinates(step) * 1e-3
        yl = self.get_lagrangian_ycoordinates(step) * 1e-3 - self.model_height
        cl = self.get_lagrangian_material_colors(step)

        abs_stretch, rel_stretch = self.get_stretch(step)
        max_stretch = np.max(rel_stretch)
        norm = (0, np.max(rel_stretch))

        # Create a list to which we will append the paths.
        collection = []
        cols = []

        for i in np.arange(0, ny_l, 30):
            x, y, c = xl[:,i], yl[:,i], cl[:,i]
            x1, x2 = x[:-1], x[1:]
            y1, y2 = y[:-1], y[1:]
            c1, c2 = c[:-1], c[1:]
            stretch = rel_stretch[:,i]
            dx = np.diff(x)
            dy = np.diff(y)
            distances = np.sqrt(dx**2 + dy**2)
            for index, distance in enumerate(list(distances)):
                if c1[index] == mat and c2[index] == mat:
                    if distance < maxdist:
                        collection.append([(x1[index], y1[index]),
                                         (x2[index], y2[index])])
                        cols.append(cmap(stretch[index] / max_stretch, alpha=alpha))

        # Create the actual collection object
        collection = LineCollection(collection, colors=cols,
                     linestyles="solid", linewidth=linewidth)

        return collection

    def get_faults_angles(self, step, maxdist=30., cmap="Greens", mat=5,
            alpha=1.0, linewidth=1.0, stretch_threshold=2.0):

        cmap = plt.get_cmap(cmap)

        nx_l, ny_l = self.get_lagrangian_grid_size(step)

        # Lagrangian grid gives coordinate xl and yl of points
        xl = self.get_lagrangian_xcoordinates(step) * 1e-3
        yl = self.get_lagrangian_ycoordinates(step) * 1e-3 - self.model_height
        cl = self.get_lagrangian_material_colors(step)

        abs_stretch, rel_stretch = self.get_stretch(step)
        rotations = self.get_rotation(step)
        rotations = np.rad2deg(rotations)
        rotations = np.abs(rotations)
        angles = []

        for i in np.arange(0, ny_l, 30):
            x, y, c = xl[:,i], yl[:,i], cl[:,i]
            x1, x2 = x[:-1], x[1:]
            y1, y2 = y[:-1], y[1:]
            c1, c2 = c[:-1], c[1:]
            stretch = rel_stretch[:,i]
            rot = rotations[:,i]
            dx = np.diff(x)
            dy = np.diff(y)
            distances = np.sqrt(dx**2 + dy**2)
            for index, distance in enumerate(list(distances)):
                if c1[index] == mat and c2[index] == mat:
                    if distance < maxdist:
                        if stretch[index] > stretch_threshold:
                            angles.append(rot[index])

        return angles

    def plot_stretching_lines(self, step, maxdist=30., cmap="Greens", mat=5,
            alpha=1.0, linewidth=1.0):

        collection = self.get_stretching_lines(step, maxdist=maxdist, cmap=cmap,
                mat=mat, alpha=alpha, linewidth=linewidth)

        # Get current axis for the plotting
        ax = plt.gca()

        # Add the collection to the axis
        ax.add_collection(collection)

        plt.xlim(400,800)
        plt.ylim(-40,10)
        m = cm.ScalarMappable(cmap=cmap)
        m.set_array(self.get_stretch(step))
        cb = plt.colorbar(m)
        cb.solids.set_rasterized(True)

        return ax

    def get_rotation_lines(self, step, maxdist=30., cmap="Reds", mat=5, alpha=1.0,
            linewidth=1.0):

        cmap = plt.get_cmap(cmap)

        nx_l, ny_l = self.get_lagrangian_grid_size(step)

        # Lagrangian grid gives coordinate xl and yl of points
        xl = self.get_lagrangian_xcoordinates(step) * 1e-3
        yl = self.get_lagrangian_ycoordinates(step) * 1e-3 - self.model_height
        cl = self.get_lagrangian_material_colors(step)

        rotations = self.get_rotation(step)
        rotations = np.rad2deg(rotations)
        #rotations += np.abs(np.min(rotations))
        rotations = np.abs(rotations)
        scale = np.max(rotations)

        # Create a list to which we will append the paths.
        collection = []
        cols = []

        for i in np.arange(0, ny_l, 90):
            x, y, c = xl[:,i], yl[:,i], cl[:,i]
            x1, x2 = x[:-1], x[1:]
            y1, y2 = y[:-1], y[1:]
            c1, c2 = c[:-1], c[1:]
            rot = rotations[:,i]
            dx = np.diff(x)
            dy = np.diff(y)
            distances = np.sqrt(dx**2 + dy**2)
            for index, distance in enumerate(list(distances)):
                if c1[index] == mat and c2[index] == mat:
                    if distance < maxdist:
                        collection.append([(x1[index], y1[index]),
                                         (x2[index], y2[index])])
                        cols.append(cmap(rot[index] / scale, alpha=alpha))

        # Create the actual collection object
        collection = LineCollection(collection, colors=cols,
                     linestyles="solid", linewidth=linewidth)


        return collection


    def plot_rotation_lines(self, step, maxdist=30., cmap="Reds", mat=5, alpha=1.0,
            linewidth=1.0):

        collection = self.get_rotation_lines(step, maxdist=maxdist, cmap=cmap,
                mat=mat, alpha=alpha, linewidth=linewidth)

        # Get current axis for the plotting
        ax = plt.gca()

        # Add the collection to the axis
        ax.add_collection(collection)

        plt.xlim(400,800)
        plt.ylim(-40,10)
        m = cm.ScalarMappable(cmap=cmap)
        #m.set_array(np.rad2deg(self.get_rotation(step)))
        m.set_array(np.abs(np.rad2deg(self.get_rotation(step))))
        cb = plt.colorbar(m)
        cb.solids.set_rasterized(True)

        return ax

    def plot_eulerian_velocity_field_quiver(self, step, **kwargs):
        """ Plot Velocity Field at step using arrows """
        # Eulerian grid
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        nx_e, ny_e = x.shape
        # Velocity
        Vx = self.get_eulerian_horizontal_velocity(step)
        Vy = self.get_eulerian_vertical_velocity(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        # Velocities to mm/year
        Vx *= 1e3 * 365 * 24 * 3600
        Vy *= 1e3 * 365 * 24 * 3600
        return plt.quiver(x[0: nx_e: 10, 0: ny_e: 10],
                          y[0: nx_e: 10, 0: ny_e: 10],
                          Vx[0: nx_e: 10, 0: ny_e: 10],
                          Vy[0: nx_e: 10, 0: ny_e: 10], units="xy",
                          width=0.5, scale=1.0,
                          pivot="mid", **kwargs)

    def plot_eulerian_velocity_field_streamline(self, step):
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        Vx = self.get_eulerian_horizontal_velocity(step)
        Vy = self.get_eulerian_vertical_velocity(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        # Velocities to mm/year
        Vx *= 1e3 * 365 * 24 * 3600
        Vy *= 1e3 * 365 * 24 * 3600
        c = np.sqrt(Vx*Vx+Vy*Vy)
        # plt.streamplot(x, y, Vx, Vy, density=(7, 7), color="#000000",
        # linewidth=3*c/c.max())
        return plt.streamplot(
            x,
            y,
            Vx,
            Vy,
            density=0.6,
            color="#000000",
            linewidth=3 *
            c /
            c.max())

    @add_colorbar("Velocity Module")
    def plot_eulerian_velocity_module(self, step):
        """ Plot Module of Velocity Field at step """
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        Vx = self.get_eulerian_horizontal_velocity(step)
        Vy = self.get_eulerian_vertical_velocity(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        # Velocities to mm/year
        Vx *= 1e3 * 365 * 24 * 3600
        Vy *= 1e3 * 365 * 24 * 3600
        c = np.sqrt(Vx*Vx+Vy*Vy)
        plt.pcolormesh(x,
            y,
            c,
            shading='gouraud',
            alpha=1,
            cmap=plt.cm.Blues,
            rasterized=True)

    #@profile(immediate=True)
    @save_to_file()
    @add_colorbar("Temperature")
    def plot_eulerian_temperature_field(self, step, xlim=None, ylim=None,
                                        output=None, colorbar="right", zoom=False,
                                        cmap="coolwarm",
                                        visugrid=True,
                                        vmin=0., vmax=1500.):
        """ Plot Temperature Field at step """
        # Temperature field
        T, x, y = self.get_eulerian_temperature_field(step)
        #vmin, vmax = self.get_range_fieldVal(self.get_eulerian_temperature_field)
        # Distances are converted to meters
        y *= 1e-3
        y -= self.model_height
        x *= 1e-3
        # Convert Temperature to C
        T -= 273
        ax = plt.gca()
        A = plt.pcolormesh(x, y, T, rasterized=True, vmin=vmin, vmax=vmax, cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Temperature Field at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    @add_colorbar("Conductivity")
    def plot_eulerian_conductivity_field(self, step, xlim=None, ylim=None,
                                        output=None, colorbar="right", zoom=False,
                                        cmap="coolwarm",
                                        visugrid=True):
        """ Plot Temperature Field at step """
        # Temperature field
        A, x, y = self.get_eulerian_conductivity_field(step)
        # Distances are converted to meters
        y *= 1e-3
        y -= self.model_height
        x *= 1e-3
        # Convert Temperature to C
        ax = plt.gca()
        A = plt.pcolormesh(x, y, A, rasterized=True, cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Conductivity Field at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    @save_to_file()
    @add_colorbar("Pressure (GPa)")
    def plot_eulerian_pressure_field(self, step, xlim=None, ylim=None,
                                     output=None, colorbar="right",
                                     zoom=False, visugrid=True, cmap="jet",
                                     vmin=0., vmax=10.):
        """ Plot Pressure Field at step """
        # Temperature field
        P, x, y = self.get_eulerian_pressure_field(step)
        #vmin, vmax = self.get_range_fieldVal(self.get_eulerian_pressure_field)
        # Convert to GPa
        P *= 1e-9
        vmin *= 1e-9
        vmax *= 1e-9
        # Distances are converted to meters
        y *= 1e-3
        y -= self.model_height
        x *= 1e-3
        ax = plt.gca()
        A = plt.pcolormesh(
            x,
            y,
            P,
            rasterized=True,
            cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Pressure Field at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    @add_colorbar("Viscosity")
    def plot_eulerian_viscosity_field(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            zoom=False,
            visugrid=True,
            cmap="jet",
            vmin=1e18, vmax=1e28):
        """ Plot Viscosity Field at step """
        # Eulerian grid
        rhoel, x, y = self.get_eulerian_density_field(step)
        #vmin, vmax = self.get_range_fieldVal(self.get_eulerian_density_field)
        # Distances are converted to meters
        y *= 1e-3
        y -= self.model_height
        x *= 1e-3
        ax = plt.gca()
        A = plt.pcolormesh(
            x,
            y,
            rhoel,
            norm=LogNorm(
                vmin=vmin,
                vmax=vmax),
            rasterized=True,
            cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Viscosity Field at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    @save_to_file()
    @add_colorbar("Horizontal Velocity")
    def plot_eulerian_horizontal_velocity_field(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            zoom=False,
            cmap="jet",
            visugrid=True
            ):
        """ Plot Horizontal Velocity Field at step """
        # Eulerian grid
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        # Velocity
        Vx = self.get_eulerian_horizontal_velocity(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        # Velocities to mm/year
        Vx *= 1e3 * 365 * 24 * 3600
        ax = plt.gca()
        A = plt.pcolormesh(x, y, Vx, rasterized=True, cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Horizontal Velocity at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    @save_to_file()
    @add_colorbar("Vertical Velocity")
    def plot_eulerian_vertical_velocity_field(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            cmap="jet",
            zoom=False,
            visugrid=True):
        """ Plot Vertical Velocity Field at step """
        # Eulerian grid
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        # Velocity
        Vy = self.get_eulerian_vertical_velocity(step)
        Vy = np.ma.masked_where(Vy < 0, Vy)
        cmap = cm.jet
        cmap.set_bad("k", 1.)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        # Velocities to mm/year
        Vy *= 1e3 * 365 * 24 * 3600
        ax = plt.gca()
        A = plt.pcolormesh(x, y, Vy, rasterized=True, cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Vertical Velocity at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    @save_to_file()
    @add_colorbar("Lithostatic Pressure (GPa)")
    def plot_eulerian_lithostatic_pressure_field(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            cmap="jet",
            zoom=False,
            visugrid=True):
        """ Plot Lithostatic Pressure Field at step """
        P, x, y = self.get_lithostatic_pressure(step)
        #vmin, vmax = self.get_range_fieldVal(self.get_lithostatic_pressure)
        # Convert to GPa
        P *= 1e-9
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3
        # Convert Temperature to C
        ax = plt.gca()
        A = plt.pcolormesh(x, y, P, rasterized=True, cmap=cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-150, 10)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Lithostatic Pressure at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    #@save_to_file()
    @add_colorbar("Strain Rate")
    def plot_eulerian_strain_rate_field(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            zoom=True,
            visugrid=True,
            cmap="jet"):
        """ Plot Strain Rate on Eulerian grid """
        e2d, x, y = self.get_eulerian_strainRate_field(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        mat, _, _ = self.get_eulerian_material_colors_on_nodes(step)
        e2d = np.ma.masked_array(e2d, mat > 6)
        cmap = eval("cm."+cmap)
        cmap.set_bad("grey")

        ax = plt.gca()
        A = plt.pcolormesh(
            x,
            y,
            e2d,
            norm=LogNorm(
                vmin=1e-21,
                vmax=1e-13),
            rasterized=True,
            cmap=cmap)

        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-50, 10)

        if xlim:
            plt.xlim(xlim)

        if ylim:
            plt.ylim(ylim)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Strain Rate at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    @add_colorbar("Stress")
    def plot_eulerian_stress2ndInvariant(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            zoom=True,
            visugrid=True,
            temperature=None,
            cmap="jet"):
        """ Plot Second Invariant of Stress on Eulerian grid """
        ss, x, y = self.get_eulerian_stress2ndInvariant(step)
        print(ss.min(), ss.max())

        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        mat, _, _ = self.get_eulerian_material_colors_on_nodes(step)
        ss = np.ma.masked_array(ss, mat > 6)
        cmap = eval("cm."+cmap)
        cmap.set_bad("grey")

        ax = plt.gca()
        A = plt.pcolormesh(
            x,
            y,
            ss,
            norm=Normalize(
                vmin=0.0,
                vmax=1.0e3),
            rasterized=True,
            cmap=cmap)
        
        ax = plt.gca()
        xtop, ytop = self.get_freesurface_elevation(step)
        ys = np.where(ytop < self.sealevel*1e-3, self.sealevel*1e-3, ytop)
        ax.plot(xtop, ys, color="k", linewidth=0.3)
        
        if temperature:
            T, dum, dum = self.get_eulerian_temperature_field(step)
            T = T - 273.0
            CS=ax.contour(x, y, T, temperature, colors="white", linewidths=1)
            ax.clabel(CS, fontsize=10, inline=1)

        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-50, 10)

        if xlim:
            plt.xlim(xlim)

        if ylim:
            plt.ylim(ylim)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Stress at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax
    
    @add_colorbar("Stress")
    def plot_eulerian_stress(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="right",
            zoom=True,
            visugrid=True,
            cmap="jet"):
        """ Plot Second Invariant of Stress on Eulerian grid """
        ss, x, y = self.get_eulerian_stress(step)
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        mat, _, _ = self.get_eulerian_material_colors_on_nodes(step)
        ss = np.ma.masked_array(ss, mat > 6)
        cmap = eval("cm."+cmap)
        cmap.set_bad("grey")

        ax = plt.gca()
        A = plt.pcolormesh(
            x,
            y,
            ss,
            rasterized=True,
            cmap=cmap)

        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if zoom:
            plt.xlim(400, 800)
            plt.ylim(-50, 10)

        if xlim:
            plt.xlim(xlim)

        if ylim:
            plt.ylim(ylim)

        if visugrid:
            ax = self.plot_visugrid(step)

        time = self.get_time(step)
        plt.title("Stress at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_color('grey')
        ax.yaxis.label.set_color('grey')
        ax.tick_params(color='grey', labelcolor='grey')

        return ax

    def plot_all_eulerian_fields(self, field="Materials", overwrite=False,
                                 format="pdf",**kwargs):

        options = {"Materials": self.plot_eulerian_material_colors,
                   "Density": self.plot_eulerian_density_field,
                   "Temperature": self.plot_eulerian_temperature_field,
                   "Pressure": self.plot_eulerian_pressure_field,
                   "HorizontalVelocity": self.plot_eulerian_horizontal_velocity_field,
                   "VerticalVelocity": self.plot_eulerian_vertical_velocity_field,
                   "LithostaticPressure": self.plot_eulerian_lithostatic_pressure_field,
                   "StrainRate": self.plot_eulerian_strain_rate_field,
                   "FreeSurface Velocities": self.plot_freesurface_velocities}


        if field not in options:
            raise "Please Enter a valid field"

        f = options[field]

        field = field.replace(" ", "")
        if not os.path.isdir(self.path+"/Plots"):
            os.mkdir(self.path+"/Plots")
        if not os.path.isdir(self.path+"/Plots/"+field):
            os.mkdir(self.path+"/Plots/"+field)

        steps = self.steps[1:]
        for step in steps:
            output = (self.path +"/Plots/" + field + "/" +
                      field+str(step).zfill(6) + "." + str(format))

            if os.path.isfile(output) and not overwrite:
                pass
            else:
                f(step, output=output, **kwargs)

        return 0

    def plot_all_eulerian_fields2(self, **kwargs):
        options = {"Materials": self.plot_eulerian_material_colors,
                   "Density": self.plot_eulerian_density_field,
                   "Temperature": self.plot_eulerian_temperature_field,
                   "Pressure": self.plot_eulerian_pressure_field,
                   "HorizontalVelocity": self.plot_eulerian_horizontal_velocity_field,
                   "VerticalVelocity": self.plot_eulerian_vertical_velocity_field,
                   "LithostaticPressure": self.plot_eulerian_lithostatic_pressure_field,
                   "StrainRate": self.plot_eulerian_strain_rate_field}
        for key in options.keys():
            print("Plot: "+key)
            self.plot_all_eulerian_fields(field=key, **kwargs)

        self.plot_freesurface_evolution(output="ElevationSerie3D.pdf")

        return 0

    def PlotStrain2(self, step):
        """ Plot Strain on Lagrangian grid """
        strain2 = self.get_field(step, "strain2")
        xl = self.get_lagrangian_xcoordinates(step)
        yl = self.get_lagrangian_ycoordinates(step)
        # Distances are converted to meters
        yl = yl*1e-3 - self.model_height
        xl *= 1e-3
        return plt.pcolormesh(xl, yl, strain2, rasterized=True)

    def plot_lagrangian_material_colors(self, step):
        """ Plot Material on Lagrangian grid """
        color2 = self.get_field(step, "color2")
        xl = self.get_lagrangian_xcoordinates(step)
        yl = self.get_lagrangian_ycoordinates(step)
        # Distances are converted to meters
        yl = yl*1e-3 - self.model_height
        xl *= 1e-3
        return plt.pcolormesh(xl, yl, color2, cmap=self.cmap, rasterized=True)

    def get_freesurface_elevation(self, step):
        """ Plot Free Surface (Topography) at step """
        # Eulerian grid
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        # extract top surface
        xtop = x[:, 0] * 1e-3
        #ytop = y[:, 0] * 1e-3 - self.model_height
        ytop = (y[:, 0]-y[1,0] - self.sealevel) * 1e-3 # Elevation relative to second left node
        #ytop = y[:, 0] * 1e-3 - self.model_height # Elevation relative to left node
        return xtop, ytop

    @save_to_file()
    def plot_freesurface_velocities(self,step, title="Free Surface Velocities",
                                    xlim=(400,800), ylim=(-10,10),
                                    output=None, every=5):
        xtop, ytop, Vxtop, Vytop = self.get_freesurface_velocities(step)
        time = self.get_time(step)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        fig.set_size_inches((4,3))
        plt.quiver(xtop[::every], [0 for i in range(len(xtop[::every]))],
                [0 for i in range(len(xtop[::every]))], Vytop[::every],
                   scale_units='xy', angles='xy', scale=1, width=0.001)

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

        plt.title(title+" at Time: %5.1f Myr" % time)
        plt.xlabel("Distance along Model (km)")
        plt.ylabel("Velocity")
        return ax


    def get_freesurface_velocities(self, step):
        """ Plot Free Surface (Topography) at step """
        # Eulerian grid
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        Vx = self.get_eulerian_horizontal_velocity(step)
        Vy = self.get_eulerian_vertical_velocity(step)
        # extract top surface
        xtop = x[:, 0] * 1e-3
        ytop = y[:, 0] * 1e-3 - self.model_height
        # Velocities to mm/year
        Vxtop = Vx[:, 0] * 1e3 * 365 * 24 * 3600
        Vytop = Vy[:, 0] * 1e3 * 365 * 24 * 3600
        return xtop, ytop, Vxtop, Vytop

    def animate_freesurface_velocities(self, title="Free surface velocity", output=None):
        """ Animate Freesurface velocities """
        def animate(i):
            plt.cla()
            xtop, ytop, Vxtop, Vytop = f(self.steps[i])
            plt.quiver(xtop, [0 for i in range(len(xtop))],
                       [0 for i in range(len(xtop))], Vytop,
                       scale_units='xy', angles='xy', scale=1, width=0.001)
            time = self.get_time(self.steps[i])
            plt.ylim(-5.0, 5.0)
            plt.title(title+" at Time: %5.1f Myr" % time)
            plt.xlabel("Distance along Model (km)")
            plt.ylabel("Velocity")

        fig, ax = plt.subplots()
        fig.set_size_inches((4,3))

        # We need to prime the pump, so to speak and create
        # a quadmesh for plt to work with
        f = self.get_freesurface_velocities
        xtop, ytop, Vxtop, Vytop = f(self.steps[0])
        plt.quiver(xtop, [0 for i in range(len(xtop))],
                   [0 for i in range(len(xtop))], Vytop,
                   scale_units='xy', angles='xy', scale=1, width=0.001)

        plt.ylim(-5.0, 5.0)

        # Set up formatting for the movie files
        Writer = animation.writers['ffmpeg']
        #writer = Writer(fps=3, bitrate=1800)
        writer = Writer(fps=3)

        ani = animation.FuncAnimation(fig, animate, np.arange(1, len(self.steps)),
                                      blit=False)

        ani.save("video.mp4", writer=writer, dpi=400)
        plt.close(fig)

    def get_freesurface_maxelevation(self, step):
        """Get Maximum Elevation of the Free Surface at step"""
        xtop, ytop = self.get_freesurface_elevation(step)
        return ytop.max()

    def get_freesurface_minelevation(self, step):
        """Get Minimum Elevation of the Free Surface at step"""
        xtop, ytop = self.get_freesurface_elevation(step)
        return ytop.min()

    def get_freesurface_meanelevation(self, step):
        """Get Mean Elevation of the Free Surface at step"""
        xtop, ytop = self.get_freesurface_elevation(step)
        return ytop.mean()

    def get_freesurface_maxelevation_evolution(self):
        time = []
        topo = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                topo.append(self.get_freesurface_maxelevation(step))
        return time, topo

    def get_freesurface_meanelevation_evolution(self):
        time = []
        topo = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                topo.append(self.get_freesurface_meanelevation(step))
        return time, topo

    def get_freesurface_minelevation_evolution(self):
        time = []
        topo = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                topo.append(self.get_freesurface_minelevation(step))
        return time, topo

    def plot_freesurface_elevation(self, step, xlim=None, ylim=None, **kwargs):
        """ Plot Free Surface (Topography) at step """
        xtop, ytop = self.get_freesurface_elevation(step)
        plt.gca()
        line = plt.plot(xtop, ytop, **kwargs)
        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)
        return line


    def plot_freesurface_before_after_surface_processes(self, step, xlim=None,
                                                        ylim=None, **kwargs):

        x1, y1 = self.get_freesurface_before_surface_processes(step)
        x2, y2 = self.get_freesurface_after_surface_processes(step)
        plt.gca()
        line1 = plt.plot(x1, y1, label="Before SP", **kwargs)
        line2 = plt.plot(x2, y2, label="After SP", **kwargs)
        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)
        plt.legend()
        return line1, line2

    def plot_freesurface_elevation_at_all_time(self, cmap="Blues", output=None):
        """ Plot Free Surfaces (Topography) at each time step """
        options = {"Blues": cm.Blues, "Reds": cm.Reds}
        cmap = options[cmap]
        ax = plt.gca()
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Z (km)")
        plt.title("Freesurface at all steps")

        #steps = self._find_available_steps("x1")
        colors = cmap(np.linspace(0, 1, len(self.steps)))
        for index, step in enumerate(self.steps):
            self.plot_freesurface_elevation(step, color=colors[index])
        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    def plot_freesurface_maxelevation_evolution(self, tlim=None, **kwargs):
        x, y = self.get_freesurface_maxelevation_evolution()
        ax = plt.gca()
        plt.plot(x, y, **kwargs)
        plt.xlim(min(x), max(x))
        if(tlim):
            plt.xlim(tlim)
        return ax

    def plot_freesurface_meanelevation_evolution(self, tlim=None, **kwargs):
        x, y = self.get_freesurface_meanelevation_evolution()
        ax = plt.gca()
        plt.plot(x, y, **kwargs)
        plt.xlim(min(x), max(x))
        if(tlim):
            plt.xlim(tlim)
        return ax

    def plot_freesurface_minelevation_evolution(self, tlim=None, **kwargs):
        x, y = self.get_freesurface_minelevation_evolution()
        ax = plt.gca()
        plt.plot(x, y, **kwargs)
        plt.xlim(min(x), max(x))
        if(tlim):
            plt.xlim(tlim)
        return ax

    def plot_freesurface_elevation_evolution(self):
        steps, minElevs = self.get_freesurface_minelevation_evolution()
        steps, maxElevs = self.get_freesurface_maxelevation_evolution()
        steps, meanElevs = self.get_freesurface_meanelevation_evolution()
        ax = plt.gca()
        plt.plot(steps, minElevs, color="blue", label="Min")
        plt.plot(steps, maxElevs, color="red", label="Max")
        plt.plot(steps, meanElevs, color="green", label="Mean")
        plt.legend(frameon=False, prop={'size':6})
        plt.xlabel("Time (Myr)")
        plt.ylabel("Elevation (m)")
        plt.title("Sopale Freesurface")
        return ax

    @add_colorbar("Elevation (km)")
    def plot_freesurface_evolution(self, output=None, colorbar="right",
                                   cmap="coolwarm"):
        ydim = len(self.steps)
        xdim, dum = self.get_eulerian_grid_size(0)
        chart = np.zeros((ydim, xdim))
        time = []
        for I in range(0, ydim):
            xtop, ytop = self.get_freesurface_elevation(self.steps[I])
            chart[I, :] = ytop
            time.append(self.get_time(self.steps[I]))

        time = np.array(time)
        X, Y = np.meshgrid(xtop, time)

        ax = plt.gca()
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Time (Myr)")
        plt.title("Elevation trough time")

        A = plt.pcolormesh(X, Y, chart, rasterized=True, cmap=cmap)
        plt.xlim(0, xtop.max())
        plt.ylim(0, time.max())

        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    def plot_eulerian_minStrainRate_evolution(self, tlim=None):
        x, y = self.get_eulerian_minStrainRate_evolution()
        ax = plt.gca()
        plt.plot(x, y)
        plt.xlim(min(x), max(x))
        if(tlim):
            plt.xlim(tlim)
        return ax

    def plot_material_string(self, step, number):
        """ Plot Material String at step """
        step = str(step).zfill(5)
        x, y = self._read_string(step, number)
        y = y*1e-3 - self.model_height
        x *= 1e-3
        return plt.plot(x, y)

    def get_material_layer_thickness(self, step, string1, string2):
        """ Get Thicknesses between 2 material strings """
        step = str(step).zfill(5)
        # xtop, ytop = get_freesurface_elevation(step)
        x1, y1 = self._read_string(step, string1)
        x2, y2 = self._read_string(step, string2)
        y2 = np.interp(x1, x2, y2)
        thickness = abs(y2 - y1)
        return {"x": x1, "Thickness": thickness}

    def get_material_layer_thickness_at_X(self, step, string1, string2, pos):
        """ Get Thickness between 2 material strings at position X """
        info = self.get_material_layer_thickness(step, string1, string2)
        x = info["x"]
        z = info["Thickness"]
        Thickness = np.interp(pos, x, z)
        return Thickness

    def get_material_layer_maxthickness(self, step, layer="crust"):
        """ Get Max Thickness between 2 material strings """
        options = {"uppercrust": (1, 2), "lowercrust": (2, 3),
            "lithosphere": (1, 4), "crust": (1, 3), "mantlelithosphere":(3,4)}
        string1, string2 = options[layer]
        return np.max(
            self.get_material_layer_thickness(
                step,
                string1,
                string2)["Thickness"])

    def get_material_layer_minthickness(self, step, layer="crust"):
        """ Get Min Thickness between 2 material strings """
        options = {"uppercrust": (1, 2), "lowercrust": (2, 3),
            "lithosphere": (1, 4), "crust": (1, 3), "mantlelithosphere":(3,4)}
        string1, string2 = options[layer]
        return np.min(
            self.get_material_layer_thickness(
                step,
                string1,
                string2)["Thickness"])

    def get_material_layer_thickness_at_X_through_time(self, pos, layer="crust"):
        """ Get Thickness between 2 material strings at position X through time """
        options = {"uppercrust": (1, 2), "lowercrust": (2, 3),
            "lithosphere": (1, 4), "crust": (1, 3), "mantlelithosphere":(3,4)}
        string1, string2 = options[layer]
        time = []
        thick = []
        for step in self.steps:
            time.append(self.get_time(step))
            thick.append(
                self.get_material_layer_thickness_at_X(
                    step,
                    string1,
                    string2,
                    pos))
        return time, thick

    def get_material_layer_maxthickness_through_time(self, layer="crust"):
        """ Get Max Thickness between 2 material strings through time """
        options = {"uppercrust": (1, 2), "lowercrust": (2, 3),
            "lithosphere": (1, 4), "crust": (1, 3), "mantlelithosphere":(3,4)}
        string1, string2 = options[layer]
        time = []
        thick = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                thick.append(self.get_material_layer_maxthickness(step, string1, string2))
        return time, thick

    def GetThicknessMinTime(self, layer="crust"):
        """ Get Min Thickness between 2 material strings through time """
        time = []
        thick = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                thick.append(self.get_material_layer_minthickness(step,
                  layer=layer))
        return time, thick

    def GetBreakUpTime(self, layer="crust", threshold=2.0):
        """Get Time of Break up
        options are = 'crust', 'lithosphere', 'uppercrust', lowercrust'
        """
        time, thick = self.GetThicknessMinTime(layer=layer)
        index = int(np.nonzero(np.array(thick) < threshold)[0][0])
        return time[index-1], time[index]

    def plot_material_layer_minthickness_through_time(
            self,
            layer="crust",
            xlim=None):
        """ Plot Min Thickness between 2 material strings through time """
        x, y = self.GetThicknessMinTime(layer=layer)
        ax = plt.gca()
        plt.plot(x, y)
        plt.xlim(min(x), max(x))
        if(xlim):
            plt.xlim(xlim)
        return ax

    def plot_material_layer_maxthickness_through_time(
            self,
            layer="crust",
            xlim=None):
        """ Plot Max Thickness between 2 material strings through time """
        x, y = self.get_material_layer_maxthickness_through_time(layer=layer)
        ax = plt.gca()
        plt.plot(x, y)
        plt.xlim(min(x), max(x))
        if(xlim):
            plt.xlim(xlim)
        return ax

    def get_eulerian_temperature_profile_at_X(self, step, target):
        """ Get Temperature Profile at X """
        T, x, y = self.get_eulerian_temperature_field(step)
        # Extract Top
        xtop = x[:, 0] * 1e-3
        # Find Closest value index in xtop array
        idx = np.argmin(np.abs(xtop - target))
        # Get depths Values
        return y[idx, :] * 1e-3 - self.model_height, T[idx, :] - 273

    def plot_eulerian_temperature_profile_at_X(self, step, target):
        """ Plot Temperature Profile at X """
        z, T = self.get_eulerian_temperature_profile_at_X(step, target)
        return plt.plot(T, z)

    def get_eulerian_horizontal_velocity_profile_at_X(self, step, target):
        """ Get Vx Profile at X """
        x = self.get_eulerian_xcoordinates(step)
        y = self.get_eulerian_ycoordinates(step)
        Vx = self.get_eulerian_horizontal_velocity(step)
        # Extract Top
        xtop = x[:, 0] * 1e-3
        # Find Closest value index in xtop array
        idx = np.argmin(np.abs(xtop - target))
        # Get depths Values
        return y[idx, :] * 1e-3 - self.model_height, Vx[idx, :]

    def plot_eulerian_horizontal_velocity_profile_at_X(self, step, target):
        """ Plot Vx Profile at X """
        z, Vx = self.get_eulerian_horizontal_velocity_profile_at_X(step, target)
        return plt.plot(Vx, z)

    def plot_eulerian_horizontal_velocity_profile_leftborder(self, step):
        """ Plot Vx Profile at Left border """
        z, Vx = self.get_eulerian_horizontal_velocity_profile_at_X(step, 0.)
        x = np.zeros(z.shape)
        Vz = np.zeros(z.shape)
        interval = np.arange(0, z.size, 10)
        plt.quiver(x[interval], z[interval], Vx[interval], Vz[interval],
                   scale_units='xy', angles='xy', scale=1, width=0.001)
        return plt.plot(Vx, z)

    def plot_eulerian_horizontal_velocity_profile_rightborder(self, step):
        """ Plot Vx Profile at Right border """
        z, Vx = self.get_eulerian_horizontal_velocity_profile_at_X(
            step, self.model_width)
        return plt.plot(Vx, z)

    def calculate_lithostaticPressure(self, density, depth):
        """ Calculate Lithostatic Pressure """
        return density*constant.g*depth

    def plot_frictional_plastic_strain_weakening(self, material):
        """ Plot Strain Weakening function"""
        data = self.GetMaterialParams(material)["friction"]
        e1, e2 = data["phie1"], data["phie2"]
        phi1, phi2 = data["phi1"], data["phi2"]

        e = np.linspace(0, 2.0, 1001)
        de = e[1] - e[0]

        phi = []
        for ei in e:
            if ei < e1:
                phi.append(phi1)
            elif e1 <= ei < e2:
                phi.append(phi[-1]+(phi1-phi2)/(e1-e2)*de)
            else:
                phi.append(phi2)

        plt.plot(e, phi, "k-", linewidth=2)
        ax = plt.gca()
        plt.xlabel(r'$\varepsilon$',size=34)
        plt.ylabel(r'$\phi_{eff}$ $(\varepsilon_p)$',size=34)
        ax.set_yticks([phi1,(phi2+phi1)/2.0,phi2])
        ax.set_xticks([0,e1,(e1+e2)/2.0,e2, 2.0])
        plt.xlim(e[0],e[-1])
        plt.ylim(phi2-1,phi1+1)
        return ax

    def animate(self, f, output="video.mp4", dpi=600, cmap=None,
                log=False, visugrid=False, xlim=None, ylim=None, **kwargs):

        def update(i):
            # This is where new data is inserted into the plot.
            C, x, y = f(self.steps[i])
            # Distances are converted to meters
            y = y*1e-3 - self.model_height
            x *= 1e-3
            # Clear the plot
            plt.cla()
            if log:
                plt.pcolormesh(x, y, C, norm=LogNorm(vmin=vmin, vmax=vmax))
            else:
                plt.pcolormesh(x, y, C, cmap=cmap, vmin=vmin, vmax=vmax, **kwargs)
            plt.axis("scaled")
            plt.ylim((ymin, ymax))
            # plt.xlim((xmin, xmax))
            if xlim:
                plt.xlim(xlim)
            if ylim:
                plt.ylim(ylim)
            if visugrid:
                self.plot_visugrid(self.steps[i])
            time = self.get_time(self.steps[i])
            plt.title("Time: %5.1f Myr" % time)

        # Set up formatting for the movie files
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=15, bitrate=1800)

        fig, ax = plt.subplots()
        ax.spines["top"].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')

        # We need to prime the pump, so to speak and create
        # a quadmesh for plt to work with
        C, x, y = f(self.steps[0])
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        vmin, vmax = self.get_range_fieldVal(f)

        ymin, ymax = self.get_range_fieldVal(self.get_eulerian_ycoordinates)
        # xmin, xmax = get_range_fieldVal(get_eulerian_xcoordinates)
        ymin = ymin*1e-3 - self.model_height
        ymax = ymax*1e-3 - self.model_height
        ymax += 10
        # xmin *= 1e-3
        # xmax *= 1e-3

        if(log):
            vmin, vmax = 1e-21, 1e-13
            plt.pcolormesh(x, y, C, norm=LogNorm(vmin=vmin, vmax=vmax))
        else:
            plt.pcolormesh(x, y, C, cmap=cmap, vmin=vmin, vmax=vmax, **kwargs)

        plt.colorbar(orientation="horizontal")
        if(visugrid):
            self.plot_visugrid(self.steps[0])

        plt.ylim((ymin, ymax))
        # plt.xlim((xmin, xmax))

        plt.axis("scaled")
        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)
        plt.title("Step: "+str(self.steps[0]))

        anim = animation.FuncAnimation(fig, update, frames=range(1, len(self.steps)),
                                       blit=False)
        anim.save(output, writer=writer, dpi=dpi)
        plt.close(fig)

    def animate_eulerian_material_colors(self, output="Materials.mp4",
                                         cmap=None, xlim=None,
                                         ylim=None, **kwargs):
        if cmap is None:
            cmap = self.cmap
        f = self.get_eulerian_material_colors
        bounds = np.linspace(1, cmap.N+1, cmap.N+1)
        norm = colors.BoundaryNorm(bounds, cmap.N)
        self.animate(f, cmap=cmap, norm=norm, output=output, xlim=xlim,
                     ylim=ylim, **kwargs)
        return 1

    def animate_eulerian_temperature_field(self, output="Temperature.mp4", xlim=None,
                                           ylim=None, **kwargs):
        f = self.get_eulerian_temperature_field
        self.animate(f, output=output, xlim=xlim, ylim=ylim, **kwargs)
        return 1

    def animate_eulerian_density_field(self, output="Density.mp4", xlim=None, ylim=None,
                                       **kwargs):
        f = self.get_eulerian_density_field
        self.animate(f, output=output, xlim=xlim, ylim=ylim, **kwargs)
        return 1

    def animate_eulerian_strain_rate_field(self, output="StrainRate.mp4", xlim=None,
                                           ylim=None, **kwargs):
        f = self.get_eulerian_strainRate_field
        self.animate(f, output=output, log=True, xlim=xlim, ylim=ylim, **kwargs)
        return 1

    def animate_eulerian_pressure_field(self, output="Pressure.mp4", xlim=None,
                                        ylim=None, **kwargs):
        f = self.get_eulerian_pressure_field
        self.animate(f, output=output, xlim=xlim, ylim=ylim, **kwargs)
        return 1

    def animate_eulerian_lithostatic_pressure_field(self, output="LithoPress.mp4",
                                                    xlim=None, ylim=None, **kwargs):
        f = self.get_lithostatic_pressure
        self.animate(f, output=output, xlim=xlim, ylim=ylim, **kwargs)
        return 1

    def animate_all_fields(self, names={"Materials": "Materials.mp4",
                                        "Temperature": "Temperature.mp4",
                                        "Density": "Density.mp4",
                                        "Strain Rate": "StrainRate.mp4",
                                        "Pressure": "Pressure.mp4",
                                        "LithoPress": "LithoPress.mp4"},
                           xlim=None, ylim=None, **kwargs):
        print("Plotting Materials")
        self.animate_eulerian_material_colors(output=names["Materials"], xlim=xlim,
                                         ylim=ylim, **kwargs)
        print("Plotting Temperature")
        self.animate_eulerian_temperature_field(output=names["Temperature"], xlim=xlim,
                                           ylim=ylim, **kwargs)
        print("Plotting Density")
        self.animate_eulerian_density_field(
            output=names["Density"],
            xlim=xlim,
            ylim=ylim,
            **kwargs)
        print("Plotting Strain Rate")
        self.animate_eulerian_strain_rate_field(
            names=["Strain Rate"],
            xlim=xlim,
            ylim=ylim,
            **kwargs)
        print("Plotting Pressure")
        self.animate_eulerian_pressure_field(
            output=names["Pressure"],
            xlim=xlim,
            ylim=ylim,
            **kwargs)
        print("Plotting Lithostatic Pressure")
        self.animate_eulerian_lithostatic_pressure_field(
            output=names["LithoPress"],
            xlim=xlim,
            ylim=ylim,
            **kwargs)

    def animate_elevation(self, title="", output=None):
        """ animate Topography """
        def animate(i):
            C = f(self.steps[i])[1]
            line.set_ydata(C)  # update the data
            time = self.get_time(self.steps[i])
            plt.title(title+" at Time: %5.1f Myr" % time)
            plt.xlabel("Distance along Model (km)")
            plt.ylabel("Elevation (km)")
            return line,

        # Init only required for blitting to give a clean state.
        def init():
            line.set_ydata(np.ma.array(f(self.steps[0])[0], mask=True))
            time = self.get_time(self.steps[0])
            plt.title(title+" at Time: %5.1f Myr" % time)
            plt.xlabel("Distance along Model (km)")
            plt.ylabel("Elevation (km)")
            return line,

        fig, ax = plt.subplots()
        ax.set_ylim(-6, 6)

        # We need to prime the pump, so to speak and create
        # a quadmesh for plt to work with
        f = self.get_freesurface_elevation
        x, y = f(self.steps[0])
        line, = ax.plot(x, y)

        ani = animation.FuncAnimation(fig, animate, np.arange(1, len(self.steps)),
                                      init_func=init,
                                      interval=150, blit=False)

        # Set up formatting for the movie files
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=15, bitrate=1800)
        if output:
            ani.save(output, writer=writer, dpi=600)
        else:
            plt.show()
        plt.close(fig)

    #def Viewer2d(step, option="Temperature", path="."):
    #    z, x, y = get_eulerian_temperature_field(step)
    #    return viewer_2d(x, y, z)

    def get_array_top_row(self, Array):
        return Array[:, 0]

    def get_array_bottom_row(self, Array):
        return Array[:, -1]

    def get_eulerian_top_material_colors(self, step):
        Mat, x, y = self.get_eulerian_material_colors(step)
        return Mat[:, 0]

    def get_mechanical_iterations(self, filename="nohup.out"):
        steps = []
        errors = []
        with open(self.path+"/"+filename, "r") as f:
            for line in f:
                if line.startswith(" e="):
                    values = line.split()
                    errors.append(float(values[1]))
                    steps.append(int(values[4]))

        # Create a dictionary
        dico = {str(e): [] for e in list(set(steps))}

        for i, elem in enumerate(steps):
            dico[str(elem)].append(errors[i])

        nb_iterations = [len(dico[str(elem)]) for elem in list(set(steps))]

        return dico, nb_iterations

    def get_thermal_iterations(self, filename="nohup.out"):
        step = 0
        steps = []
        errors = []
        with open(self.path+"/"+filename, "r") as f:
            for line in f:
                if line.startswith(" thermal iterations"):
                    values = line.split()
                    if values[2] is "1":
                        step += 1
                        while next(f).startswith(" thermal iterations"):
                            errors.append(float(values[3]))
                            steps.append(step)

        # Create a dictionary
        dico = {str(e): [] for e in list(set(steps))}

        for i, elem in enumerate(steps):
            dico[str(elem)].append(errors[i])

        nb_iterations = [len(dico[str(elem)]) for elem in list(set(steps))]

        return dico, nb_iterations

    def plot_mechanical_iterations_through_time(self, filename="nohup.out"):
        mechanical = self.get_mechanical_iterations()
        ax = plt.plot(mechanical[1], color="blue", label="Mechanical")
        plt.ylabel("Nb iterations")
        plt.xlabel("Steps")
        plt.title("Mechanical Iterations")
        ya = plt.gca().yaxis
        ya.set_major_locator(plt.MaxNLocator(integer=True))
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        return ax

    def plot_thermal_iterations_through_time(self, filename="nohup.out"):
        thermal = self.get_thermal_iterations()
        ax = plt.plot(thermal[1], color="red", label="Thermal")
        plt.ylabel("Nb iterations")
        plt.xlabel("Steps")
        plt.title("Thermal iterations")
        plt.ylim(0,max(10, len(thermal)))
        ya = plt.gca().yaxis
        ya.set_major_locator(plt.MaxNLocator(integer=True))
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        return ax

    def plot_mechanical_iterations_vs_maxVy(self, filename="nohup.out"):
        mechanical = self.get_mechanical_iterations()
        ax = plt.plot(mechanical[1], color="blue", label="Mechanical")
        plt.ylabel("Nb iterations")
        plt.twinx()
        steps, Vy = self.get_eulerian_maxVy_evolution()
        plt.plot(steps, Vy, label="Vy", color="red")
        plt.legend()
        return ax

    def plot_mechanical_convergence_step(self, step):
        Mechanical = self.get_mechanical_iterations()[0]
        ax = plt.plot(Mechanical[str(step)], color = "red")
        plt.xlabel("Number of steps")
        plt.ylabel("Error value")
        plt.title("Mechanical solve at step"+str(step))
        return ax

    def plot_thermal_convergence_step(self, step):
        Thermal = self.get_mechanical_iterations()[0]
        ax = plt.plot(Thermal[str(step)], color = "red")
        plt.xlabel("Number of steps")
        plt.ylabel("Error value")
        plt.title("Thermal solve at step"+str(step))
        return ax

    def plot_fastscape_solve_time(self, filename="nohup.out", output=None):
        with open(self.path+"/"+filename, 'r') as f:
            targets = [float(line.split()[-1]) for line in f if "elapsed" in line]
        ax = plt.plot(targets)
        plt.xlabel("Steps")
        plt.ylabel("Time (s)")
        plt.title("FastScape Solve time")
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        if(output):
            plt.savefig(output)
        return ax

    def plot_pressure_pump_deltavel(self, filename="nohup.out", output=None):
        with open(filename, 'r') as f:
            targets = [float(line.split()[5]) for line in f if "delta_vel" in line]
        ax = plt.plot(targets, color="red")
        plt.ylabel("Velocity")
        plt.twinx()
        Avg = self.Get_avglithob_with_time(filename)
        plt.plot(Avg, label="Avg", color="green")
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        plt.xlabel("Steps")
        plt.ylabel("Avg Pressure")
        if(output):
            plt.savefig(output)
        return ax

    def plot_average_temperature(self, filename="nohup.out"):
        with open(filename, 'r') as f:
            targets = [float(line.split()[2]) for line in f if "avg temp." in line]
        ax = plt.plot(targets, color="red")
        plt.ylabel("Temperature")
        plt.xlabel("Steps")
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        return ax

    def get_strain_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: Strain min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("Strain: min=" in line)]
        strain_min = [elem[0] for elem in targets]
        strain_max = [elem[1] for elem in targets]
        return strain_min, strain_max

    def get_Vx_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: Vx min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("Vx: min=" in line)]
        Vx_min = [elem[0] for elem in targets]
        Vx_max = [elem[1] for elem in targets]
        return Vx_min, Vx_max

    def get_Vy_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: Vy min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("Vy: min=" in line)]
        Vy_min = [elem[0] for elem in targets]
        Vy_max = [elem[1] for elem in targets]
        return Vy_min, Vy_max

    def get_pressure_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: pressure min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("Pressure: min=" in line)]
        press_min = [elem[0] for elem in targets]
        press_max = [elem[1] for elem in targets]
        return press_min, press_max

    def get_th_dt(self):
        """ Extract information from standard input: pressure min, max"""
        filename = self.path+"/nohup.out"
        secinyr = 3.15*1e7
        with open(filename, 'r') as f:
            targets = [float(line.split()[1])/secinyr
                        for line in f if ("th_dt:" in line)]
        targets = np.array(targets)
        print("Min: {0} Mean: {1}  Max: {2}".format(targets.min(),
            targets.mean(), targets.max()))
        return targets

    def get_temperature_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: temperature min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("Temperature: min=" in line)]
        temp_min = [elem[0] for elem in targets]
        temp_max = [elem[1] for elem in targets]
        return temp_min, temp_max

    def get_rhoel_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: rhoel min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("rhoel: min=" in line)]
        rhoel_min = [elem[0] for elem in targets]
        rhoel_max = [elem[1] for elem in targets]
        return rhoel_min, rhoel_max

    def get_e2d_minmax(self, filename="nohup.out"):
        """ Extract information from standard input: e2d min, max"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[2]), float(line.split()[4]))
                       for line in f if ("e2d: min=" in line)]
        e2d_min = [elem[0] for elem in targets]
        e2d_max = [elem[1] for elem in targets]
        return e2d_min, e2d_max

    def get_vxdown_left(self, filename="nohup.out"):
        """ Extract information from standard input: Vxdown left"""
        with open(filename, 'r') as f:
            targets = [(float(line.split()[3]), float(next(f).split()[0]))
                       for line in f if ("vxleft_down" in line
                       and "vxright_down" in line)]

        vxleft_down = [elem[0] for elem in targets]
        return vxleft_down

    def get_vxdown_right(self, filename="nohup.out"):
        with open(filename, 'r') as f:
            targets = [(float(line.split()[3]), float(next(f).split()[0]))
                       for line in f if "vxleft_down" in line
                       and "vxright_down" in line]

        vxright_down = [elem[1] for elem in targets]
        return vxright_down

    def plot_down_velocities(self, filename="nohup.out"):
        vxleft_down = self.get_vxdown_left()
        vxright_down = self.get_vxdown_right()
        plt.plot(vxleft_down, color="red", label="left")
        plt.plot(vxright_down, color="blue", label="right")
        return

    def plot_pressure_pump_ref_plithob_avg(self, filename="nohup.out", output=None):
        with open(filename, 'r') as f:
            targets = [float(line.split()[2]) for line in f if "ref_plithob_avg" in line]

    def Get_minlithob_with_time(self, filename="nohup.out"):
        MinLithob = []
        with open(filename, 'r') as f:
            for line in f:
                if "max_plithob" in line:
                    MinLithob.append((float(line.split()[0])*1e-9, int(line.split()[2])))
        return MinLithob

    def Get_maxlithob_with_time(self, filename="nohup.out"):
        MaxLithob = []
        with open(filename, 'r') as f:
            for line in f:
                if "max_plithob" in line:
                    line2 = next(f)
                    MaxLithob.append((float(line2.split()[0])*1e-9, int(line2.split()[2])))
        return MaxLithob

    def Get_avglithob_with_time(self, filename="nohup.out"):
        AvgLithob = []
        with open(filename, 'r') as f:
            for line in f:
                if "avg_plithob" in line:
                    line2 = next(f)
                    AvgLithob.append(float(line2.split()[0])*1e-9)
        return AvgLithob

    def get_sealevel_coordinates(self, step, threshold=None, sealevel=None):
        xtop, ytop = self.get_freesurface_elevation(step)
        size = len(xtop)
        n = (size - 1) // 2

        if sealevel == None:
            sealevel = self.sealevel*1e-3

        leftx = xtop[:n]
        lefty = ytop[:n]
        lx = np.argmax(lefty)

        if leftx[lx] < threshold:
            leftseax = None
            leftmaxx = None
        else:
            leftx = np.array(leftx[lx:])
            lefty = np.array(lefty[lx:])
            sx = np.argmax(lefty < sealevel)
            leftseax = leftx[sx]
            leftmaxx = leftx[0]

        center = xtop[n]

        return leftseax, leftmaxx, center

    def get_sealevel_coordinates_with_time(self, threshold=None, sealevel=None):
        leftseax=[]
        leftmaxx=[]
        center=[]

        steps = self._find_available_steps("x1")
        for i, step in enumerate(steps):
            Clx, maxx, Clc = self.get_sealevel_coordinates(step, threshold=threshold, sealevel=sealevel)
            leftseax.append(Clx)
            leftmaxx.append(maxx)
            center.append(Clc)

        return leftseax, leftmaxx, center

    def get_maxelevation_coordinates(self, step, threshold=None):
        xtop, ytop = self.get_freesurface_elevation(step)
        size = len(xtop)
        n = (size - 1) // 2

        leftx = xtop[:n]
        lefty = ytop[:n]
        leftxmax = leftx[np.argmax(lefty)]
        if leftx[np.argmax(lefty)] < threshold:
           leftxmax = None
        rightx = xtop[-n:]
        righty = ytop[-n:]
        rightxmax = rightx[np.argmax(righty)]
        if rightx[np.argmax(righty)] < threshold:
           rightxmax = None
        center = xtop[n]

        return leftxmax, rightxmax, center

    def get_maxelevation_coordinates_with_time(self, threshold=None):
        leftxmax=[]
        rightxmax=[]
        center=[]

        steps = self._find_available_steps("x1")
        for i, step in enumerate(steps):
            xl, xr, xc = self.get_maxelevation_coordinates(step,
                threshold=threshold)
            leftxmax.append(xl)
            rightxmax.append(xr)
            center.append(xc)

        return leftxmax, rightxmax, center

    def plot_maxminlithob(self, filename="nohup.out", output=None):
        """ Plot evolution of min, max and avg lithostatic pressure at
            the base of the model
        """
        Max = self.Get_maxlithob_with_time(filename)
        Min = self.Get_minlithob_with_time(filename)
        Avg = self.Get_avglithob_with_time(filename)
        MaxLithob = [elem[0] for elem in Max]
        MinLithob = [elem[0] for elem in Min]
        plt.plot(MaxLithob, label="Max", color="red")
        plt.plot(MinLithob, label="Min", color="blue")
        plt.plot(Avg, label="Avg", color="green")
        plt.ylabel("Pressure (GPa)")
        plt.xlabel("Steps")
        plt.title("Lithostatic Pressure")
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        plt.legend(frameon=False, prop={'size':6})
        if(output):
            plt.savefig(output)
        return

    def plot_maxminlithob_loc(self, filename="nohup.out", output=None):
        """ Plot location (col) of min, max and avg lithostatic pressure at
            the base of the model
        """
        Max = self.Get_maxlithob_with_time(filename)
        Min = self.Get_minlithob_with_time(filename)
        MaxLithobCol = [elem[1] for elem in Max]
        MinLithobCol = [elem[1] for elem in Min]
        plt.plot(MaxLithobCol, label="Max", color="red")
        plt.plot(MinLithobCol, label="Min", color="blue")
        plt.ylim(0, self.eulerian_dims[0])
        plt.ylabel("Col number")
        plt.xlabel("Steps")
        plt.title("Lithostatic Pressure")
        xa = plt.gca().xaxis
        xa.set_major_locator(plt.MaxNLocator(4))
        plt.legend(frameon=False, prop={'size':6})
        if(output):
            plt.savefig(output)
        return

    def get_pressure_bottom(self, step):
        """ Plot the pressure at the base of the model """
        x = self.get_eulerian_xcoordinates(step)
        x = self.get_array_bottom_row(x)
        Pressure, dum, dum = self.get_eulerian_pressure_field(step)
        Pressure = self.get_array_bottom_row(Pressure)
        return x, Pressure

    def get_pressure_bottom_reference_column(self):
        x, Pressure = self.get_pressure_bottom(1)
        col = self.GetBoundaryConditionsParams()["reference_pressure_column"]
        col -= 1
        return Pressure[col]

    def get_lithostatic_pressure_bottom_reference_column(self):
        x, Pressure = self.get_lithostatic_pressure_bottom(1)
        col = self.GetBoundaryConditionsParams()["reference_pressure_column"]
        col -= 1
        return Pressure[col]

    def get_pressure_bottom_mean_value(self, step):
        x, Pressure = self.get_pressure_bottom(step)
        return Pressure.mean()

    def plot_equilibrium(self, step):
        litho = self.get_lithostatic_pressure_bottom(step)
        delta = litho - self.get_lithostatic_pressure_bottom_reference_column()

    def get_lithostatic_pressure_bottom_mean_value(self, step):
        x, Pressure = self.get_lithostatic_pressure_bottom(step)
        return Pressure.mean()

    def get_pressure_bottom_mean_value_evolution(self):
        Pressures = []
        times = []
        for step in self.steps:
            Pressures.append(self.get_pressure_bottom_mean_value(step)*1e-9)
            times.append(self.get_time(step))

        return times, Pressures

    def get_lithostatic_pressure_bottom_mean_value_evolution(self):
        Pressures = []
        times = []
        for step in self.steps:
            Pressures.append(self.get_lithostatic_pressure_bottom_mean_value(step)*1e-9)
            times.append(self.get_time(step))

        return times, Pressures

    def plot_pressure_bottom_mean_value_evolution(self, **kwargs):
        time, Pressures = self.get_pressure_bottom_mean_value_evolution()
        variations = [(Val / Pressures[1] - 1.0) * 100 for Val in Pressures]
        ax = plt.plot(time[1:], variations[1:], **kwargs)
        plt.title("Evolution of Mean Pressure at the base of the model")
        plt.xlabel("Time (Myr)")
        plt.ylabel("%% Variations (reference is first step)")
        return ax

    def plot_lithostatic_pressure_bottom_mean_value_evolution(self, **kwargs):
        time, Pressures = self.get_lithostatic_pressure_bottom_mean_value_evolution()
        variations = [(Val / Pressures[1] - 1.0) * 100 for Val in Pressures]
        ax = plt.plot(time[1:], variations[1:], **kwargs)
        plt.title("Evolution of Mean Lithostatic Pressure at the base of the model")
        plt.xlabel("Time (Myr)")
        plt.ylabel("%% Variations (reference is first step)")
        return ax

    def get_lithostatic_pressure(self, step):
        Y = self.get_eulerian_ycoordinates(step)
        # Calculate cell heights
        heights = (Y[:-1,:] + Y[1:,:])/2.0
        heights = np.abs(np.diff(heights, n=1, axis=1))
        # Get densities
        densities, x, y = self.get_eulerian_density_field(step)
        # Calculate masses
        lithop = heights * densities * constant.g
        lithop = np.cumsum(lithop, axis=1)
        return lithop, x, y

    def get_lithostatic_pressure_bottom(self, step):
        x = self.get_eulerian_xcoordinates(step)
        x = self.get_array_bottom_row(x)
        x = x[: -1]
        Pressure, dum, dum = self.get_lithostatic_pressure(step)
        Pressure = self.get_array_bottom_row(Pressure)
        return x, Pressure

    def plot_pressure_bottom(self, step, **kwargs):
        x, y = self.get_pressure_bottom(step)
        x *= 1e-3
        A = plt.plot(x, y*1e-9, **kwargs)
        plt.xlabel("Distance along model")
        plt.ylabel("Pressure (GPa)")
        return A

    def plot_lithostatic_pressure_bottom(self, step, **kwargs):
        x, y = self.get_lithostatic_pressure_bottom(step)
        x *= 1e-3
        A = plt.plot(x, y*1e-9, **kwargs)
        plt.xlabel("Distance along model")
        plt.ylabel("Lithostatic Pressure (GPa)")
        return A

    def PlotRheologyModel(self, materials):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        stress, Z = self.YieldEnveloppeCompression(
            materials, [
                0, 25000, 35000, 125000, 600000], [
                50, 50, 100, 100])
        plt.plot(stress, -Z)
        plt.ylim(-140, 0)
        ax.set_aspect(15)
        plt.xlim(0, 800)
        plt.show()

    def plot_diffusionCoef_with_depth(self,
                                      H=np.linspace(-1000, 0, 100),
                                      Ks=100.0, sealevel=0., Hscale=1000.):
        def K(H, Ks, sealevel, Hscale):
            return Ks*exp((H - sealevel)/Hscale)

        K = K(H, Ks, sealevel, Hscale)

        plt.plot(abs(H-sealevel), K)
        plt.xlabel("Depth (m)")
        plt.ylabel("K")
        plt.show()

    def create_Paraview_VTSFile(self, step):
        nx, ny = self.get_eulerian_grid_size(step)
        color, x, y = self.get_eulerian_material_colors(step)
        color = color.reshape((nx-1, ny-1, 1)).T

        # Get Data
        x = self.get_eulerian_xcoordinates(step)
        z = self.get_eulerian_ycoordinates(step)
        x = x.reshape((nx, ny, 1))
        z = z.reshape((nx, ny, 1))
        y = np.zeros((nx, ny, 1))
        y = y.astype(np.float32, copy=False)
        # Convert to Km
        x = x * 1e-3
        z = z * 1e-3
        z = z - self.model_height

        Vx = self.get_eulerian_horizontal_velocity(step)
        Vy = self.get_eulerian_vertical_velocity(step)
        Vx = Vx.reshape((nx, ny, 1))
        Vy = Vy.reshape((nx, ny, 1))
        # Convert to mm/yr
        Vx = Vx * 1000 * 365 * 24 * 3600
        Vy = Vy * 1000 * 365 * 24 * 3600

        T, dum, dum = self.get_eulerian_temperature_field(step)
        T = T.reshape((nx, ny, 1))
        # Convert to Celsius
        T = T - 273

        P, dum, dum = self.get_eulerian_pressure_field(step)
        P = P.reshape((nx, ny, 1))
        # Convert to GPa
        P = P * 1e-9

        Density, dum, dum = self.get_eulerian_density_field(step)
        Density = Density.reshape((nx-1, ny-1, 1))

        strain, dum, dum = self.get_eulerian_strain_field(step)
        strain = strain.reshape((nx - 1, ny - 1, 1))

        (gridToVTK("./Step"+str(step).zfill(6), x, y, z,
                   cellData={"material": color,
                             "Strain": strain},
                   pointData={"Temperature": T,
                              "Pressure": P,
                              "Vx": Vx,
                              "Vy": Vy}))

    def create_Paraview_VTSFile_for_all_steps(self):
        g = VtkGroup("./group")
        for step in self.steps:
            self.create_Paraview_VTSFile(step)
            g.addFile(filepath=loops[step])
        g.save()

    #def compress_run_results(run):
    #    current = os.getcwd()+"/"
    #
    #    os.chdir(current + run + "/Sopale/r30/")
    #    binary = glob.glob("*")
    #    for i in binary:
    #        tar = tarfile.open(i+".tar.gz", "w: gz")
    #        tar.add(i)
    #        tar.close()
    #        os.remove(i)
    #
    #    os.chdir(current + run + "/matlab/")
    #    loops = glob.glob("loop.*")
    #
    #    steps = []
    #    for i in loops:
    #        steps.append(os.path.splitext(i)[1][1:])
    #
    #    for i in steps:
    #        tar = tarfile.open("step"+i+".tar.gz", "w: gz")
    #        for filename in glob.glob("*"+i+"*"):
    #            tar.add(filename)
    #        tar.close()
    #
    #    for i in glob.glob("*"):
    #        filename, extension = os.path.splitext(i)
    #        if extension != ".gz":
    #            os.remove(i)
    #
    #    os.chdir(current)
    #    tar = tarfile.open(run+".tar.gz", "w: gz")
    #    tar.add(run)
    #    tar.close()

    def PlotMaterialAndVelocityBorders(self, step):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        im = self.plot_eulerian_material_colors(step)
        ax.set_yticks(ticks=[])
        ax.set_ylabel("")

        # create an axes on the right side of ax. The width of cax will be 5%
        # of ax and the padding between cax and ax will be fixed at 0.05 inch.
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("left", size="5%", pad=0.05)

        vleft, = self.plot_eulerian_horizontal_velocity_profile_leftborder(step)
        vleft.set_color("b")
        cax1.add_line(vleft)
        cax1.set_ylim(-600, 100)
        cax1.set_xlim(-2.5e-10, 1e-10)
        # cax1.set_xlabel("")
        # cax1.set_ylabel("")
        # cax1.set_xticks(ticks=[])
        # cax1.set_yticks(ticks=[])
        # cax1.set_axis_off()
        # cax1.spines["top"].set_visible(False)
        # cax1.spines["right"].set_visible(False)
        cax1.set_frame_on(False)
        cax1.xaxis.set_visible(False)
        cax1.yaxis.set_visible(False)

        cax2 = divider.append_axes("right", size="5%", pad=0.05)
        vright, = self.plot_eulerian_horizontal_velocity_profile_rightborder(step)
        vright.set_color("b")
        cax2.add_line(vright)
        cax2.set_ylim(-600, 100)
        # cax2.set_xlabel("")
        # cax2.set_ylabel("")
        # cax2.set_xticks(ticks=[])
        # cax2.set_yticks(ticks=[])
        cax2.set_frame_on(False)
        cax2.xaxis.set_visible(False)
        cax2.yaxis.set_visible(False)

        plt.show()

    def PlotMatAndStrain(self, step, theme='dark'):
        # The idea here is to do a plot that combine Material as hue
        # and strain as luminance.

        # 1) Get Material, StrainRate, X, Y,  arrays
        materials, X, Y = self.get_eulerian_material_colors(step)
        strain, dum, dum = self.get_eulerian_strain_field(step)
        X *= 1e-3
        Y *= 1e-3
        # 2) Apply colormap to the material array
        # The material label starts at 1 while the index in the colormap start at 0
        # one need to substract 1 to the material value to fit with the appropriate color
        # we End up with a rgba (red, green, blue, alpha) array
        cmap = self.cmap
        rgba = cmap(materials-1)
        rgb = rgba[..., : -1]

        # Convert to HSV
        hsv = colors.rgb_to_hsv(rgb)

        # Now is the interesting part. We want to encode the Strain into Luminance...
        # Materials give Hue
        # StrainRate give S and V
        absmax = np.abs(strain).max()

        hsv2 = np.zeros(strain.shape + (3, ), dtype='float')
        hsv2[..., 0] = hsv[..., 0]

        if theme == 'light':
            hsv2[..., 1] = np.clip(np.abs(strain) / absmax, 0, 1)
            hsv2[..., 2] = 1.0
        elif theme == 'dark':
            hsv2[..., 1] = hsv[..., 1]
            hsv2[..., 2] = 1.0 - np.clip(np.abs(strain) / absmax, 0, 1)

        rgb = colors.hsv_to_rgb(hsv2)

        color_tuple = rgb.reshape((rgb.shape[0]*rgb.shape[1], rgb.shape[2]))
        # I guess the color_tuple line might be non-obvious: essentially we need to turn
        # the (n, m, 4) array into an (n*m, 4) array.
        # It's also worth noting that the colors we pass in need to be
        # floating-point, between 0 and 1.
        m = plt.pcolormesh(X, Y, materials, color=color_tuple, linewidth=1)
        plt.xlim(400, 800)
        m.set_array(None)
        return m

    def get_centroid_coordinates(self, step):
        X = self.get_eulerian_xcoordinates(step)
        Y = self.get_eulerian_ycoordinates(step)

        offsets = []
        offsets.append((0,  0))  # left top
        offsets.append((0, -1))  # right top
        offsets.append((-1,  0))  # left bottom
        offsets.append((-1, -1))  # right bottom

        stackedX = np.dstack(
            np.roll(
                np.roll(
                    X,
                    i,
                    axis=0),
                j,
                axis=1) for i,
            j in offsets)
        X = np.mean(stackedX, axis=2)
        stackedY = np.dstack(
            np.roll(
                np.roll(
                    Y,
                    i,
                    axis=0),
                j,
                axis=1) for i,
            j in offsets)
        Y = np.mean(stackedY, axis=2)

        return X[0: -1, 0: -1], Y[0: -1, 0: -1]

    def get_overpressure(self, step):
        P, X, Y = self.get_eulerian_pressure_field(step)
        Xcells, Ycells = self.get_centroid_coordinates(step)
        P = griddata(
            (X.ravel(), Y.ravel()), P.ravel(),
            (Xcells.ravel(), Ycells.ravel())).reshape(
            Xcells.shape)
        Pl, X, Y = self.get_lithostatic_pressure(step)
        OP = P - Pl
        return OP, Xcells, Ycells

    @add_colorbar("Overpressure (MPa)")
    def plot_overpressure(
            self,
            step,
            xlim=None,
            ylim=None,
            output=None,
            colorbar="bottom"):
        """ Plot Overpressure: Pressure from Stoke - lithostatic pressure """
        # Temperature field
        P, x, y = self.get_overpressure(step)
        #vmin, vmax = get_range_fieldVal(get_overpressure)
        vmin = -5e8
        vmax = 5e8
        P *= 1e-6
        vmin *= 1e-6
        vmax *= 1e-6
        # Distances are converted to meters
        y = y*1e-3 - self.model_height
        x *= 1e-3

        orig_cmap = cm.coolwarm
        shifted_cmap = orig_cmap
        #shifted_cmap = shiftedColorMap(orig_cmap, midpoint=0.0, name='shifted')

        ax = plt.gca()
        A = plt.pcolormesh(
            x,
            y,
            P,
            rasterized=True,
            vmin=vmin,
            vmax=vmax,
            cmap=shifted_cmap)
        plt.axis("scaled")
        plt.xlim((x.min(), x.max()))

        if(xlim):
            plt.xlim(xlim)
        if(ylim):
            plt.ylim(ylim)

        time = self.get_time(step)
        plt.title("Overpressure at %5.1f Myr" % time)
        plt.xlabel("Distance along model (km)")
        plt.ylabel("Depth (km)")

        if(output):
            plt.savefig(output, dpi=600)
            plt.close()
        return ax

    def get_compositional_statistics(self, step):
        """ Get Some Material Statistics

        Return the percentage of cell of each material
        Note that this is different than the percentage of
        mass as the grid is usually non-uniform.
        """
        mat = self.get_eulerian_material_colors(step)[0]
        unique, counts = np.unique(mat, return_counts=True)
        percent = counts / mat.size * 100.0
        return {"Values": unique, "Counts": counts, "Percentage of Cells": percent}

    def get_freesurface_endpoints_elevation(self, step):
        xtop, ytop = self.get_freesurface_elevation(step)
        return (xtop[0], ytop[0]), (xtop[-1], ytop[-1])

    def get_freesurface_endpoints_elevation_evolution(self):
        time = []
        left = []
        right = []
        for step in self.steps:
            if(step != 0):
                time.append(self.get_time(step))
                left.append(self.get_freesurface_endpoints_elevation(step)[0][1])
                right.append(self.get_freesurface_endpoints_elevation(step)[1][1])
        return time, left, right

    def plot_freesurface_endpoints_elevation_evolution(self):
        time, left, right = self.get_freesurface_endpoints_elevation_evolution()
        ax = plt.gca()
        plt.plot(time, left, color="r", label="left")
        plt.plot(time, right, color="b", label="right")
        plt.ylabel("Elevation (m)")
        plt.xlabel("Time (Myr)")
        plt.title("Borders")
        plt.legend(frameon=False, prop={'size':6})
        return ax

    def get_velocity_stats_at_borders(self, step, border="left"):
        Vx = self.get_eulerian_horizontal_velocity(step) * 1e3 * 365 * 24 * 3600
        Vy = self.get_eulerian_horizontal_velocity(step) * 1e3 * 365 * 24 * 3600
        if border == "left":
            Vx = Vx[0, 1: -1]
            Vy = Vy[0, 1: -1]
            c = np.sqrt(Vx*Vx+Vy*Vy)
        if border == "right":
            Vx = Vx[-1, 1: -1]
            Vy = Vy[-1, 1: -1]
            c = np.sqrt(Vx*Vx+Vy*Vy)
        if border == "top":
            Vx = Vx[1: -1, 0]
            Vy = Vy[1: -1, 0]
            c = np.sqrt(Vx*Vx+Vy*Vy)
        if border == "bottom":
            Vx = Vx[1: -1, -1]
            Vy = Vy[1: -1, -1]
            c = np.sqrt(Vx*Vx+Vy*Vy)

        return {"Vx min": Vx.min(), "Vx max": Vx.max(), "Vx mean": Vx.mean(),
                "Vy min": Vy.min(), "Vy max": Vy.max(), "Vy mean": Vy.mean(),
                "Module Min": c.min(), "Module Max": c.max(),
                "Module Mean": c.mean()}

    def plot_layer_thickness(self, step, layer=None, strings=(1, 2)):
        options = {"uppercrust": (1, 2), "lowercrust": (2, 3),
                   "lithosphere": (1, 4), "crust": (1, 3)}
        string1, string2 = strings
        if layer:
            string1, string2 = options[layer]
        info = self.get_material_layer_thickness(step, string1, string2)
        original = self.get_material_layer_thickness(1, string1, string2)
        ax = plt.gca()
        plt.plot(info["x"]*1e-3, info["Thickness"]*1e-3)
        plt.plot(original["x"]*1e-3, original["Thickness"]*1e-3, color="r")
        plt.xlabel("Distance along model")
        plt.ylabel("Thickness (km)")
        return ax

    def plot_layer_thickness_at_all_steps(self, layer=None,
                                          strings=(1, 2), cmap="Blues"):
        options = {"uppercrust": (1, 2), "lowercrust": (2, 3),
                   "lithosphere": (1, 4), "crust": (1, 3)}
        string1, string2 = strings
        if layer:
            string1, string2 = options[layer]

        options = {"Blues": cm.Blues, "Reds": cm.Reds}
        cmap = options[cmap]
        # Remove step 0 (there is no loop file for step 0)
        steps = self.steps[1:]
        ax = plt.gca()
        colors = cmap(np.linspace(0, 1, len(steps)))

        for index, step in enumerate(steps):
            info = self.get_material_layer_thickness(step, string1, string2)
            plt.plot(info["x"]*1e-3, info["Thickness"]*1e-3, color=colors[index])
        return ax

        plt.xlabel("Distance along model")
        plt.ylabel("Thickness (km)")
        return ax

    def get_fluxes_at_border(self, step, border="left"):
        """Return fluxes in Km/s"""
        borders = {"left": 0, "right": self.model_width}
        # Get velocities at left border
        Z, Vx = self.get_eulerian_horizontal_velocity_profile_at_X(step, borders[border])
        # Calculate height of cells
        heights = abs(Z[1:] - Z[:-1])
        # Velocities are in m/s convert them to km/s
        meanVx = (Vx[1:] + Vx[:-1]) / 2.0 * 1e-3
        Flux_in = None
        Flux_out = None
        if border is "left":
            Flux_in = abs(sum(meanVx[meanVx > 0.] * heights[meanVx > 0.]))
            Flux_out = abs(sum(meanVx[meanVx < 0.] * heights[meanVx < 0.]))
        if border is "right":
            Flux_in = abs(sum(meanVx[meanVx < 0.] * heights[meanVx < 0.]))
            Flux_out = abs(sum(meanVx[meanVx > 0.] * heights[meanVx > 0.]))
        return {"Flux in": Flux_in, "Flux out": Flux_out, "Total": Flux_in - Flux_out}

    def get_fluxes_at_border_evolution(self, border="left"):
        Flux_in = []
        Flux_out = []
        Total = []
        Time = []
        for i, step in enumerate(self.steps):
            values = self.get_fluxes_at_border(step, border=border)
            Flux_in.append(values["Flux in"])
            Flux_out.append(values["Flux out"])
            Total.append(values["Total"])
            Time.append(self.get_time(step))
        return {"Flux in": Flux_in, "Flux out": Flux_out, "Total": Total,
                "Time": Time}

    def plot_fluxes_at_border_evolution(self, border="left"):
        values = self.get_fluxes_at_border_evolution(border)
        ax = plt.plot(values["Time"], values["Flux in"], color="g", label="Flux in")
        plt.plot(values["Time"], values["Flux out"], color="b", label="Flux out")
        # plt.plot(values["Time"], values["Total"], color="r", label="Balance")
        ymin = min([min(values["Flux in"][1:]), min(values["Flux out"][1:])])
        ymax = max([max(values["Flux in"][1:]), max(values["Flux out"][1:])])
        print(ymin, ymax)
        plt.ylim(ymin, ymax)
        plt.xlim(0., max(values["Time"]))
        plt.title("Fluxes at "+border+" border")
        plt.legend(frameon=False, prop={'size':6})
        return ax

    def get_model_total_volume(self, step):
        """Get Model volume (actually surface) in Km2"""
        X = self.get_eulerian_xcoordinates(step)
        Y = self.get_eulerian_ycoordinates(step)
        widths = abs(X[1:, :] - X[:-1, :])
        heights = abs(Y[:, 1:] - Y[:, :-1])
        volume = widths[:, :-1] * heights[:-1, :]
        return sum(sum(volume)) * 1e-6

    def get_model_total_mass(self, step):
        X = self.get_eulerian_xcoordinates(step)
        Y = self.get_eulerian_ycoordinates(step)
        widths = abs(X[1:, :] - X[:-1, :])
        heights = abs(Y[:, 1:] - Y[:, :-1])
        volume = widths[:, :-1] * heights[:-1, :]
        density = self.get_eulerian_density_field(step)[0]
        return sum(sum(volume*density))

    def get_model_total_volume_evolution(self):
        volume = []
        Time = []
        for i, step in enumerate(self.steps):
            volume.append(self.get_model_total_volume(step))
            Time.append(self.get_time(step))
        return Time, volume

    def get_model_total_mass_evolution(self):
        mass = []
        Time = []
        for i, step in enumerate(self.steps):
            mass.append(self.get_model_total_mass(step))
            Time.append(self.get_time(step))
        return Time, mass

    def plot_model_total_volume_evolution(self):
        Time, volume = self.get_model_total_volume_evolution()
        variations = [(Val / volume[0] - 1.0)*100 for Val in volume]
        ax = plt.plot(Time, variations, color="g", label="Volume")
        ymin = min(variations)
        ymax = max(variations)
        print(ymin, ymax)
        plt.ylim(ymin, ymax)
        plt.xlim(0., max(Time))
        plt.title('Model volume')
        plt.xlabel("Time (Myr)")
        plt.ylabel('% variation / step 0')
        return ax

    def plot_model_total_mass_evolution(self):
        Time, mass = self.get_model_total_mass_evolution()
        variations = [(Val / mass[1] - 1.0)*100 for Val in mass]
        ax = plt.plot(Time[1:], variations[1:], color="g", label="Volume")
        ymin = min(variations[1:])
        ymax = max(variations[1:])
        print(ymin, ymax)
        plt.ylim(ymin, ymax)
        plt.xlim(0., max(Time))
        plt.title('Evolution of model mass, in %')
        plt.xlabel("Time (Myr)")
        plt.ylabel('%% variation (reference is at step 1)')
        return ax

    def debug_plots(self, output="debug.pdf"):
        SetFullPage()

        pdf_doc = PdfPages(output)

        fig = plt.figure()
        plt.subplots_adjust(left=0.2, right=0.9, wspace=0.3, hspace=0.3)
        fig.add_subplot(221)
        try:
            self.plot_mechanical_iterations_through_time()
        except:
            pass
        fig.add_subplot(222)
        try:
            self.plot_thermal_iterations_through_time()
        except:
            pass
        fig.add_subplot(223)
        try:
            self.plot_maxminlithob()
        except:
            pass
        fig.add_subplot(224)
        try:
            self.plot_maxminlithob_loc()
        except:
            pass
        plt.suptitle(self.model_name)
        pdf_doc.savefig(fig)


        fig = plt.figure()
        plt.subplots_adjust(left=0.2, right=0.9, wspace=0.3, hspace=0.3)
        fig.add_subplot(221)
        try:
            self.plot_model_total_volume_evolution()
        except:
            pass
        fig.add_subplot(222)
        try:
            self.plot_fluxes_at_border_evolution()
        except:
            pass
        fig.add_subplot(223)
        try:
            self.plot_freesurface_endpoints_elevation_evolution()
        except:
            pass
        fig.add_subplot(224)
        try:
            self.plot_pressure_pump_deltavel()
        except:
            pass
        plt.suptitle(self.model_name)
        pdf_doc.savefig(fig)


        fig = plt.figure()
        plt.subplots_adjust(left=0.2, right=0.9, wspace=0.3, hspace=0.3)
        fig.add_subplot(221)
        try:
            self.plot_fastscape_solve_time()
        except:
            pass
        fig.add_subplot(222)
        try:
            self.plot_freesurface_elevation_evolution()
        except:
            pass
        fig.add_subplot(223)
        try:
            self.plot_average_temperature()
        except:
            pass
        fig.add_subplot(224)
        try:
            self.plot_down_velocities()
        except:
            pass
        plt.suptitle(self.model_name)
        pdf_doc.savefig(fig)
        pdf_doc.close()

        fig = plt.figure()
        plt.subplots_adjust(left=0.2, right=0.9, wspace=0.3, hspace=0.3)
        fig.add_subplot(221)
        try:
            self.plot_fluxes_at_border_evolution()
        except:
            pass
        fig.add_subplot(222)
        try:
            self.plot_fluxes_at_border_evolution(border="right")
        except:
            pass
        plt.suptitle(self.model_name)
        pdf_doc.savefig(fig)
        pdf_doc.close()

    def find_nearests_material_points(self, pts, step):
         """ Find the nearest particle at coordinates
         pts has to be a list of tuples (xi, yi)

         Returns: index of the nearest particle for each point
         coordinates in the pts list.
         """
         xl = self.get_lagrangian_xcoordinates(step)
         yl = self.get_lagrangian_ycoordinates(step)
         xl = xl[:,:50]
         yl = yl[:,:50]
         data = list(zip(xl.ravel(),yl.ravel()))
         tree = spatial.KDTree(data)

         nearests = []
         for pt in pts:
             pt = np.array(pt)
             dist, index = tree.query(pt)
             index = np.unravel_index(index, xl.shape)
             nearests.append(index)

         return nearests

    def find_paths_material_points(self, indexes, step):
        """ Return point locations at each step

            indexes: a python list of point indexes.
         """
        paths = []
        xls = []
        yls = []

        steps = [x for x in self.steps if x <= step and x != 0]
        xls = np.zeros((len(steps),len(indexes)))
        yls = np.zeros((len(steps),len(indexes)))

        for i, step in enumerate(steps):
            xl = self.get_lagrangian_xcoordinates(step)[:,:50]
            yl = self.get_lagrangian_ycoordinates(step)[:,:50]

            for j, index in enumerate(indexes):
                # Need to handle exception when the material point
                # does not exist. Can happen when a material point
                # has been injected.
                xls[i,j] = xl[index]
                yls[i,j] = yl[index]

        for j, index in enumerate(indexes):
            paths.append(list(zip(xls[:,j], yls[:, j])))

        return paths

    def find_temperatures_at_locations(self, pts, step):
        """ Given a list of points with coordinates (xi,yi)
            return the temperature of each point at step """
        xs = self.get_eulerian_xcoordinates(step)
        ys = self.get_eulerian_ycoordinates(step)
        temp,_,_ = self.get_eulerian_temperature_field(step)
        data = list(zip(xs.ravel(),ys.ravel()))
        tree = spatial.KDTree(data)

        temperatures = []
        for pt in pts:
            pt = np.array(pt)
            dist, index = tree.query(pt)
            index = np.unravel_index(index, xs.shape)
            temperatures.append(temp[index])

        return temperatures

    def retrieve_temperatures_paths(self, paths, step):
        """ Retrieve temperatures along a list of paths """

        # The call to find_temperatures_at_locations()
        # is costly so we want to avoid calling it for each
        # location at each steps. We can shorter the pb
        # by creating new lists of points at each step.

        T = []
        data = list(zip(*paths))

        steps = [x for x in self.steps if x <= step and x != 0]

        for i, step in enumerate(steps):
            T.append(self.find_temperatures_at_locations(data[i], step))

        TPaths = list(zip(*T))
        return TPaths, paths, steps

    def Extract_surface_TTPaths(self, step, filename="tTPaths.json"):
        """ Extract Time-Temperature paths for the surface points

        The Paths are from istep=0 to istep=step

        """
        try:
            TPaths = []
            Paths = []
            steps = []
            with open(filename, "r") as f:
                A = json.loads(f.read())
                for key in A.keys():
                    if step == A[key]["Steps"][-1]:
                        TPaths.append(A[key]["Temperature"])
                        Paths.append(A[key]["Path"])
                        steps.append(A[key]["Steps"])
                    else:
                        raise
            return TPaths, Paths, steps

        except:
            pass

        xtop = self.get_eulerian_xcoordinates(step)[:,0]
        ytop = self.get_eulerian_ycoordinates(step)[:,0]
        pts = list(zip(xtop,ytop))

        mpoints = self.find_nearests_material_points(pts, step)
        paths = self.find_paths_material_points(mpoints, step)
        TPaths, Paths, steps = self.retrieve_temperatures_paths(paths, step)

        TPaths_data = {}
        for index, TPath in enumerate(TPaths):
            TPaths_data["Point"+str(index)] = {
            "Temperature": TPath,
            "Path": Paths[index],
            "Steps": steps}


        with open(filename, "w") as f:
            f.write(json.dumps(TPaths_data))

        return TPaths, Paths, steps

    def Calculate_AFTA(self, step, output="AFT_data.json", initial_age=300):
        """ Calculate Fission Track data

        Arguments:

        step: The step at which we want to calculate the FT data
        output: output json file
        initial_age: initial age of the samples.
        """

        TPaths, paths, steps = self.Extract_surface_TTPaths(step)
        # Now we want to use the Ketcham annealing model to calculate
        # the AFTA and TLD
        dt = self.get_dt(step) # dt in Myr
        time = [step * dt for step in steps[0]]
        if(time[-1] < initial_age):
            time.append(time[-1] + initial_age)
        AFT_data = {}
        for index, TPath in enumerate(TPaths):
            # Create History object
            TPath = list(reversed(TPath)) # Reverse path (0 is recent time)
            TPath.append(TPath[-1])
            History = Thermal_history("History", time, TPath)
            History.Temperature =  [temp - 273.0 for temp in History.Temperature]
            data = KetchamModel(History)
            data["Coordinates"] = paths[index][-1]
            data["Temperatures"] = TPath
            data["Time"] = time
            data["Paths"] = paths[index]
            data["Fission Track length distribution"] = (
                data["Fission Track length distribution"][0].tolist(),
                data["Fission Track length distribution"][1].tolist())
            AFT_data["Sample"+str(index).strip()] = data

        # Need to save dictionary to json file
        with open(output,"w") as f:
            f.write(json.dumps(AFT_data))

        return AFT_data

    def Plot_AFT_AgeProfile(self, step, filename="AFT_data.json"):
        """ Plot Age transect at surface of the model"""

        try:
            with open(filename, "r") as f:
                AFT_data = json.loads(f.read())
        except:
            AFT_data = self.Calculate_AFTA(step)

        Ages = []
        X = []
        Y = []

        for Sample in AFT_data.keys():
            y = AFT_data[Sample]["Coordinates"][1]
            if y >= 600000.0-300.:
                Ages.append(AFT_data[Sample]["Final Age"])
                X.append(AFT_data[Sample]["Coordinates"][0])
                Y.append(AFT_data[Sample]["Coordinates"][1])

        Pos = [x*1e-3 for (x,y) in sorted(zip(X, Ages))]
        Ages = [y for (x,y) in sorted(zip(X, Ages))]
        Ages_hat = savgol_filter(np.array(Ages), 31, 3)
        ax = plt.plot(Pos, Ages)
        ax = plt.plot(Pos, Ages_hat, color="r")
        plt.xlabel("Distance")
        plt.ylabel("AFTA Age in Myr")
        plt.title("AFTA Prediction at step "+str(step))
        return ax

    def Plot_AFT_MTLProfile(self, step, filename="AFT_data.json"):
        """ Plot Mean Track Length transect at surface of the model"""

        try:
            with open(filename, "r") as f:
                AFT_data = json.loads(f.read())
        except:
            AFT_data = self.Calculate_AFTA(step)

        MTLs = []
        X = []
        Y = []

        for Sample in AFT_data.keys():
            y = AFT_data[Sample]["Coordinates"][1]
            if y >= 600000.0-300.:
                MTLs.append(AFT_data[Sample]["Mean Track Length"])
                X.append(AFT_data[Sample]["Coordinates"][0])
                Y.append(AFT_data[Sample]["Coordinates"][1])

        Pos = [x*1e-3 for (x,y) in sorted(zip(X, MTLs))]
        MTL = [y for (x,y) in sorted(zip(X, MTLs))]
        MTL_hat = savgol_filter(np.array(MTL), 31, 3)
        ax = plt.plot(Pos, MTL)
        ax = plt.plot(Pos, MTL_hat, color="r")
        plt.xlabel("Distance")
        plt.ylabel("Mean Track Length")
        plt.title("MTL Prediction at step "+str(step))
        return ax

    def Plot_AFTA_MTL_banana(self, step, filename="AFT_data.json"):
        try:
            with open(filename, "r") as f:
                AFT_data = json.loads(f.read())
        except:
            AFT_data = self.Calculate_AFTA(step)

        Ages = []
        MTLs = []
        X = []
        Y = []

        for Sample in AFT_data.keys():
            Ages.append(AFT_data[Sample]["Final Age"])
            MTLs.append(AFT_data[Sample]["Mean Track Length"])
            X.append(AFT_data[Sample]["Coordinates"][0])
            Y.append(AFT_data[Sample]["Coordinates"][1])

        MTL = [y for (x,y) in sorted(zip(X, MTLs))]
        Ages = [y for (x,y) in sorted(zip(X, Ages))]

        ax = plt.plot(Ages, MTL, "ro")
        plt.xlabel("AFTA")
        plt.ylabel("Mean Track Length")
        return ax

class SopaleModel(ThermoMechanicalModel):
    def __init__(self, path=None, name=None, sealevel=None):
        super(ThermoMechanicalModel, self).__init__()
        self.path = os.getcwd()
        if path:
            self.path = path
        self.model_name = os.path.split(self.path)[-1]
        if name:
            self.model_name = name
        self.HDF5_filename = self.path+"/HDF/HDF.h5"
        try:
            self.HDF5 = h5py.File(self.HDF5_filename, "r")
        except:
            pass
        self.model_height = self.GetHeightOfModel()
        self.model_width = self.GetWidthOfModel()
        self.steps = self._list_available_steps()
        self.steps = list(range(min(self.steps),max(self.steps)+50, 50))
        self.steps.append(1)
        self.steps = sorted(self.steps)
        #self.times = [self.get_time(step) for step in self.steps]
        #self.dts = [self.get_dt(step) for step in self.steps]
        self.nmat_used = self.GetNumberOfMaterialsUsed()
        self.cmap = ritskemat

        if not sealevel:
            try:
                self.sealevel=self.GetSeaLevel()
            except:
                self.sealevel = 0.
        else:
            self.sealevel = sealevel

        self.eulerian_dims = self.GetEulerianDims()
        self.lagrangian_dims = self.GetLagrangianDims()
        if ritskemat.N < self.nmat_used:
            print("""The default colormap is not appropriate for your
                   model, use the setColormap to replace the default""")

        return

class material:
#    _registry = []
#
#    MaterialNumber = 0
#    NumberOfmaterial = 0

    # Define Material itself
    name = None
    index = 0
    density = None
    # Brittle behavior
    C = None
    phi = None

    # Viscous behavior
    f = None    # Weakening factor
    A = None    # Initial Constant
    n = None    # Power Law exponent
    Ea = None   # Activation Energy
    Va = None   # Activation Volume

    # Thermal properties
    alpha = None  # Volume coefficient of thermal expansion
    HeatProd = 0.
    BasalHeatFlux = 0.
    Conductivity = 0.
    Top = None
    Bottom = None

#    def __init__(self):
#        self._registry.append(self)
#        self.__class__.NumberOfmaterial += 1
#        self.NumberOfmaterial = self.__class__.NumberOfmaterial
#
#    def SetPositionInPile(self, top, bottom):
#        self.Top = top
#        self.Bottom = bottom
#        for mat in material._registry:
#            if( top > mat.Bottom and bottom <= mat.Top):
#                mat.Bottom = top
#            elif (bottom < mat.Top and top >= mat.Bottom):
#                mat.Top = bottom
#
#        for mat in material._registry:
#            print(mat.name, mat.Top, mat.Bottom)

def write_SOPALE1_i(header=None, models=[30], modelkey="B",
                 amodel=30, benchmark=6, wres=50, rres=0, align_time=0,
                 nsave1=[100000], nsave2=[100000], output_path=None,
                 time_nstep=4000, dt=1.57, outunt=6):
    if header is None:
        header = """This is a shortened version of Sopale input"""
    if output_path is None:
        output_path = "./Sopale/r30/g"

    # Count the number of line in the header paragraph
    nlines = header.count("\n") + 1

    with open("SOPALE1_i", "w") as f:
        # Write Header
        f.write("%i\n" % nlines)
        f.write(header)
        f.write("\n")
        # Write nmodels
        f.write("%i\n" % len(models))
        for i in range(len(models)):
            f.write("%i " % models[i])
            f.write("\n")
        # Write modelkey
        f.write(modelkey)
        f.write("\n")
        # Write amodel and Benchmark
        f.write("%i %i\n" % (amodel, benchmark))
        # Write restart options
        f.write("%i %i %i\n" % (wres, rres, align_time))
        # Write nsave1
        f.write("%i\n" % len(nsave1))
        for i in range(len(nsave1)):
            f.write("%i " % nsave1[i])
        f.write("\n")
        # Write nsave2
        f.write("%i\n" % len(nsave2))
        for i in range(len(nsave2)):
            f.write("%i " % nsave2[i])
        f.write("\n")
        # Write output path
        f.write(output_path+"\n")
        # Write time_nstep, dt, outunt
        f.write("%i %f5.2 %i" % (time_nstep, dt, outunt))

def Calculate_AFTA_all(step):
    current = os.getcwd()
    Models = [model for model in os.listdir(current) if os.path.isdir(model)]
    for model in Models:
        try:
            os.chdir(model)
            SModel = SopaleModel()
            SModel.Calculate_AFTA(step)
            SModel = None
            os.chdir(current)
        except:
            pass

def PlotYieldEnvellope(model, output=True):

    def plot_area(Stress, Depths, Top, Bottom, color="yellow"):
        S = [S for i, S in enumerate(Stress) if Top <= Depths[i] <= Bottom]
        D = [-1*D for D in Depths if Top <= D <= Bottom]
        ax.fill_betweenx(D, 0, S,facecolor=color)

    fig = plt.figure(figsize=(6,12))
    ax = fig.add_subplot(111)
    #ax2 = ax.twiny()

    stress, Z, T, Tb = YieldEnveloppe(model, phi=1)

    ax.plot(stress, -Z, linestyle="-", linewidth=0.5, color="k")

    plot_area(stress, Z, 0., 25., uc)
    plot_area(stress, Z, 25., 35., lc)
    plot_area(stress, Z, 35., 125., uml)
    plot_area(stress, Z, 125., 600., slm)

    stress, Z, T, P = YieldEnveloppe(model, phi=2)

    ax.fill_betweenx(-Z, 0, stress
            ,edgecolor='k',facecolor="gray",
            linestyle="-",linewidth=0.5, alpha=0.5, hatch="\\")

    ax.axhline(-1*model.moho, linewidth=2.0, color="red")

    ax.set_ylim(-100,0)
    ax.set_yticks([0,-35,-125])
    ax.set_xticks([0,600])
    ax.tick_params(axis='x', labelsize=18)
    ax.tick_params(axis='y', labelsize=18)

    #ax2.plot(T-273.5,-Z,'r-',linewidth=1)
    #ax2.plot(Tb-273.5,-Z,'g-',linewidth=1)
    #ax2.spines['top'].set_color('red')
    #ax2.tick_params(axis='x', colors='red', size=18)
    #ax2.set_xlabel('temperature ($^{\circ}C$)',size=18)
    #ax2.set_xticks([0,200,550,1330])

    ax.set_xlabel('stress (MPa)',size=18)
    ax.set_ylabel('depth (km)',size=18)
    ax.set_xlim(0,600)

    if output:
        plt.savefig("output.pdf")

    return ax

def YieldEnveloppe(model, strainRate=2.64e-16, phi=1):

    materials, Z = CreateCol(model.material)

    # Get dz
    dz = np.abs(np.diff(Z))
    dz = np.hstack([0, dz])

    # Get Density
    rho = np.array([float(mat.density) for mat in materials])

    # Pressure
    Pres = rho * constant.g * dz
    P = np.cumsum(Pres)

    # Get Temperature
    Tb, _ = get_geotherm(materials, Z)
    Temperature = get_simple_geotherm(Z) + 273.15
    #Temperature = Tb

    # Get cohesion
    Cohesion = np.array([mat.C for mat in materials])

    # Get Phi
    PHI = np.array([mat.phi[phi-1] for mat in materials])

    # Calculate Friction
    Plastic = DruckerPragerYieldCompression2(Cohesion, P, PHI)

    # Calculate viscous
    Ductile = EffectiveViscosityMat(materials, strainRate, P, Temperature)

    #Enveloppe = np.minimum(Plastic, Ductile)*strainRate*2*1e-6
    Enveloppe = np.minimum(Plastic, Ductile)
    return Enveloppe*1e-6, Z*1e-3, Temperature, Tb

def CreateCol(A):
    """ Create a pile of elements
    """

    bottom = [mat.Bottom for mat in A]
    zi = set(bottom)
    zi = list(zi)

    depths = np.hstack([np.linspace(mat.Top, mat.Bottom, 1000, endpoint=False) for mat in A])
    C = np.hstack([np.repeat(mat, 1000) for mat in A])

    depths = np.hstack([depths, max(zi)])
    C = np.hstack([C, C[-1]])

    return C, depths

def CreateCol2(A):
    """ Create a pile of elements
    """
    controlpoints = [0,25000,35000,125000,600000]
    nbelements = [50,50,100,100]
    depths = np.hstack([np.linspace(controlpoints[i], controlpoints[i+1],
                       nbelements[i], endpoint=False)
        for i in range(0, len(A)-1)])
    depths = np.hstack(
        [depths, np.linspace(controlpoints[-2], controlpoints[-1], nbelements[-1])])
    C = np.hstack([np.repeat(A[i], nbelements[i]) for i in range(0, len(A))])
    return C, depths

def get_simple_geotherm(depth):
    T = [0, 550, 1330, 1330]
    D = [0, 35000, 125000, 600000]
    return np.interp(depth, D, T)

def get_simple_continental_geotherm(depth):
    T = [0, 550, 1330, 1330]
    D = [0, 35000, 125000, 600000]
    return np.interp(depth, D, T)

def get_geotherm(materials, depths, Tsurf=273.15):
    A = [mat.HeatProd for mat in materials]
    k = [mat.Conductivity for mat in materials]
    Q = [mat.BasalHeatFlux for mat in materials]

    zi = set([mat.Bottom for mat in materials])
    zi = list(zi)
    zi.sort()

    T = []
    layer = 0
    Ttop = Tsurf
    for i, elem in enumerate(materials):

        z = depths[i]
        zb = elem.Bottom
        zt = elem.Top

        if z >= zi[layer]:
            Ttop = T[-1]
            layer += 1

        temp = (-A[i]/(2*k[i]) * (z-zt)**2) + (Q[i]/k[i] + (A[i]/k[i])*(zb-zt))*(z-zt) + Ttop

        #if layer == len(zi) and temp < 1350.:
        #    temp = (-A[i]/(2*k[i]) * z**2) + (Q[i]/15.0 + A[i]/k[i] * zb)*z + Ttop

        T.append(temp)

    return np.array(T), depths

def Compute_plastic_enveloppe(C, P, PHI, strain_rate):
    """
    Drucker - Prager Yield Criterion in Compression

    Parameters:
    ----------

    P : real
        Pressure
    C : real
        Friction Coefficient
    """
    tau_yield =  C * np.cos(PHI) + P * np.sin(PHI)
    return 0.5*tau_yield / strain_rate

def DruckerPragerYieldCompression(C, P, PHI):
    """
    Drucker - Prager Yield Criterion in Compression

    Parameters:
    ----------

    P : real
        Pressure
    C : real
        Friction Coefficient
    """

    PHI = np.radians(PHI)
    A = 2*np.tan(PHI)*P + 2*C
    B = np.sqrt(1+np.tan(PHI)**2) - np.tan(PHI)
    return A / B

def DruckerPragerYieldCompression2(C, P, PHI):
    """
    Drucker - Prager Yield Criterion in Compression

    Parameters:
    ----------

    P : real
        Pressure
    C : real
        Friction Coefficient
    """

    A = 2.0 * ( C * np.cos(np.radians(PHI)) + P * np.sin(np.radians(PHI)))
    B = (1 + np.cos(2*(45.0+PHI/2.0)*np.pi/180.)) * np.sin(np.radians(PHI)) + np.sin(2*(45.0+PHI/2.0)*np.pi/180.0)
    return A / B

def DruckerPragerYieldTension(C, P, PHI):
    """
    Drucker - Prager Yield Criterion

    Parameters:
    ----------

    P : real
        Pressure
    C : real
        Friction Coefficient
    """

    A = -1*2*np.tan(PHI)*P + 2*C
    B = np.sqrt(1+np.tan(PHI)**2) + np.tan(PHI)
    return A / B

def EffectiveViscosityMat(materials, strainRate, P, T):

    I = strainRate
    f = np.array([mat.f for mat in materials])
    A = np.array([mat.A for mat in materials])
    n = np.array([mat.n for mat in materials])
    Ea = np.array([mat.Ea for mat in materials])
    Va = np.array([mat.Va for mat in materials])

    return f*(I/A)**(1.0/n)*np.exp((Ea + P*Va)/(constant.R * T * n))

def fillMaterial(matnumber, elements, Z, stress, color="orange"):
    matnumber -= 1
    top = int(np.sum(elements[: matnumber]))
    bottom = int(np.sum(elements[: matnumber+1])-1)
    x = np.hstack([0.0, stress[top: bottom+1], 0.0])
    y = np.hstack([Z[top], Z[top: bottom+1], Z[bottom]])
    plt.fill(x, -y, color=color)
