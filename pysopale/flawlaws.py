#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#    
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

class flawlaw(object):
    A = None
    n = None
    Ea = None
    Va = None
    name = None

WetQz = flawlaw()
WetQz.name = "Wet Quartz"
WetQz.A = 8.574e-28
WetQz.n = 4.0
WetQz.Ea = 223e3
WetQz.Va = 0.

WetOl = flawlaw()
WetOl.name = "Wet Olivine"
WetOl.A = 1.7578e-14
WetOl.n = 3.0
WetOl.Ea = 430e3
WetOl.Va = 15e-6


