from pysopale import material
from pysopale.flawlaws import WetQz, WetOl


UpperCrust                    = material()
UpperCrust.name               = "Upper Crust"
UpperCrust.density            = 2750
UpperCrust.C                  = 20e6
UpperCrust.phi                = (15,2)
UpperCrust.f                  = 1
UpperCrust.A                  = WetQz.A
UpperCrust.n                  = WetQz.n
UpperCrust.Ea                 = WetQz.Ea
UpperCrust.Va                 = WetQz.Va
UpperCrust.HeatProd           = 0.9e-6  # Watt per m3 
UpperCrust.BasalHeatFlux      = 19.5e-3 # Watt
UpperCrust.Top                = 0.e3 
UpperCrust.Bottom             = 25.e3
UpperCrust.Conductivity       = 2.245 # Watt per m per Kelvin

LowerCrust                    = material()
LowerCrust.name               = "Lower Crust"
LowerCrust.density            = 2900
LowerCrust.C                  = 20e6
LowerCrust.phi                = (15,2)
LowerCrust.f                  = 1
LowerCrust.A                  = WetQz.A
LowerCrust.n                  = WetQz.n
LowerCrust.Ea                 = WetQz.Ea
LowerCrust.Va                 = WetQz.Va
LowerCrust.BasalHeatFlux      = 19.5e-3
LowerCrust.Top                = 25.e3
LowerCrust.Bottom             = 35.e3
LowerCrust.Conductivity       = 2.245

MantleLithosphere             = material()
MantleLithosphere.name        = "Mantle Lithosphere"
MantleLithosphere.density     = 3300
MantleLithosphere.C           = 20e6
MantleLithosphere.phi         = (15, 2)
MantleLithosphere.f           = 5.0
MantleLithosphere.A           = WetOl.A
MantleLithosphere.n           = WetOl.n
MantleLithosphere.Ea          = WetOl.Ea
MantleLithosphere.Va          = WetOl.Va
MantleLithosphere.BasalHeatFlux = 19.5e-3
MantleLithosphere.Top         = 35.e3
MantleLithosphere.Bottom      = 125.e3
MantleLithosphere.Conductivity = 2.245

Mantle                        = material()
Mantle.name                   = "Mantle"
Mantle.density                = 3300
Mantle.C                      = 20e6
Mantle.phi                    = (15,2)
Mantle.f                      = 1.0
Mantle.A                      = WetOl.A
Mantle.n                      = WetOl.n
Mantle.Ea                     = WetOl.Ea
Mantle.Va                     = WetOl.Va
Mantle.BasalHeatFlux          = 19.5e-3
Mantle.Top                    = 125.e3
Mantle.Bottom                 = 600.e3
Mantle.Conductivity           = 48.75

moho = 35.0
material=[UpperCrust, LowerCrust, MantleLithosphere, Mantle]
