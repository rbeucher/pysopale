#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#    
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


from pysopale import material
from pysopale.flawlaws import WetQz, WetOl


UpperCrust                    = material()
UpperCrust.name               = "Upper Crust"
UpperCrust.index              = 1
UpperCrust.density            = 2750
UpperCrust.C                  = 20e6
UpperCrust.phi                = (15,2)
UpperCrust.f                  = 30.0
UpperCrust.A                  = WetQz.A
UpperCrust.n                  = WetQz.n
UpperCrust.Ea                 = WetQz.Ea
UpperCrust.Va                 = WetQz.Va
UpperCrust.HeatProd           = 0.9e-6  # Watt per m3 
UpperCrust.BasalHeatFlux      = 19.5e-3 # Watt
UpperCrust.Top                = 0.e3 
UpperCrust.Bottom             = 25.e3
UpperCrust.Conductivity       = 2.245 # Watt per m per Kelvin


LowerCrust                    = material()
LowerCrust.name               = "Lower Crust"
LowerCrust.index              = 2
LowerCrust.density            = 2900
LowerCrust.C                  = 20e6
LowerCrust.phi                = (15,2)
LowerCrust.f                  = 30.0
LowerCrust.A                  = WetQz.A
LowerCrust.n                  = WetQz.n
LowerCrust.Ea                 = WetQz.Ea
LowerCrust.Va                 = WetQz.Va
LowerCrust.HeatProd           = 0.9e-6  # Watt per m3 
LowerCrust.BasalHeatFlux      = 19.5e-3
LowerCrust.Top                = 25.e3
LowerCrust.Bottom             = 35.e3
LowerCrust.Conductivity       = 2.245

MantleLithosphere             = material()
MantleLithosphere.name        = "Mantle Lithosphere"
MantleLithosphere.index       = 3
MantleLithosphere.density     = 3300
MantleLithosphere.C           = 20e6
MantleLithosphere.phi         = (15, 2)
MantleLithosphere.f           = 5.0
MantleLithosphere.A           = WetOl.A
MantleLithosphere.n           = WetOl.n
MantleLithosphere.Ea          = WetOl.Ea
MantleLithosphere.Va          = WetOl.Va
MantleLithosphere.HeatProd   = 0.0  # Watt per m3 
MantleLithosphere.BasalHeatFlux = 19.5e-3
MantleLithosphere.Top         = 35.e3
MantleLithosphere.Bottom      = 125.e3
MantleLithosphere.Conductivity = 2.245

Mantle                        = material()
Mantle.name                   = "Mantle"
Mantle.index                  = 4
Mantle.density                = 3300
Mantle.C                      = 20e6
Mantle.phi                    = (15,2)
Mantle.f                      = 1.0
Mantle.A                      = WetOl.A
Mantle.n                      = WetOl.n
Mantle.Ea                     = WetOl.Ea
Mantle.Va                     = WetOl.Va
Mantle.HeatProd               = 0.0  # Watt per m3 
Mantle.BasalHeatFlux          = 19.5e-3
Mantle.Top                    = 125.e3
Mantle.Bottom                 = 600.e3
Mantle.Conductivity           = 48.75


moho = 35.0

material=[UpperCrust, LowerCrust, MantleLithosphere, Mantle]
