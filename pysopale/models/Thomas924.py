#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#    
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

from pysopale import material
from pysopale.flawlaws import WetQz, WetOl, DryDia
# Type I Weak Crust and Lower Crust


UpperCrust                    = material()
UpperCrust.name               = "Upper Crust"
UpperCrust.density            = 2750
UpperCrust.C                  = 20e6
UpperCrust.phi                = (15,2)
UpperCrust.f                  = 0.02
UpperCrust.A                  = WetQz.A
UpperCrust.n                  = WetQz.n
UpperCrust.Ea                 = WetQz.Ea
UpperCrust.Va                 = WetQz.Va

LowerCrust                    = material()
LowerCrust.name               = "Lower Crust"
LowerCrust.density            = 2900
LowerCrust.C                  = 20e6
LowerCrust.phi                = (15,2)
LowerCrust.f                  = 0.1
LowerCrust.A                  = DryDia.A
LowerCrust.n                  = DryDia.n
LowerCrust.Ea                 = DryDia.Ea
LowerCrust.Va                 = DryDia.Va

MantleLithosphere             = material()
MantleLithosphere.name        = "Mantle Lithosphere"
MantleLithosphere.density     = 3300
MantleLithosphere.C           = 20e6
MantleLithosphere.phi         = (15, 2)
MantleLithosphere.f           = 5.0
MantleLithosphere.A           = WetOl.A
MantleLithosphere.n           = WetOl.n
MantleLithosphere.Ea          = WetOl.Ea
MantleLithosphere.Va          = WetOl.Va

Mantle                        = material()
Mantle.name                   = "Mantle"
Mantle.density                = 3300
Mantle.C                      = 20e6
Mantle.phi                    = (15,2)
Mantle.f                      = 1.0
Mantle.A                      = WetOl.A
Mantle.n                      = WetOl.n
Mantle.Ea                     = WetOl.Ea
Mantle.Va                     = WetOl.Va

material=[UpperCrust, LowerCrust, MantleLithosphere, Mantle]
