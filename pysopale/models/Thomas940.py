from pysopale import material
from pysopale.flawlaws import WetQz, WetOl


UpperCrust                    = material()
UpperCrust.name               = "Upper Crust"
UpperCrust.density            = 2750
UpperCrust.C                  = 20e6
UpperCrust.phi                = (15,2)
UpperCrust.f                  = 20
UpperCrust.A                  = WetQz.A
UpperCrust.n                  = WetQz.n
UpperCrust.Ea                 = WetQz.Ea
UpperCrust.Va                 = WetQz.Va

LowerCrust                    = material()
LowerCrust.name               = "Lower Crust"
LowerCrust.density            = 2900
LowerCrust.C                  = 20e6
LowerCrust.phi                = (15,2)
LowerCrust.f                  = 20
LowerCrust.A                  = WetQz.A
LowerCrust.n                  = WetQz.n
LowerCrust.Ea                 = WetQz.Ea
LowerCrust.Va                 = WetQz.Va

MantleLithosphere             = material()
MantleLithosphere.name        = "Mantle Lithosphere"
MantleLithosphere.density     = 3300
MantleLithosphere.C           = 20e6
MantleLithosphere.phi         = (15, 2)
MantleLithosphere.f           = 5.0
MantleLithosphere.A           = WetOl.A
MantleLithosphere.n           = WetOl.n
MantleLithosphere.Ea          = WetOl.Ea
MantleLithosphere.Va          = WetOl.Va

Mantle                        = material()
Mantle.name                   = "Mantle"
Mantle.density                = 3300
Mantle.C                      = 20e6
Mantle.phi                    = (15,2)
Mantle.f                      = 1.0
Mantle.A                      = WetOl.A
Mantle.n                      = WetOl.n
Mantle.Ea                     = WetOl.Ea
Mantle.Va                     = WetOl.Va

material=[UpperCrust, LowerCrust, MantleLithosphere, Mantle]
