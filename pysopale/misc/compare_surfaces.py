import pysopale as sop
import pyfastscape as fas
import pylab as plt
from matplotlib import animation
import glob


def compare_surfaces(step, path1=".", path2="."):
    """ Plot Sopale freesurface at step and compare it
        to Mean, Max and Min profiles out of FastScape.

    Parameters:
    -----------
    step -- timestep number id
    path1 -- Sopale path
    path2 -- FastScape path
    """
    plt.gca()
    plt.cla()
    line1, = sop.plot_freesurface_elevation(step)
    line2, = fas.PlotMeanElevProfile(step, 
                                     filename=(path1 + "/" + 
                                               path2 + "/RUN00/RUN00.h5"))
    line3, = fas.PlotMaxElevProfile(step, 
                                    filename=(path1 + "/" + 
                                              path2 + "/RUN00/RUN00.h5"))
    line4, = fas.PlotMinElevProfile(step, 
                                    filename=(path1 + "/" + 
                                              path2 + "/RUN00/RUN00.h5"))
    line1.set_color("b")
    line1.set_label("Sopale Freesurface")
    line2.set_color("r")
    line2.set_label("FastScape Mean")
    line3.set_color("g")
    line3.set_label("FastScape Max")
    line3.set_color("g")
    line4.set_label("FastScape Min")
    X = line1.get_xdata()
    ax = plt.gca()
    ax.set_title("Comparison at Step: %s" % step)
    plt.xlim(min(X), max(X))
    plt.ylim(-2.0, 2.0)
    plt.legend()
    return


def animate_compare_surfaces():
    """ Animate the compare_surfaces function at all timesteps."""
    fig = plt.figure()
    path = "."
    steps = glob.glob(path+"/matlab/loop*")
    steps = [int(step.split(".")[-1]) for step in steps]
    steps.sort()
    ani = animation.FuncAnimation(fig, compare_surfaces, steps[1:],
                                  init_func=compare_surfaces(steps[1]),
                                  blit=False)
    
    # Set up formatting for the movie files
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=5, bitrate=1800)
    ani.save("surfaces.mp4", writer=writer, dpi=600)
    plt.close()

if __name__ == "__main__":
    animate_compare_surfaces()
