__author__ = "Romain Beucher"



#class SopaleModel:
#     """Sopale Model Class"""
#    self.wres = 50
#    self.time_nstep = 1000
#    self.dt =  1.5770e11 
#    self.outunt = 60
#    
#    self.bigu = 1e7
#    self.bigt = 1e7
#
#    self.length = 1.2e6
#    self.height = 0.6e6
#    self.nx1 = 601
#    self.ny1 = 201
#    self.betaxg = 
#    self.betayg
#    self.length2
#    self.height2
#    self.nx2
#    self.ny2
#
#    self.npert
#
#    self.nfpert
#    self.iformul = 2
#    self.hists
#    self.ang_gravity
#    self.gravityz
#    self.dplimit
#    self.visminmax
#    self.psmooth
#    self.p_bdry_layer
#    self.splasmin
#    self.vismin_bdry_layer
#    self.edotmax
#    
#    self.nmatsets
#    
#    self.mixhyp
#    self.refsrate
#    self.redphi
#    self.nredphi
#    self.plas_fluid_method
#    
#    self.ibinread
#    
#    self.type_bc
#    self.vxleft_up
#    self.vxleft_down
#    self.yleft1
#    self.yleft2
#    self.rollleft
#    self.vxright_up
#    self.vxright_down
#    self.yright1
#    self.yright2
#    self.rollright
#    self.vytop0
#    self.vxbottom0
#    self.reference_pressure_column
#    self.reference_material
#    self.ksplitx1
#    self.tsmooth
#    self.ismooth
#    self.vscale
#    self.verror
#    self.verror1
#    self.miterv
#    self.irescale
#    self.kinitvis
#    self.force_iter1
#    self.force_iter
#    self.force_continue
#    self.nycyc2
#    self.color2op
#    self.lagtest
#    self.eulcolourrule
#    self.eul_density_rule
#    
#    self.ucompute
#    self.tcompute
#    self.ale_compute
#    self.dtcompute
#    self.tbottom
#    self.ttop
#    self.e_temperature
#    self.cfl
#    
#    self.lambda_blend1
#    self.lambda_blend2
#    
#    
#    self.steady
#    self.analy
#    self.esteady
#    
#    self.maxter
#    self.dtempm
#    self.ther_advection
#    self.th_gamma_time
#    self.th_cn
#    self.tempsmooth
#    self.type_bct
#    
#    self.th_dt
#    self.vzgiven
#    
#    self.dmtm
#    self.type_bct0
#    self.bct30
#    self.ntperts
#    self.tpert

#     def read_existing_input(self):

#class ThermalMat():
#    """Thermal material class"""
#    self.density =
#    self.bulk_viscosity =
#    self.friction_angle =
#    self.phi1 = self.friction_angle
#    self.friction_angle_sw = 
#    self.phi2 = self.friction_angle_sw
#    self.strain_weakening_range =
#    self.cohesion = 
#    self.cohesion_sw =






def set_mechanical_boundary_condition_left(filename, vxleft_up, vxleft_down,
                                           yleft1, yleft2, rollleft = 0):
    ReplaceElementInputFile(filename, 87, 0, vxleft_up) 
    ReplaceElementInputFile(filename, 87, 1, vxleft_down) 
    ReplaceElementInputFile(filename, 87, 2, yleft1) 
    ReplaceElementInputFile(filename, 87, 3, yleft2) 
    ReplaceElementInputFile(filename, 87, 4, rollleft) 

def set_mechanical_boundary_condition_right(filename, vxright_up, vxright_down,
                                           yright1, yright2, rollright = 0):
    ReplaceElementInputFile(filename, 87, 5, vxright_up) 
    ReplaceElementInputFile(filename, 87, 6, vxright_down) 
    ReplaceElementInputFile(filename, 87, 7, yright1) 
    ReplaceElementInputFile(filename, 87, 8, yright2) 
    ReplaceElementInputFile(filename, 87, 9, rollright) 

def ReplaceElementInputFile(filename, lineno, pos, value):
    f = open(filename, "r")
    lines = f.readlines()
    f.close()
    line = lines[lineno].split()
    line[pos] = str(value)
    line.append("\n")
    line = " ".join(line)
    lines[lineno] = line
    f = open(filename, "w")
    f.writelines(lines)

def GetElementFromInputFile(lineno, position, filename="SOPALE1_i"):
    import linecache
    A = linecache.getline(filename, lineno).split()[position-1]
    A = A.replace("d", "E")
    A = A.replace("D", "E")
    return float(A)


def PrintModel(tablefmt="plain"):
    table = [
            ["Crust thickness (km)", 2],
            ["Lithosphere thickness (km)", 2],
            ["Model width (km)", 2],
            ["Model thickness (km)", 2],
            ["Rift Velocity, full rate (cm yr-1)", 2],
            ["Crust density (kg.m3)", 2],
            ["Mantle lithosphere density", 2],
            ["Asthenosphere density", 2],
            ["Internal angle of friction", 2]
            ]
    print(tabulate(table, tablefmt=tablefmt))
