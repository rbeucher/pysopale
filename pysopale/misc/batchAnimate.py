import itertools
import os
from multiprocessing import Pool, freeze_support
from pysopale import animate_eulerian_material_colors

def func_star(a):
    wd = os.getcwd()
    os.chdir(a[output].split(".")[0])
    print("Doing Model %s" % a["title"])
    animate_eulerian_material_colors(**a)
    os.chdir(wd)
    return 1

def main():
    pool = Pool()
    models = [elem for elem in os.listdir(".") if os.path.isdir(elem)]
    inputs = [{"output":model+".mp4"} for model in models]
    pool.map(func_star, inputs)

if __name__=="__main__":
    main()
