import pylab as plt
import h5py
from matplotlib import animation


#def view(step, filename="./matlab/HDF.h5"):
#    print("Step"+str(step))
#    plt.clf()
#    A = h5py.File(filename)
#    x1 = A["Step"+str(step).zfill(5)]["x1"][()] * 1e-3
#    y1 = A["Step"+str(step).zfill(5)]["y1"][()] * 1e-3
#    vy1 = A["Step"+str(step).zfill(5)]["vy1"][()]
#    temp1 = A["Step"+str(step).zfill(5)]["Temp1"][()]
#    vx1 = A["Step"+str(step).zfill(5)]["vx1"][()]
#    vx1 *= 1e3 * 365 * 24 * 3600
#    vy1 *= 1e3 * 365 * 24 * 3600
#    plt.subplot(311)
#    plt.pcolormesh(x1, y1, vy1, rasterized = True, vmin=-10.0, vmax=10.0)
#    plt.colorbar()
#    plt.subplot(312)
#    plt.pcolormesh(x1, y1, vx1, rasterized = True, vmin=-10.0, vmax=10.0)
#    plt.colorbar()
#    plt.subplot(313)
#    plt.pcolormesh(x1, y1, temp1, rasterized=True, vmin=0, vmax=1900)
#    plt.colorbar()
#    plt.suptitle("Step"+str(step))

def view(step, filename="./matlab/HDF.h5"):
    print("Step"+str(step))
    plt.clf()
    A = h5py.File(filename)
    x1 = A["Step"+str(step).zfill(5)]["x1"][()] * 1e-3
    y1 = A["Step"+str(step).zfill(5)]["y1"][()] * 1e-3
    vy1 = A["Step"+str(step).zfill(5)]["vy1"][()]
    vx1 = A["Step"+str(step).zfill(5)]["vx1"][()]
    vx1 *= 1e3 * 365 * 24 * 3600
    vy1 *= 1e3 * 365 * 24 * 3600
    plt.plot(vx1[0,:], y1[0,:]) 
    plt.plot(vx1[-1,:], y1[-1,:]) 
    plt.xlim(-10,10)
    plt.suptitle("Step"+str(step))

#def view(step, filename="./matlab/HDF.h5"):
#    print("Step"+str(step))
#    plt.clf()
#    A = h5py.File(filename)
#    x1 = A["Step"+str(step).zfill(5)]["x1"][()] * 1e-3
#    y1 = A["Step"+str(step).zfill(5)]["y1"][()] * 1e-3
#    plt.plot(x1[:,0], y1[:,0]) 
#    plt.ylim(597,603)
#    plt.suptitle("Step"+str(step))


def animate(filename="./matlab/HDF.h5",
            output="debug.mp4", dpi=600, fps=5, bitrate=320):
    fig = plt.figure()
    f = view
    A = h5py.File(filename, "r")
    steps = list(A.items())
    steps = sorted([int(name[4:]) for name, object in steps[:-2]])
    
    f(steps[1], filename)
    fargs = [filename]
    ani = animation.FuncAnimation(fig, f, steps[2:], fargs=fargs, blit=False)

    # Set up formatting for the movie files
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=fps, bitrate=bitrate)
    ani.save(output, writer=writer, dpi=dpi)
    plt.close()

def plotAll(filename="./matlab/HDF.h5"):
    A = h5py.File(filename, "r")
    steps = list(A.items())
    steps = sorted([int(name[4:]) for name, object in steps[:-2]])
    for step in steps[1:]:
        print("Doing Step"+str(step))
        x1 = A["Step"+str(step).zfill(5)]["x1"][()] * 1e-3
        y1 = A["Step"+str(step).zfill(5)]["y1"][()] * 1e-3
        vy1 = A["Step"+str(step).zfill(5)]["vy1"][()]
        temp1 = A["Step"+str(step).zfill(5)]["Temp1"][()]
        vx1 = A["Step"+str(step).zfill(5)]["vx1"][()]
        vx1 *= 1e3 * 365 * 24 * 3600
        vy1 *= 1e3 * 365 * 24 * 3600
        plt.subplot(311)
        plt.pcolormesh(x1, y1, vy1, rasterized = True, vmin=-2.0, vmax=2.0)
        plt.colorbar()
        plt.subplot(312)
        plt.pcolormesh(x1, y1, vx1, rasterized = True, vmin=-2.0, vmax=2.0)
        plt.colorbar()
        plt.subplot(313)
        plt.pcolormesh(x1, y1, temp1, rasterized=True, vmin=0, vmax=1600)
        plt.colorbar()
        plt.suptitle("Step"+str(step))
        plt.savefig("Step"+str(step).zfill(5)+".pdf")
        plt.close()
    
    A.close()
