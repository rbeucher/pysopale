#!/bin/bash/python

# Compare StrainRate output between 2 differents models
# I wrote that script to look at mesh dependency on strain Rates
# as Ritske suggested that using a different number of elements between the
# crust and the lithosphere may affect the calculation of the strain rate.

import pysopale as sop
import pylab as plt
import numpy as np
from math import *

def PlotCompareStrainRate(step, model1path, model2path, model1title=None, model2title=None, xlim=None, ylim=None):
    fig = plt.figure()
    ax = fig.add_subplot(211)
    sop.PlotStrainRate(step, model1path)
    plt.title(model1title)
    plt.ylabel("Depth (km)")
    plt.xlabel("Distance along model (km)")
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.colorbar(label="Strain Rate (1/s)")
    ax = fig.add_subplot(212)
    sop.PlotStrainRate(step, model2path)
    plt.title(model2title)
    plt.ylabel("Depth (km)")
    plt.xlabel("Distance along model (km)")
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.colorbar(label="Strain Rate (1/s)")
    #plt.show()
    plt.tight_layout()
    plt.savefig("Step"+str(step)+".pdf")

steps = np.arange(100,1500,100)

for step in steps:
    PlotCompareStrainRate(step, "./test3layersNoErosion", "./testUNiform2LayersNoErosion", model1title="Step ("+str(step)+") 3Layers (75/75/50)", model2title="Step ("+str(step)+") 2 Layers (150/50)", xlim=(450,750), ylim=(-125, 10))
