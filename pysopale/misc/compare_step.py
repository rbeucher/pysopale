import pylab as plt
import pyfastscape as fas
from pyfastscape.shaded_relief import get_shaded_colorbar
import pysopale as sop
from pysopale import ritskemat2, ritskemat, ritskemat3
from matplotlib.colorbar import ColorbarBase
from matplotlib.colors import LogNorm, Normalize
import numpy as np

def compare_steps(step1, step2, step3, Model, output="CompareSteps.pdf",
    shoreline=False, title=None, azimuth=130, angle_altitude=45,
    sealevel=False, time=False, sopaleField="Material", erosionSedim=True,
    velocities=True, temperature=[550], vmin=-10., vmax=10.):
    """ Compare 4 steps """

    FModel = fas.FastScapeModel(Model+"/RUN00/RUN00.h5")
    SModel = sop.SopaleModel(Model)

    if time:
        step1 = SModel.get_step_from_time(step1)
        step2 = SModel.get_step_from_time(step2)
        step3 = SModel.get_step_from_time(step3)

    print(step1, step2, step3)
    xlim=(400,800)
    ylim=(70,130)

    plt.clf()
    fig, axes = plt.subplots(nrows=6)

    def plot_fastscape(axis, step):
        plt.sca(axis)
        if erosionSedim:
            ax, cax, im = FModel.PlotShadedReliefStepErosionSedimentation(step, xlim=xlim, ylim=ylim,
                            colorbar=False, shoreline=shoreline, azimuth=azimuth,
                            angle_altitude=angle_altitude, vmin=vmin, vmax=vmax)
        else:
            ax, cax, im = FModel.PlotShadedReliefStep(step, xlim=xlim, ylim=ylim,
                             colorbar=False, shoreline=shoreline, azimuth=azimuth,
                             angle_altitude=angle_altitude)
        ax.set_xlabel("")
        ax.set_title("")
        ax.xaxis.set_ticklabels([])
        ax.set_yticks([ylim[0],ylim[1]])
        ax.set_yticklabels([str(ylim[0])+" km", str(ylim[1])+" km"])
        ax.set_ylabel("")
        time = FModel.GetTime(step)
        ax.text(400,ylim[1]-15,
               "{0:5.1f} Myr".format(time),
                size=18, ha="left")
        return ax, cax, im

    def plot_sopale_mat(axis, step):
        plt.sca(axis)
        ax, __ = SModel.plot_eulerian_material_colors(step, xlim=xlim,
            colorbar=False, ctemp=[550], velocities=False, sealevel=sealevel)

        collection = SModel.get_stretching_lines(step, linewidth=.5, cmap="jet")
        ax.add_collection(collection)
        ax.set_xlabel("")
        ax.set_title("")
        ax.xaxis.set_ticklabels([])
        ax.set_yticks([0,-35])
        ax.set_yticklabels(["0 km", "35 km"])
        ax.set_ylabel("")
        ax.text(425,-33,
               "{0:d}$^\circ$C".format(550),
                size=10, ha="center", color="red")
        return ax

    def plot_sopale_strainRate(axis, step):
        plt.sca(axis)
        ax, __ = SModel.plot_eulerian_strain_rate_field(step, xlim=xlim,
            colorbar=False)

        ax.set_xlabel("")
        ax.set_title("")
        ax.xaxis.set_ticklabels([])
        ax.set_yticks([0,-35])
        ax.set_yticklabels(["0 km", "35 km"])
        ax.set_ylabel("")
        return ax
    
    def plot_sopale_stress(axis, step, velocities=velocities,
                           temperature=temperature):
        plt.sca(axis)
        ax, __ = SModel.plot_eulerian_stress2ndInvariant(step, xlim=xlim,
            colorbar=False, temperature=temperature)

        if velocities:
            plt.sca(ax)
            SModel.plot_eulerian_velocity_field_quiver(step, color="white")

        ax.set_xlabel("")
        ax.set_title("")
        ax.xaxis.set_ticklabels([])
        ax.set_yticks([0,-35])
        ax.set_yticklabels(["0 km", "35 km"])
        ax.set_ylabel("")
        return ax

    def add_elevation_colorbar(axis, im):
        box = axis.get_position()
        height = box.ymax - box.ymin
        pad, width = 0.02, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin, width, height])
        cb = get_shaded_colorbar(im, "vertical", cax)
        cb.solids.set_rasterized(True)
        cb.set_ticklabels(["", "-4" , "", "-2" ,"", "0", "", "2", "", "4", ""])
        cb.set_label("Elevation (km)")

        for t in cb.ax.get_yticklabels():
            t.set_horizontalalignment('right')
            t.set_x(1.3)

        return box, cax
    
    def add_erosed_colorbars(axis, im):
        box = axis.get_position()
        height = box.ymax - box.ymin
        pad, width = 0.01, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin, width, height])
        cb = ColorbarBase(cax, cmap="Reds",
                          orientation="vertical",
                          norm=Normalize(vmin=0., vmax=10.0),
                          ticks=range(0,11))
        cb.set_ticklabels(["0", "" , "", "" ,"", "5", "", "", "", "", "10"])
        cb.set_label("Sediment (km)", labelpad=-0.02)
        box = cb.ax.get_position()
        pad, width = 0.04, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin, width, height])
        cb = ColorbarBase(cax, cmap="Blues",
                          orientation="vertical",
                          norm=Normalize(vmin=0., vmax=10.0),
                          ticks=range(0,11))
        cb.set_ticklabels(["0", "" , "", "" ,"", "5", "", "", "", "", "10"])
        cb.set_label("Erosion (km)", labelpad=-0.02)
        return box, cax
    
    def add_erosed_colorbar(axis, im):
        box = axis.get_position()
        height = box.ymax - box.ymin
        pad, width = 0.02, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin, width, height])
        cb = ColorbarBase(cax, cmap="coolwarm",
                          orientation="vertical",
                          norm=Normalize(vmin=vmin, vmax=vmax),
                          ticks=range(int(vmin),int(vmax)+1))
        cb.solids.set_rasterized(True)
        labs = [""] * len(range(int(vmin), int(vmax)+1))
        labs[0] = str(int(vmin))+" km"
        labs[-1] = str(int(vmax)) + " km"
        labs[int(len(labs) / 2.0)] = "0"
        cb.set_ticklabels(labs)
        cb.set_label("Erosion / Sediment")
        return box, cax

    def add_sopale_colorbar_mat(axis):
        box = axis.get_position()
        height = box.ymax - box.ymin
        pad, width = 0.02, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin+0.01, width, height])
        cb = ColorbarBase(cax, cmap=ritskemat2, orientation="vertical")
        ticks = []
        for j, lab in enumerate(['SLM', 'ML', 'LC', 'UC', 'SED1', 'SED2']):
            ypos = .5/6.0 + j/6.0
            ticks.append(ypos)
            cb.ax.text(1.2, .5/6.+j/6., lab, ha='left', va='center')
            cb.ax.get_yaxis().set_ticks(ticks)
            cb.ax.set_yticklabels([])
            cb.solids.set_rasterized(True)
        return box, cb

    def add_sopale_colorbar_SR(axis):
        box = axis.get_position()
        height = box.ymax - box.ymin
        pad, width = 0.02, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin+0.01, width, height])
        cb = ColorbarBase(cax, cmap="jet",
                          orientation="vertical",
                          norm=LogNorm(vmin=1e-21,
                                       vmax=1e-13))
        #cb.ax.get_yaxis().set_ticks([1e-21, 1e-19, 1e-17, 1e-15, 1e-13])
        cb.solids.set_rasterized(True)
        return box, cb
    
    def add_sopale_colorbar_stress(axis):
        box = axis.get_position()
        height = box.ymax - box.ymin
        pad, width = 0.02, 0.02
        cax = fig.add_axes([box.xmax + pad, box.ymin+0.01, width, height])
        cb = ColorbarBase(cax, cmap="jet",
                          orientation="vertical",
                          norm=Normalize(vmin=0., vmax=1.0e3),
                          ticks=range(0,1100,100))
        cb.solids.set_rasterized(True)
        cb.ax.set_yticklabels(['0', '', '', '', '', '500 Mpa', '', '', '','','1 GPa'])
        return box, cb

    ax1, cax1, im1 = plot_fastscape(axes[0], step1)
    if sopaleField == "Material":
        ax2 = plot_sopale_mat(axes[1], step1)
    elif sopaleField == "Strain Rate":
        ax2 = plot_sopale_strainRate(axes[1], step1)
    elif sopaleField == "Stress":
        ax2 = plot_sopale_stress(axes[1], step1, velocities=velocities)

    ax3, cax2, im2 = plot_fastscape(axes[2], step2)
    if sopaleField == "Material":
        ax4 = plot_sopale_mat(axes[3], step2)
    elif sopaleField == "Strain Rate":
        ax4 = plot_sopale_strainRate(axes[3], step2)
    elif sopaleField == "Stress":
        ax4 = plot_sopale_stress(axes[3], step2, velocities=velocities)
    ax5, cax3, im3 = plot_fastscape(axes[4], step3)

    if sopaleField == "Material":
        ax6 = plot_sopale_mat(axes[5], step3)
    elif sopaleField == "Strain Rate":
        ax6 = plot_sopale_strainRate(axes[5], step3)
    elif sopaleField == "Stress":
        ax6 = plot_sopale_stress(axes[5], step3, velocities=velocities)

    ax6.set_xlabel("")
    ax6.set_xticks([400,450,500,550,600,650,700,750,800])
    ax6.set_xticklabels(["-200 km","-150 km","-100 km","-50 km","0","50 km","100 km","150 km","200 km"])

    if not erosionSedim:
        _, _ = add_elevation_colorbar(ax1, im1)
        _, _ = add_elevation_colorbar(ax3, im2)
        _, _ = add_elevation_colorbar(ax5, im3)
    else:
        _, _ = add_erosed_colorbar(ax1, im1)
        _, _ = add_erosed_colorbar(ax3, im2)
        _, _ = add_erosed_colorbar(ax5, im3)
    if sopaleField == "Material":
        _, _ = add_sopale_colorbar_mat(ax2)
        _, _ = add_sopale_colorbar_mat(ax4)
        _, _ = add_sopale_colorbar_mat(ax6)
    elif sopaleField =="Strain Rate":
        _, _ = add_sopale_colorbar_SR(ax2)
        _, _ = add_sopale_colorbar_SR(ax4)
        _, _ = add_sopale_colorbar_SR(ax6)
    elif sopaleField =="Stress":
        _, _ = add_sopale_colorbar_stress(ax2)
        _, _ = add_sopale_colorbar_stress(ax4)
        _, _ = add_sopale_colorbar_stress(ax6)


    if output:
        plt.savefig(output, bbox_inches='tight', dpi=600)

