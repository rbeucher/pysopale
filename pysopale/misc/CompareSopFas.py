import pysopale as sop
import pyfastscape as fas
import pylab as plt


def Compare(steps, field1="StrainRate",field2="Elevation", title="Model",
             fname="./RUN00/RUN00.h5", zrange=(-180,20), output=None, xlim=None):
    elevmin, elevmax = None, None
   
    sopale_options = {"StrainRate": sop.plot_eulerian_strain_rate_field,
                      "Strain": sop.plot_eulerian_strain_field,
                      "Materials": sop.plot_eulerian_material_colors}


    fastscape_options = {"Elevation": fas.PlotElevation,
                         "Streams": fas.PlotStream,
                         "Slope": fas.PlotSlope,
                         "Discharge":fas.PlotDischarge}

    f1 = sopale_options[field1]
    f2 = fastscape_options[field2]

    fig = plt.figure()
    plt.suptitle(title)

    def Plot(step, index, nrow=5, ncol=2):
        ax = fig.add_subplot(nrow,ncol,index*ncol + 1)
        ax.set_frame_on(False)
        ax.axes.xaxis.set_visible(False)
        ax.axes.yaxis.set_ticks(list(zrange))
        f1(step, ylim=zrange, colorbar="right", xlim=xlim)
        ax = fig.add_subplot(nrow,ncol,index*ncol + 2)
        ax.set_frame_on(False)
        ax.axes.xaxis.set_visible(False)
        ax.axes.yaxis.set_visible(False)
        ax, cax1 = f2(step,fname, vmin=None, vmax=None)
   
    for index, step in enumerate(steps):
        Plot(step, index, nrow=len(steps))

    plt.savefig(output, dpi=600)


def main():
    fas.SetHalfPageHorizontal()
    steps = [500,1000,2000,3000,4000]
    Compare(steps, field1="StrainRate", field2="Elevation", output="StrainRate_Elev.pdf")   
    Compare(steps, field1="Strain", field2="Elevation", output="Strain_Elev.pdf")   
    Compare(steps, field1="Strain", field2="Discharge", output="Strain_Discharge.pdf")   

 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
