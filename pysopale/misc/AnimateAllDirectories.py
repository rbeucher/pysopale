from .pysopale import animate_eulerian_material_colors
import os
import time
import argparse


def main():
    parser = argparse.ArgumentParser(prog='my_megazord_program')
    parser.add_argument('-xl','--xlim', help='xlim', required=False)
    parser.add_argument('-yl','--ylim', help='ylim', required=False)
    args = parser.parse_args()
    xlim = tuple(int(v) for v in re.findall("[0-9]+", args.xlim))
    ylim = tuple(int(v) for v in re.findall("[0-9]+", args.ylim))

    AnimateAllDirectories(xlim=xlim, ylim=ylim)


def AnimateAllDirectories(xlim=None, ylim=None):
    t0 = time.clock()
    directories = [d for d in os.listdir(os.getcwd()) if os.path.isdir(d)]

    for directory in directories:
         print("Doing Animation:", directory)
         animate_eulerian_material_colors(path=directory, output=directory+".mp4", xlim=xlim, ylim=ylim)

    print((time.time() - t0) / 60.0, "minutes process time")


