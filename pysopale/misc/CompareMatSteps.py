import pyfastscape as fas
import pylab as plt
from pysopale import plot_eulerian_material_colors
from pysopale import get_freesurface_elevation
from mpl_toolkits.axes_grid1 import make_axes_locatable

def CompareMatSteps(steps, nrow=4, ncol=2, path=".",
                    title="Model", output=None, xlim=None, ylim=None):
  
    fig = plt.figure()
    plt.suptitle(title)
    plt.subplots_adjust(wspace=0.4)

    for index, step in enumerate(steps):
        ax = fig.add_subplot(nrow, ncol, index+1)
        ax.set_frame_on(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        plot_eulerian_material_colors(step, path,
                                      xlim=xlim, 
                                      ylim=ylim,
                                      colorbar="right")
   
    if output:
        plt.savefig(output, dpi=600)


def CompareMatSteps2(steps, nrow=4, ncol=2, path=".",
                    title="Model", output=None, xlim=None, ylim=None):
  
    fig = plt.figure()
    plt.suptitle(title)
    plt.subplots_adjust(wspace=0.4)

    for index, step in enumerate(steps):
        ax = fig.add_subplot(nrow, ncol, index+1)
        ax.set_frame_on(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax, cab = plot_eulerian_material_colors(step, path,
                                      xlim=xlim, 
                                      ylim=ylim,
                                      colorbar=None)
        keep = ax.get_title()
        ax.set_title("")
        ax.set_xlabel("")
        divider = make_axes_locatable(ax)
        ax2 = divider.append_axes("top", size="50%", pad=0.05)
        xtop, ytop = get_freesurface_elevation(step, path)
        ax2.get_xaxis().set_visible(False)
        ax2.get_yaxis().tick_left()
        ax2.plot(xtop, ytop)
        ax2.set_title(keep)
        ax2.set_yticks([min(ytop), max(ytop)])
   
    if output:
        plt.savefig(output, dpi=600)

def main():

    fas.SetFullPage()
    steps = range(500, 4500, 500)
    # CompareMatSteps(steps, output="test.pdf", xlim=(400,800), ylim=(-200,10))
    CompareMatSteps2(steps, output="test.pdf", xlim=(400,800), ylim=(-150,10))
    
