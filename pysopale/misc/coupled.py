import pysopale as sop
import pyfastscape as fas
import pylab as plt
import h5py
from matplotlib import animation


def coupled_view(step, filename1="./RUN00/RUN00.h5", filename2=".",
                 title="Model"):
    fig = plt.clf()
    plt.suptitle(title)
    plt.subplot2grid((4,1), (0,0), rowspan=2)
    fastscape_model = fas.FastScapeModel(filename1)
    fastscape_model.PlotShadedReliefStep(step, xlim=(400, 800),
                                  ylim=(0, 180))
    plt.subplot2grid((4,1), (2,1))
    sopale_model = sop.SopaleModel()
    sopale_model.plot_eulerian_material_colors(step, clean=False)
    fastscape_model = None
    sopale_model = None

def coupled_animation(filename1="./RUN00/RUN00.h5", filename2=".", 
            output="video.mp4", title="Model", dpi=300, fps=5, bitrate=640):
    fig = plt.figure()
    fig.set_figwidth(4)
    fig.set_figheight(3)
    f = coupled_view
    A = h5py.File(filename1, "r")
    steps = list(A.items())
    steps = sorted([int(name[4:]) for name, object in steps[:-2]])
    
    # Get Range of value.
    f(steps[0], filename1, filename2, title)
    fargs = [filename1, filename2, title]
    ani = animation.FuncAnimation(fig, f, steps[1:], fargs=fargs, blit=False)

    # Set up formatting for the movie files
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=fps, bitrate=bitrate)
    ani.save(output, writer=writer, dpi=dpi)
    plt.close()
