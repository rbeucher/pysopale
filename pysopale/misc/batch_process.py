import itertools
from multiprocessing import Pool, freeze_support
import os
from pysopale import Compare
from pyfastscape import SetHalfPageHorizontal

def func_star(a):
    wd = os.getcwd()
    os.chdir(a["title"])
    print("Doing Model %s" % a["title"])
    Compare(**a)
    os.chdir(wd)
    return 1

def main():
    SetHalfPageHorizontal()
    pool = Pool()
    models = [elem for elem in os.listdir(".") if os.path.isdir(elem)]
    inputs = [{"steps":[500,1000,2000,3000,4000],
              "field1":"StrainRate",
              "field2":"Elevation",
              "output":model+"SR2.pdf",
              "title":model} for model in models]
    pool.map(func_star, inputs)

if __name__=="__main__":
    freeze_support()
    main()
