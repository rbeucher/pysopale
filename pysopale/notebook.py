#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#    
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


import nbformat.v4 as nb4
import nbformat as nb
import glob
from subprocess import call


def header():
    return ("%matplotlib inline\n"
            "import pysopale as sop\n"
            "import pyfastscape as fas\n"
            "import pylab as plt\n"
            "plt.rcParams['figure.figsize']=20,20\n")

def sopModel(ModelName, path):
    return "{0}=sop.SopaleModel('{1}')\n".format(ModelName,path)
    
def fasModel(ModelName, path):
    path += "/RUN00/RUN00.h5"
    return "{0}=fas.FastScapeModel('{1}')\n".format(ModelName,path)

def function(obj,f, **kwargs):
    l = len(kwargs)
    s = ""
    for i,key in enumerate(kwargs):
        if isinstance(kwargs[key],str):
            s+= "{0}='{1}'".format(key, kwargs[key])
        else:
            s+= "{0}={1}".format(key, kwargs[key])

        if(i < l-1):
            s+= ","
    return "{0}.{1}({2})\n".format(obj, f, s)

def subplots(dic):
    s = ""
    for i, v in enumerate(dic.keys):
        s += "plt.subplot({0},{1},{2})".format(len(dic.keys),1,i+1)

def create_notebook(name, path):
        
    Notebook=nb4.new_notebook()  

    cell1 = nb4.new_code_cell()    
    cell1["source"] += header()
    cell1["source"] += sopModel("SModel", path)
    cell1["source"] += fasModel("FModel", path)

    cell2 = nb4.new_markdown_cell("# Model {0}".format(path))
    cell3 = nb4.new_markdown_cell("## Materials")

    cell4 = nb4.new_code_cell()   
    cell4["source"] += "plt.subplot(411)\n"
    cell4["source"] += function("SModel", "plot_eulerian_material_colors", step=1000)
    cell4["source"] += "plt.subplot(412)\n"
    cell4["source"] += function("SModel", "plot_eulerian_material_colors", step=2000)
    cell4["source"] += "plt.subplot(413)\n"
    cell4["source"] += function("SModel", "plot_eulerian_material_colors", step=3000)
    cell4["source"] += "plt.subplot(414)\n"
    cell4["source"] += function("SModel", "plot_eulerian_material_colors", step=4000)
    
    cell5 = nb4.new_markdown_cell("## Strain")

    cell6 = nb4.new_code_cell()   
    cell6["source"] += "plt.subplot(411)\n"
    cell6["source"] += function("SModel", "plot_eulerian_strain_field",
            step=1000,xlim=(400,800))
    cell6["source"] += "plt.subplot(412)\n"
    cell6["source"] += function("SModel", "plot_eulerian_strain_field",
            step=2000,xlim=(400,800))
    cell6["source"] += "plt.subplot(413)\n"
    cell6["source"] += function("SModel", "plot_eulerian_strain_field",
            step=3000,xlim=(400,800))
    cell6["source"] += "plt.subplot(414)\n"
    cell6["source"] += function("SModel", "plot_eulerian_strain_filed",
            step=4000, xlim=(400,800))
    
    cell7 = nb4.new_markdown_cell("## Profiles")
    
    cell8 = nb4.new_code_cell()   
    cell8["source"] += "plt.subplot(411)\n"
    cell8["source"] += function("SModel", "plot_freesurface_elevation", step=1000, label='S')
    cell8["source"] += function("FModel", "PlotMeanElevProfile", step=1000, label='F')
    cell8["source"] += "plt.subplot(412)\n"
    cell8["source"] += function("SModel", "plot_freesurface_elevation", step=2000, label='S')
    cell8["source"] += function("FModel", "PlotMeanElevProfile", step=2000, label='F')
    cell8["source"] += "plt.subplot(413)\n"
    cell8["source"] += function("SModel", "plot_freesurface_elevation", step=3000, label='S')
    cell8["source"] += function("FModel", "PlotMeanElevProfile", step=3000, label='F')
    cell8["source"] += "plt.subplot(414)\n"
    cell8["source"] += function("SModel", "plot_freesurface_elevation", step=4000, label='S')
    cell8["source"] += function("FModel", "PlotMeanElevProfile", step=4000, label='F')
    cell8["source"] += "plt.legend()"
    
    cell9 = nb4.new_markdown_cell("## Surfaces")
    
    cell10 = nb4.new_code_cell()   
    cell10["source"] += "plt.subplot(411)\n"
    cell10["source"] += function("FModel", "PlotElevation", step=1000, xlim=(400,800))
    cell10["source"] += "plt.subplot(412)\n"
    cell10["source"] += function("FModel", "PlotElevation", step=2000, xlim=(400,800))
    cell10["source"] += "plt.subplot(413)\n"
    cell10["source"] += function("FModel", "PlotElevation", step=3000, xlim=(400,800))
    cell10["source"] += "plt.subplot(414)\n"
    cell10["source"] += function("FModel", "PlotElevation", step=4000, xlim=(400,800))
    
    
    cells = [cell1, cell2, cell3, cell4, cell7, cell8, cell9,
            cell10]
    for cell in cells:
        Notebook["cells"].append(cell)
    
    nb.write(Notebook,name+".ipynb", 4)


def run_all_notebook():
    notebooks = glob.glob("*.ipynb")
    for notebook in notebooks:
        call("ipython nbconvert --to notebook --inplace --ExecutePreprocessor.enabled=True {0}".format(notebook))

    return 1

