import pyfastscape as fas
import pysopale as sop
import pylab as plt

nrow = 2
ncol = 3

H5 = "RUN00/RUN00.h5"

fig = plt.figure()

plt.subplots_adjust(left=0.1, right=0.9, wspace=0.3, hspace=.5)
plt.subplot2grid((4, 1), (0, 0))
fas.PlotMinElevationTime(H5, label="Min")
fas.PlotMeanElevationTime(H5, label="Mean")
fas.PlotMaxElevationTime(H5, label="Max")
plt.legend()
ax = plt.gca()
ax.set_title("FastScape Topography")
ax.set_ylabel("Z (km)")

plt.subplot2grid((4, 1), (1, 0))
sop.plot_freesurface_elevation_at_all_time()

plt.subplot2grid((4, 1), (2, 0))
fas.PlotElevationMeanSerie3D(H5)

plt.savefig("Elevation.pdf", dpi=600)
