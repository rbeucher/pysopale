#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#    
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import pyfastscape as fas
import pysopale as sop
import matplotlib.pyplot as plt
import matplotlib
import os
import subprocess
from multiprocessing import Pool, freeze_support

class tryplot(object):

    def __init__(self, model):
        self.last = 0
        self.FModel = fas.FastScapeModel(model+"/RUN00/RUN00.h5", name=model)
        self.SModel = sop.SopaleModel(model, name=model)
    
    def plot_eroded(self, step, **kwargs):
        Model = self.FModel
        try:
            Model.PlotErosion(step, **kwargs)
            self.last = step
        except:
            try:
                step = Model._find_last_available_step("Elevation")
                if step != self.last:
                    ax, cax= Model.PlotErosion(step, **kwargs)
                    title = ax.get_title()
                    ax.set_title(title+"(last step available)")
                    self.last = step
                else:
                    raise 
            except:
                ax = plt.gca()
                plt.plot([0, 1], [0, 1], c="r", lw=3)
                plt.plot([0, 1], [1, 0], c="r", lw=3)
                ax.axis("off")
    
    def plot_sedim(self, step, **kwargs):
        Model = self.FModel
        try:
            Model.PlotSediment(step, **kwargs)
            self.last = step
        except:
            try:
                step = Model._find_last_available_step("Elevation")
                if step != self.last:
                    ax, cax= Model.PlotSediment(step, **kwargs)
                    title = ax.get_title()
                    ax.set_title(title+"(last step available)")
                    self.last = step
                else:
                    raise 
            except:
                ax = plt.gca()
                plt.plot([0, 1], [0, 1], c="r", lw=3)
                plt.plot([0, 1], [1, 0], c="r", lw=3)
                ax.axis("off")

    def plot_elevation(self, step, **kwargs):
        Model = self.FModel
        try:
            Model.PlotElevation(step, **kwargs)
            self.last = step
        except:
            try:
                step = Model._find_last_available_step("Elevation")
                if step != self.last:
                    ax, cax= Model.PlotElevation(step, **kwargs)
                    title = ax.get_title()
                    ax.set_title(title+"(last step available)")
                    self.last = step
                else:
                    raise 
            except:
                ax = plt.gca()
                plt.plot([0, 1], [0, 1], c="r", lw=3)
                plt.plot([0, 1], [1, 0], c="r", lw=3)
                ax.axis("off")
    
    def plot_shaded(self, step, **kwargs):
        Model = self.FModel
        try:
            Model.PlotShadedReliefStep(step, **kwargs)
            self.last = step
        except:
            try:
                step = Model._find_last_available_step("Elevation")
                if step != self.last:
                    ax, cax= Model.PlotShadedReliefStep(step, **kwargs)
                    title = ax.get_title()
                    ax.set_title(title+"(last step available)")
                    self.last = step
                else:
                    raise 
            except:
                ax = plt.gca()
                plt.plot([0, 1], [0, 1], c="r", lw=3)
                plt.plot([0, 1], [1, 0], c="r", lw=3)
                ax.axis("off")

    def plot_material(self, step, **kwargs):
        Model = self.SModel
        try:
            Model.plot_eulerian_material_colors(step, **kwargs)
            self.last = step
        except:
            try:
                step = Model._find_last_available_step("color1")
                if step != self.last:
                    ax, cax= Model.plot_eulerian_material_colors(step, **kwargs)
                    title = ax.get_title()
                    ax.set_title(title+"(last step available)")
                    self.last = step
                else:
                    raise 
            except:
                ax = plt.gca()
                plt.plot([0, 1], [0, 1], c="r", lw=3)
                plt.plot([0, 1], [1, 0], c="r", lw=3)
                ax.axis("off")

    def plot_profiles(self, step, **kwargs):
        SModel = self.SModel
        FModel = self.FModel
        try:
            SModel.plot_freesurface_elevation(label='S',step=step)
            FModel.PlotMeanElevProfile(label='F',step=step)
            plt.legend()
            self.last = step
        except:
            try:
                step = SModel._find_last_available_step("x1")
                if step != self.last:
                    SModel.plot_freesurface_elevation(label='S',step=step)
                    FModel.PlotMeanElevProfile(label='F',step=step)
                    plt.legend()
                    ax = plt.gca()
                    title = ax.get_title()
                    ax.set_title(title+"(last step available)")
                    self.last = step
                else:
                    raise 
            except:
                ax = plt.gca()
                plt.plot([0, 1], [0, 1], c="r", lw=3)
                plt.plot([0, 1], [1, 0], c="r", lw=3)
                ax.axis("off")
    
    def plot_profiles2(self, steps, **kwargs):
        
        FModel = self.FModel
        last = FModel._find_last_available_step("Elevation")
        steps = [step for step in steps if step < last]
        steps = steps + [last]
        for step in steps:
            time = FModel.GetTime(step)
            time = "{0:5.1f} Ma".format(time)
            FModel.PlotMeanElevProfile(label=time,step=step)
        
        plt.legend(loc=4)

def plot_overview(model, steps = [2500, 5000,7500,10000]):

    Tryplot = tryplot(model)
    
#    fig = plt.figure(figsize=(8.27,11.69))
#    plt.subplot(411)
#    Tryplot.plot_elevation(1000, xlim=(300,900))
#    plt.subplot(412)
#    Tryplot.plot_elevation(2000, xlim=(300,900))
#    plt.subplot(413)
#    Tryplot.plot_elevation(3000, xlim=(300,900))
#    plt.subplot(414)
#    Tryplot.plot_elevation(4000, xlim=(300,900))
#    plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.3)
#    plt.suptitle(model, fontsize=25)
#    plt.savefig(model+"_Surfaces"+".pdf")
#    plt.close() 
    if not os.path.isfile(model+"_Materials.pdf"): 
        plt.figure(figsize=(8.27,11.69))
        plt.subplot(611)
        Tryplot.plot_material(1)
        plt.subplot(612)
        Tryplot.plot_material(1000, velocities=True)
        plt.subplot(613)
        Tryplot.plot_material(steps[0], velocities=True)
        plt.subplot(614)
        Tryplot.plot_material(steps[1], velocities=True)
        plt.subplot(615)
        Tryplot.plot_material(steps[2], velocities=True)
        plt.subplot(616)
        Tryplot.plot_material(steps[3], velocities=True)
        plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.2)
        plt.suptitle(model, fontsize=25)
        plt.savefig(model+"_Materials"+".pdf", dpi=600)
        plt.close() 
    
    
    #if not os.path.isfile(model+"_Profiles.pdf"): 
    #    plt.figure(figsize=(8.27,11.69))
    #    plt.subplot(411)
    #    Tryplot.plot_profiles(steps[0], xlim=(300,900))
    #    plt.subplot(412)
    #    Tryplot.plot_profiles(steps[1], xlim=(300,900))
    #    plt.subplot(413)
    #    Tryplot.plot_profiles(steps[2], xlim=(300,900))
    #    plt.subplot(414)
    #    Tryplot.plot_profiles(steps[3], xlim=(300,900))
    #    plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.3)
    #    plt.suptitle(model, fontsize=25)
    #    plt.savefig(model+"_Profiles"+".pdf")
    #    plt.close() 
    
    if not os.path.isfile(model+"_Profiles.pdf"): 
        plt.figure(figsize=(8.27,6.0))
        Tryplot.plot_profiles2(steps)
        ax = plt.gca()
        ax.set_title("")
        plt.suptitle(model, fontsize=25)
        plt.savefig(model+"_Profiles"+".pdf")
        plt.close() 

    if not os.path.isfile(model+"_Shaded.pdf"): 
        plt.figure(figsize=(8.27,11.69))
        plt.subplot(411)
        Tryplot.plot_shaded(steps[0], xlim=(300,900))
        plt.subplot(412)
        Tryplot.plot_shaded(steps[1], xlim=(300,900))
        plt.subplot(413)
        Tryplot.plot_shaded(steps[2], xlim=(300,900))
        plt.subplot(414)
        Tryplot.plot_shaded(steps[3], xlim=(300,900))
        plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.3)
        plt.suptitle(model, fontsize=25)
        plt.savefig(model+"_Shaded"+".pdf")
        plt.close() 

    if not os.path.isfile(model+"_Eroded.pdf"): 
        plt.figure(figsize=(8.27,11.69))
        plt.subplot(411)
        Tryplot.plot_eroded(steps[0], xlim=(300,900))
        plt.subplot(412)
        Tryplot.plot_eroded(steps[1], xlim=(300,900))
        plt.subplot(413)
        Tryplot.plot_eroded(steps[2], xlim=(300,900))
        plt.subplot(414)
        Tryplot.plot_eroded(steps[3], xlim=(300,900))
        plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.3)
        plt.suptitle(model, fontsize=25)
        plt.savefig(model+"_Eroded"+".pdf")
        plt.close() 


    if not os.path.isfile(model+"_Sedim.pdf"): 
        plt.figure(figsize=(8.27,11.69))
        plt.subplot(411)
        Tryplot.plot_sedim(steps[0], xlim=(300,900))
        plt.subplot(412)
        Tryplot.plot_sedim(steps[1], xlim=(300,900))
        plt.subplot(413)
        Tryplot.plot_sedim(steps[2], xlim=(300,900))
        plt.subplot(414)
        Tryplot.plot_sedim(steps[3], xlim=(300,900))
        plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.3)
        plt.suptitle(model, fontsize=25)
        plt.savefig(model+"_Sedim"+".pdf")
        plt.close() 
    
    if not os.path.isfile(model+"_Shaded2.pdf"): 
        plt.figure(figsize=(8.27,11.69))
        plt.subplot(411)
        Tryplot.plot_shaded(steps[0])
        plt.subplot(412)
        Tryplot.plot_shaded(steps[1])
        plt.subplot(413)
        Tryplot.plot_shaded(steps[2])
        plt.subplot(414)
        Tryplot.plot_shaded(steps[3])
        plt.subplots_adjust(bottom=0.1, right=0.9, top=0.9, hspace=0.3)
        plt.suptitle(model, fontsize=25)
        plt.savefig(model+"_Shaded2"+".pdf")
        plt.close() 

def plot_overview_all(steps = [2500, 5000,7500,10000]):
    Models = [model for model in os.listdir(".") if os.path.isdir(model)]
    for model in Models:
        try:
            plot_overview(model, steps)
        except:
            pass

def func_star(a):
    print("Doing model: ", a)
    try:
        SModel = sop.SopaleModel(a, name=a)
        SModel.plot_all_eulerian_fields() 
    except:
        pass

def plot_materials_all(parallel=True):
    freeze_support()
    Models = [model for model in os.listdir(".") if os.path.isdir(model)]
    if parallel:
        pool = Pool(10)
        pool.map(func_star, Models)
    else:
        for model in Models:
            try:
                SModel = sop.SopaleModel(a, name=a)
                SModel.plot_all_eulerian_fields() 
            except:
                pass
            
        

def func_star2(a):
    print("Doing model: ", a)
    try:
        FModel = fas.FastScapeModel(a+"/RUN00/RUN00.h5", name=a)
        FModel.PlotAllSteps() 
    except:
        pass

def plot_shaded_all(parallel=True):
    freeze_support()
    Models = [model for model in os.listdir(".") if os.path.isdir(model)]
    if parallel:
        pool = Pool(10)
        pool.map(func_star2, Models)
    else:
        for model in Models:
            try:
                FModel = fas.FastScapeModel(a+"/RUN00/RUN00.h5", name=a)
                FModel.PlotAllSteps() 
            except:
                pass

def plot_poster(name, sealevel):
    Models = [model for model in os.listdir(".") if os.path.isdir(model)]
    Models = [model for model in Models if name in model]
    Models = [model for model in Models if sealevel in model]
    l = {}
    for model in Models:
        l.update({model:float(model[11:16])})

    l = sorted(l, key=l.__getitem__)

    Materials = ""
    Profiles = ""
    Surfaces = ""
    for model in l:
        Materials += model+"_Materials.pdf "
        Profiles += model+"_Profiles.pdf "
        Surfaces += model+"_Shaded.pdf "

    string = ' {0} {1} {2} --nup 6x3 --landscape --paper a0paper --outfile {3}'.format(Materials, Profiles, Surfaces, "PosterA.pdf")
    p = subprocess.call("pdfjam"+string, shell=True)

    Eroded = ""
    Sedim = ""
    Curves = ""
    for model in l:
        Eroded += model+"_Eroded.pdf "
        Sedim += model+"_Sedim.pdf "
        Curves += model+"_Curves.pdf "

    string = ' {0} {1} {2} --nup 6x3 --landscape --paper a0paper --outfile {3}'.format(Eroded, Sedim, Curves,"PosterB.pdf")
    p = subprocess.call("pdfjam"+string, shell=True)
