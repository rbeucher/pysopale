#    General Public License Version 3
#    Romain Beucher romain.beucher@geo.uib.no
#    
#    This file is part of pySopale
#
#    pySopale is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    pySopale is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


from .pysopale import *
from .models import *
from  .examples import *
from  .misc import *
import six
#from .notebook import *
from .plot_overview import *
#from .rcparams import *


try:
    import pyfastscape as fas
except ImportError:
    pass

